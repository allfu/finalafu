<?php

Class Leader extends CI_Controller {

public function __construct() {
parent::__construct();

// Load database
$this->load->model('login_database');
$this->load->model('mdl_student_reg');
$this->load->model('mdl_leader');
$this->load->library('facebook');
//if(!$this->session->userdata('logged_in'))     
	//{    redirect('leader','refresh');  }
}

// Show leader signup:
public function index() 
{
	if(!$this->session->userdata('logged_in'))     
	{    redirect('leader/leader_login','refresh');  }
else  {
        redirect('leader/leader_home','refresh');
    }
	//$this->leader_home();
	//$this->leader_login();
	//$this->load->view('vw_student_login');
		
}
public function logout() {
		// Remove local Facebook session
		$this->facebook->destroy_session();
		// Remove user data from session
		$this->session->unset_userdata('logged_in');
		// Redirect to login page
      
		//$this->load->view('vw_student_login', $data);
		redirect('leader/leader_login','refresh');
    }

	function leader_login()
	{
		$userData = array();
		// Check if user is logged in
        if(!empty($this->session->userdata('logged_in')))    {
            redirect('leader/leader_home');
        } else  
		{
			
		if($this->facebook->is_authenticated()){
			// Get user facebook profile details
			$userProfile = $this->facebook->request('get', '/me?fields=id,first_name,last_name,email,gender,locale,picture');

            // Preparing data for database insertion
            $userData['oauth_provider'] = 'facebook';
            $userData['fb_id'] = $userProfile['id'];
            $userData['user_name'] = $userProfile['first_name'].' '.$userProfile['last_name'];
            $userData['firstname'] = $userProfile['first_name'];
            $userData['lastname'] = $userProfile['last_name'];
            $userData['user_email'] = $userProfile['email'];
            $userData['gender'] = $userProfile['gender'];
            $userData['fb_pic'] = 'https://www.facebook.com/'.$userProfile['id'];
            //$userData['picture_url'] = $userProfile['picture']['data']['url'];
            // Insert or update user data
            $userID = $this->mdl_leader->checkUser($userData);
			// Check user data insert or update status
		
            if($userID == 1){
				
                $data['userData'] = $userData;
                $user_session = array(
                    'username' => $userData['user_name'],
                    'email' => $userData['user_email'],
                    'sessiontype' => 1
                );
                $this->session->set_userdata('logged_in',$user_session);
				echo "<script>alert('user does not exist');</script>";
				//echo $userID;
				//insert user
				$userData['oauth_provider'] = 'facebook';
				$userData['fb_id'] = $userProfile['id'];
				$userData['user_name'] = $userProfile['first_name'].' '.$userProfile['last_name'];
				$userData['firstname'] = $userProfile['first_name'];
				$userData['lastname'] = $userProfile['last_name'];
				$userData['user_email'] = $userProfile['email'];
				$userData['gender'] = $userProfile['gender'];
				//$userData['locale'] = $userProfile['locale'];
				$userData['fb_pic'] = $userProfile['picture']['data']['url'];
				
				//$this->mdl_leader->add_user($userData);
				
				 $fb_session = array(
                'oauth_provider' => 'facebook',
				'fb_id' 		 => $userProfile['id'],
				'user_name'		 => $userProfile['first_name'].' '.$userProfile['last_name'],
				'firstname'		 => $userProfile['first_name'],
				'lastname'		 => $userProfile['last_name'],
				'user_email'	 => $userProfile['email'],
				'gender' 		 => $userProfile['gender'],
				//'locale'		 => $userProfile['locale'],
				'fb_pic' 		 => $userProfile['picture']['data']['url'],
				
						);
				$this->session->set_userdata('user_profile',$fb_session);
				//$this->user_form();
				redirect('leader/user_form');
            } else {
               $data['userData'] = array();
			   //echo "facebook set now1";
			   //$this->user_form();
			   
			   echo "<script>alert('user is already exists');</script>";
			    $user_session = array(
                    'username' => $userData['user_name'],
                    'email' => $userData['user_email'],
					'fb_pic' => $userData['fb_pic'],
                    'sessiontype' => 1
                );
				 $this->session->set_userdata('logged_in',$user_session);
			  redirect('leader/leader_home');
			   //echo $userID;
            }
			
			// Get logout URL
			$data['logoutUrl'] = $this->facebook->logout_url();
		    }else{
            $fbuser = '';
			
			// Get login URL
            $data['authUrl'] =  $this->facebook->login_url();
        }
		
		// Load login & profile view
		//print_r($data);
        $this->load->view('vw_student_login',$data);
        }
		
	}
	
	function user_form()
	{
		$this->load->view('vw_insertfb');
	}
	
	// Show registration page
public function user_registration() {
$this->load->view('registration_form');
}

// Validate and store registration data in database
public function new_user_registration()
{
    // Check validation for user input in SignUp form
    $this->form_validation->set_rules('user_name', 'Username', 'trim|required');
    $this->form_validation->set_rules('first_name', 'Firstname', 'trim|required');
    $this->form_validation->set_rules('last_name', 'Lastname', 'trim|required');
    $this->form_validation->set_rules('contact', 'contact No', 'trim|required');
    $this->form_validation->set_rules('email', 'Email', 'trim|required|callback_validate_email');
    $this->form_validation->set_rules('website', 'Website', 'trim|required');
    $this->form_validation->set_rules('pass', 'Password', 'trim|required|min_length[6]');
    $this->form_validation->set_rules('re_pass', 'Confirm Password', 'required|trim|matches[pass]');
    $this->form_validation->set_rules('school', 'School Information', 'required|trim');
    $this->form_validation->set_rules('yr', 'Year of Study', 'required|trim');
    $this->form_validation->set_rules('matno', 'Matriculation No', 'required|trim');
    
    if ($this->form_validation->run() == FALSE) 
    {
         $data = array(
                'error'		    => 'fail',
                'user_name'     => form_error('user_name'),
                'first_name'    => form_error('first_name'),
                'last_name'     => form_error('last_name'),
                'contact'       => form_error('contact'),
                'email'         => form_error('email'),
                'website'       => form_error('website'),
                'pass'          => form_error('pass'),
                're_pass'       => form_error('re_pass'),
                'school'        => form_error('school'),
                'yr'            => form_error('yr'),
                'matno'         => form_error('matno'),				
                );
		      echo json_encode($data);
        //$this->load->view('registration_form');
    } 
    else 
    {
        $data = array(
            'user_name'     => $this->input->post('user_name'),
            'firstname'     => $this->input->post('first_name'),
            'lastname'      => $this->input->post('last_name'),
            'user_email'    => $this->input->post('email'),
            'user_password' => $this->input->post('pass'),
            'contact_no'    => $this->input->post('contact'),
            'website'       => $this->input->post('website'),
            'school'        => $this->input->post('school'),
            'yr'            => $this->input->post('yr'),
            'matno'         => $this->input->post('matno'),
        );
        $result = $this->login_database->registration_insert($data);
        $msg=null;
        if ($result == TRUE) 
        { $msg = true/*"Registration successful"*/;}
        else 
        { $msg= false/*"Username already exists!"*/; }
        $return_json = array('error'=> 'success','msg'=>$msg);
        echo json_encode($return_json);
        /*
        if ($result == TRUE) {
        $data['message_display'] = 'Registration Successful !';
        $this->load->view('login_form', $data);
        } else {
        $data['message_display'] = 'Username already exist!';
        $this->load->view('registration_form', $data);
        }*/
        
    }
}
    function validate_email($email) 
   {
	   if(! preg_match("/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/", $email))	
	   {
		   $this->form_validation->set_message('validate_email', 'Invalid Email.');			
		   return FALSE;		
	   }else{
		   return TRUE;
		   }	
	}

// Check for user login process
public function user_login() {

$this->form_validation->set_rules('username', 'Username', 'trim|required');
$this->form_validation->set_rules('password', 'Password', 'trim|required');

if ($this->form_validation->run() == FALSE) {
if(isset($this->session->userdata['logged_in'])){
/*$this->load->view('admin_page');*/
//redirect('students','refresh');
$this->leader_home();
}else{
$this->load->view('login_form');
}
} else {
$data = array(
'username' => $this->input->post('username'),
'password' => $this->input->post('password')
);
$result = $this->login_database->login($data);
if ($result == TRUE) {

$username = $this->input->post('username');
$result = $this->login_database->read_user_information($username);
if ($result != false) {
$session_data = array(
'username' => $result[0]->user_name,
'email' => $result[0]->user_email,
'sessiontype'=> 2,
);
// Add user data in session
$this->session->set_userdata('logged_in', $session_data);
/*$this->load->view('admin_page');*/
$this->leader_home();
//redirect('students','refresh');
}
} else {
$data = array(
'error_message' => 'Invalid Username or Password'
);
$this->load->view('login_form', $data);
}
}
}/* 

// Logout from admin page
public function logout() {

// Removing session data
$sess_array = array(
'username' => '',
'email'=> '',
'sessiontype'=>'',
);
$this->session->unset_userdata('logged_in', $sess_array);
$this->index();
//$data['message_display'] = 'Successfully Logged out';
//$this->load->view('login_form', $data);

} */
    
function stud_register()
{
       //echo json_encode($_POST);
    $this->form_validation->set_rules('matriculation_number', 'Matriculation no','trim|required|alpha_numeric');
    $this->form_validation->set_rules('university', 'University name','trim|required');
    //$this->form_validation->set_rules('faculty', 'Faculty name','trim|required');
    //$this->form_validation->set_rules('hall_name', 'Hall name','trim|required');
    //$this->form_validation->set_rules('cca_name[]', 'CCA name','trim|required');
 
	$data = null;
    if($this->form_validation->run() == false)
    {
         $data = array(
            'st'		            => 'fail',
            'matriculation_number'  => form_error('matriculation_number'),
            'university'            => form_error('university'),
            'faculty'               => form_error('faculty'),
            'hall_name'             => form_error('hall_name'),
            'ccaname'               => form_error('cca_name[]'),
                   );
          echo json_encode($data);
    }
    else 
	{
		
        //*send fb values from view to db/model*/
    $data = array(
	    'fb_id' 		=> $this->session->userdata['user_profile']['fb_id'],
	    'oauth_provider'=> $this->session->userdata['user_profile']['oauth_provider'],
		'user_name'		=> $this->session->userdata['user_profile']['user_name'],
	    'user_email'	=> $this->session->userdata['user_profile']['user_email'],
	    'firstname'		=> $this->session->userdata['user_profile']['firstname'],
	    'lastname'		=> $this->session->userdata['user_profile']['lastname'],
	    'gender'		=> $this->session->userdata['user_profile']['gender'],
	    'fb_pic'		=> $this->session->userdata['user_profile']['fb_pic'],
        'matno'  		=> $this->input->post('matriculation_number'),
        'school' 		=> $this->input->post('university'),
        'faculty'  	=> $this->input->post('faculty'),
        'hall_name' 	=> $this->input->post('hall_name'),
        'created'  		=> date('Y-m-d'),
        'type' 			=> $this->session->userdata['logged_in']['sessiontype'],/*use from session type*/
            );
    $lastid = $this->mdl_leader->add_user($data);
	
	//echo "<script>alert('after last id');</script>";
	$g = $this->input->post('cca_name[]');
	$h = $this->input->post('hall_name');
	$f = $this->input->post('faculty');
	
	$data1 = array(
				'gstudid' => $lastid,
				'gclubid' => $h, /*join club that was shown in halls dropdown vw_insertfb.php */
				'type' 	  => 1, /*user is joined type, not admin. so type 1.*/
				'created' => date('Y-m-d'),
				);
	$this->mdl_leader->join_club($data1);
	
	$data2 = array(
				'gstudid' => $lastid,
				'gclubid' => $f, /*join club that was shown in faculty dropdown of vw_insertfb.php*/
				'type' 	  => 1, /*user is joined group, not admin. so type 1=joined.*/
				'created' => date('Y-m-d'),
				);
	$this->mdl_leader->join_club($data2);
	
	if(!empty($g))	{
		foreach($g as $row) /*join all clubs that were shown in groups dropdown of vw_insertfb.php*/
		{
			$club= array(
			'gstudid' => $lastid ,
			'gclubid' => $row,
			'type' 	  => 1,  /*user is joined group type, not admin of group. so type 1.*/
			'created' => date('Y-m-d'),
			);
			$this->mdl_leader->join_club($club);
		}
	}
    $return_json = array('st'=> 'success','data1'=>$data,'data2'=>$g);
    echo json_encode($return_json);
	}
}

function leader_signup()
{
	 $data['res'] = $this->login_database->getRecords();
	 //print_r($data['res']);
    $this->load->view('registration_form',$data);        
}

function leader_home()
{
	if(!empty($this->session->userdata['logged_in'])) 
	{
		$username = ($this->session->userdata['logged_in']['username']);
		$email = ($this->session->userdata['logged_in']['email']);
	}
	$data['events']=$this->mdl_leader->getevents($username);
    //$this->load->view('vw_header2');
    //$this->load->view('vw_student_leader_home',$data);
    //$this->load->view('vw_footer');  
	
	$this->load->view('vw_updated_header');
    $this->load->view('vw_updated_leader_events',$data);
    $this->load->view('vw_updated_footer');   
}
function leader_source_sponsors($eventid = 34)
{
	if(isset($this->session->userdata['logged_in'])) 
	{
		$username = ($this->session->userdata['logged_in']['username']);
		$email = ($this->session->userdata['logged_in']['email']);
	}
	$data['events']=$this->mdl_leader->getevents($username);
	$data['sponsors']=$this->mdl_leader->getall_sponsors();
	$data['e_sponsors']=$this->mdl_leader->getevent_sponsors($eventid);
	
	$sponsor_data = array();
	foreach($data['e_sponsors'] as $sp)
	{
		$sponsor_data[] = $this->db->get_where('sponsor_table',array('s_id'=>$sp->spn_id,'permit'=>1));
	}
	
    $this->load->view('vw_updated_header');
    $this->load->view('leaderviews/vw_leader_sponsor',$data);
 
    $this->load->view('vw_updated_footer');
    
}
function my_groups()
{
	 //if (isset($this->session->userdata['logged_in'])) 
        //{
		$username = ($this->session->userdata['logged_in']['username']);
		$email = ($this->session->userdata['logged_in']['email']);
    //}
			   
        $data['grps']=$this->mdl_leader->getgroups($username);
		//print_r($data);
        
       /* $this->load->view('vw_header2');
        $this->load->view('vw_mygroups',$data);
        $this->load->view('vw_footer');
		*/
		$this->load->view('vw_updated_header');
        $this->load->view('vw_updated_mygroups',$data);
        $this->load->view('vw_updated_footer');
}

function create_event()
{
	//if (isset($this->session->userdata['logged_in'])) 
        //{
		$username = ($this->session->userdata['logged_in']['username']);
		$email = ($this->session->userdata['logged_in']['email']);
    //}   
    $data['grps']=$this->mdl_leader->getadmingroups($username);	
	
	/*$this->load->view('vw_header2');
    $this->load->view('vw_leader_create_event',$data);
    $this->load->view('vw_footer');*/	
	$this->load->view('vw_updated_header');
    $this->load->view('vw_updated_create_event',$data);
    $this->load->view('vw_updated_footer');
	
}
 function seeuid()
 {
 if(isset($this->session->userdata['logged_in'])) {
				$username = ($this->session->userdata['logged_in']['username']);
				$email = ($this->session->userdata['logged_in']['email']);
	}
	$data['events']=$this->mdl_leader->getevents($username);
    $this->load->view('vw_header2');
    $this->load->view('vw_leaderhome-bckup',$data);
    //$this->load->view('vw_student_leader_localhost');
    $this->load->view('vw_footer');
 }
 

function eventsubmit()
{
	//echo json_encode(array('post' => $_POST,'file' => $_FILES));
	//die;
	$username = $this->session->userdata['logged_in']['username'];
	$uid=$this->mdl_leader->getuserid($username);
	
	//Example Code
	//form input validation
		$update=null;
        //$this->form_validation->set_rules('e_fblink','Fb link','trim|required');
        //$this->form_validation->set_rules('e_instalink','Instagram link','trim|required');
        $this->form_validation->set_rules('e_name','Event name','trim|required');
        $this->form_validation->set_rules('e_by','Group name','trim|required');
        $this->form_validation->set_rules('e_desc','Description','trim|required|max_length[200]');
        $this->form_validation->set_rules('e_start','Start Date','trim|required');
        $this->form_validation->set_rules('e_end','End Date','trim|required');
        $this->form_validation->set_rules('e_type','Type','required');
        $this->form_validation->set_rules('e_loc','Location','required');
     
        if($this->form_validation->run() == FALSE)  
		{               //form validation false
            $data = array(
                'status' 		=> validation_errors(),
                'status_code'   => 2,
				'name' 			=> form_error('e_name'),
				'group'         => form_error('e_by'),
				'desc'          => form_error('e_desc'),
				//'start_date'    => form_error('e_start'),
				//'end_date'      => form_error('e_end'),
				'type'          => form_error('e_type'),
				'loc'           => form_error('e_loc'),
				
            );
            echo json_encode($data);
        }  
		else    
		{  
		$sourcePath = $_FILES['group_logo']['tmp_name']; //filename="file" 
		$targetPath = "assets/uploads/".rand(0,10000).$_FILES['group_logo']['name']; //targetpath with random number as prefix
		move_uploaded_file($sourcePath,$targetPath); //move file to target folder
		$data1 = array( //take inputs
			'e_name' => $this->input->post('e_name'),
			'e_desc' => $this->input->post('e_desc'),
			'e_img' => $targetPath,
			'e_logo' => $targetPath,
			'e_start' => $this->input->post('e_start'),
			'e_end' => $this->input->post('e_end'),
            'e_start_time'=>$this->input->post('e_start_time'),
            'e_end_time'=>$this->input->post('e_end_time'),
			'e_by' => $this->input->post('e_by'),
			//'e_fblink' => $this->input->post('e_fblink'),
			//'e_instalink' => $this->input->post('e_instalink'),
			'e_type' => $this->input->post('e_type'),
			'e_loc' => $this->input->post('e_loc'),
			'e_leader' => $uid,
			'created' => date('Y-m-d H:i:s'),
		);
            $insert_id = $this->mdl_leader->add_event($data1);          //send data1 array to model
		 
            if($insert_id) {                                               //if return array true
              /*$data2 = array(	
				'form_data' => $data1,
                'status' => 'Event Updated',
				'insert_id' => $insert_id,
                'status_code' => 1,
                'data' => $update,
            );
            echo json_encode($data2) */;
			$valid_formats = array("jpg", "png","jpeg");
			if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") 
			{
			for($i=0;$i<count($_FILES["images"]["name"]);$i++)
			{
				$name = $_FILES['images']['name'][$i];
				$size = $_FILES['images']['size'][$i];
				if (strlen($name)) 
				{
					list($txt, $ext) = explode(".", $name);
					if (in_array($ext, $valid_formats)) 
					{
						if ($ext == 'jpg' || $ext == 'png' || $ext == 'jpeg') 
						{
							if ($size < (1024 * 1024)) 
							{ // Image size max 1 MB
								$actual_image_name = time().$i. "." . $ext;
								$img =  'assets/frontend/img/clublogos/' . $actual_image_name;
								$tmp = $_FILES['images']['tmp_name'][$i];
								if (move_uploaded_file($tmp,$img)) 
								{
									$data=array(
									'event_id'=>$insert_id,
									'img_path'=>$img,
									'status' => 1
									);
									
									
									if($this->db->insert('event_slider_images',$data))
									{
										//echo $img;
										 /*$data = array(
											 'status_code' => 1,
											 'status' => 'Successful',
										 );
										 echo json_encode($data);*/
									}
								}  else {
								 $data = array(
											 'status_code' => 0,
											 'status' => 'Failed',
										 );
										 echo json_encode($data);
							}
							} else {
								 $data = array(
											 'status_code' => 4,
											 'status' => 'Image is bigger than 1MB',
										 );
										 echo json_encode($data);
						}
				
					} else {
										$data = array(
											 'status_code' => 5,
											 'status' => 'Invalid Image Format',
										 );
										 echo json_encode($data);
				} 
					} else {
										$data = array(
											 'status_code' => 6,
											 'status' => 'Please Select Image',
										 );
										 echo json_encode($data);
			}
			
		 }
		 }
		 $this->session->set_userdata('lastid', $insert_id);
			 $data = array(
				'status_code' => 1,
				 'status' => 'Successful',
			 );
			 echo json_encode($data);
		  }
				}   
				else    
				{
					 $data = array(
					'form_data' => $data1,
					'status' => 'Failed to Update',
					'status_code' => 0,
					'errors'=>validation_errors(),
				);
				echo json_encode($data);
				}		
        }
}
function sponsor_submit()
{
	
	   // echo json_encode(array('data' => $_POST));
		//$username = $this->session->userdata['logged_in']['username'];
	    //$uid=$this->mdl_leader->getuserid($username);
	    //$eid =  $this->input->post('e_id');
	    //$update=null;
        
		$this->form_validation->set_rules('sponsor[]','Sponsor','required' );
        
        if($this->form_validation->run() == FALSE)  
		{               //form validation false
            $data = array(
                'status' => validation_errors(),
                'status_code' => 2
            );
            echo json_encode($data);
        }  
		else    
		{    
			  $spnsr = $this->input->post('sponsor[]');
			  $event_id =$this->input->post('e_id');
			  $event = array();
			  //$spnsr = json_encode($spnsr);
			 /* $event = array(
				'spn_id' => $spnsr,
				'eve_id' => $event_id,
				'created' => date('Y-m-d'),
			  );  */
				foreach($spnsr as $row) 
				{
				$event= array(
				'spn_id' => $row,
				'eve_id' => $event_id,
				'created'=> date('Y-m-d H:i:s'),
							);
				$update = $this->mdl_leader->add_sponsor($event);
				//echo json_encode(array('data' => $update));
              } 
				//echo json_encode(array('update' => $update));
                     //send data1 array to model
		    if($update) {                                               //if return array true
             $data2 = array(	
				//'form_data' => $data1,
                'status' => 'Sponsor sent for Approval',
                'status_code' => 1,
                'data' => $update,
            );
            echo json_encode($data2);
            }   
			else    
			{
              $data = array(
				'form_data' => $data1,
                'status' => 'Failed to Submit',
                'status_code' => 0,
				'errors'=>validation_errors(),
            );
              echo json_encode($data);
            } 
			}
}
function edit_event($id)
{
	$username = $this->session->userdata['logged_in']['username'];
	$uid=$this->mdl_leader->getuserid($username);
	$data['grps']=$this->mdl_leader->getgroups($username);
	$data['event'] = $this->mdl_leader->editevent($id);
	$this->load->view('vw_updated_header');
    $this->load->view('vw_updated_edit_event',$data);
    $this->load->view('vw_updated_footer');
}

function editsubmit()
{
	//echo json_encode(array('post' => $_POST,'files'=>$_FILES));
	//die;
		$username = $this->session->userdata['logged_in']['username'];
		$uid=$this->mdl_leader->getuserid($username);
		$eid =  $this->input->post('e_id');
		$update=null;
        $this->form_validation->set_rules('e_name','Event name','trim|required');
        $this->form_validation->set_rules('e_desc','Description','trim|required');
        $this->form_validation->set_rules('e_type','Type','required');
        $this->form_validation->set_rules('e_loc','Location','required');
		//$this->form_validation->set_rules('e_by','CCA','trim|required');
        if($this->form_validation->run() == FALSE)  {               //form validation false
            $data = array(
                'status' => validation_errors(),
                'status_code' => 2
            );
            echo json_encode($data);
        }  
		else    
		{  		$targetPath = $this->input->post('event_logo');
		//check if logo is changed
				if(!empty($_FILES['event_logo']['name'])){	
				$sourcePath   = $_FILES['event_logo']['tmp_name'];
				$filename     =  rand(0,10000).$_FILES['event_logo']['name'];
				$targetPath   =  "assets/uploads/".$filename;
				move_uploaded_file($sourcePath, $targetPath);
				$data = array(
					'e_name' => $this->input->post('e_name'),
					'e_desc' => $this->input->post('e_desc'),
					'e_img' => $targetPath,
					'e_logo' => $targetPath,
					'e_start' => $this->input->post('e_start'),
					'e_end' => $this->input->post('e_end'),
					'e_start_time'=>$this->input->post('e_start_time'),
					'e_end_time'=>$this->input->post('e_end_time'),
					'e_by' => $this->input->post('e_by'),
					//'e_fblink' => $this->input->post('e_fblink'),
					//'e_instalink' => $this->input->post('e_instalink'),
					'e_type' => $this->input->post('e_type'),
					'e_loc' => $this->input->post('e_loc'),
					'e_leader' => $uid,
					'created' => date('Y-m-d H:i:s'),
						);
			}
			else
			{
				$data = array(
					'e_name' => $this->input->post('e_name'),
					'e_desc' => $this->input->post('e_desc'),
					'e_start' => $this->input->post('e_start'),
					'e_end' => $this->input->post('e_end'),
					'e_start_time'=>$this->input->post('e_start_time'),
					'e_end_time'=>$this->input->post('e_end_time'),
					'e_by' => $this->input->post('e_by'),
					'e_type' => $this->input->post('e_type'),
					'e_loc' => $this->input->post('e_loc'),
					'e_leader' => $uid,
					'created' => date('Y-m-d H:i:s'),
				);
			}
	       //$cid = $this->input->post('groupid');
	       $this->mdl_leader->update_event($eid,$data);
            $valid_formats = array("jpg", "png","jpeg");
           if(isset($_FILES['images']['name']))    {
                for($i=0;$i<count($_FILES["images"]["name"]);$i++)
		{
			$name = $_FILES['images']['name'][$i];
			$size = $_FILES['images']['size'][$i];
			if (strlen($name)) 
			{
				list($txt, $ext) = explode(".", $name);
				if (in_array($ext, $valid_formats)) 
				{
					if ($ext == 'jpg' || $ext == 'png' || $ext == 'jpeg') 
					{
						if ($size < (1024 * 1024)) 
						{ // Image size max 1 MB
							$actual_image_name = time().$i. "." . $ext;
							$img =  'assets/frontend/img/clublogos/' . $actual_image_name;
							$tmp = $_FILES['images']['tmp_name'][$i];
							if (move_uploaded_file($tmp,$img)) 
							{
								$data=array(
								'event_id'=>$eid,
								'img_path'=>$img,
								'status' => 1,
								);
								
								
								if($this->db->insert('event_slider_images',$data))
								{
									//echo $img;
                                     /*$data = array(
                                         'status_code' => 1,
                                         'status' => 'Successful',
                                     );
                                     echo json_encode($data);*/
								}
							}  else {
							 $data = array(
                                         'status_code' => 0,
                                         'status' => 'Failed',
                                     );
                                     echo json_encode($data);
						}
                        } else {
							 $data = array(
                                         'status_code' => 4,
                                         'status' => 'Image is bigger than 1MB',
                                     );
                                     echo json_encode($data);
					}
            
				} else {
                                    $data = array(
                                         'status_code' => 5,
                                         'status' => 'Invalid Image Format',
                                     );
                                     echo json_encode($data);
			} 
                } else {
                                    $data = array(
                                         'status_code' => 6,
                                         'status' => 'Please Select Image',
                                     );
                                     echo json_encode($data);
		}
		
	 }
	 }
            }
	
	$return_json = array('status_code' => 1, 'status' => 'Data Saved Successfully');
	echo json_encode($return_json);
		}
}
function image_submit()
 {
	 //echo json_encode(array('post' => $_POST,'file'=>$_FILES));
	 //die;
     $username = $this->session->userdata['logged_in']['username'];
     $this->form_validation->set_rules('c_uni','School Name','trim|required');
     $this->form_validation->set_rules('c_name','Group Name','trim|required');
     //$this->form_validation->set_rules('group_logo','Group Image is Required','trim|required');
     if($this->form_validation->run() == false) {
         $data = array(
            'status_code' => 0,
             'status' => validation_errors(),
         );
         echo json_encode($data);
     }  else    {
	$response = array();
	$uid= $this->mdl_leader->getuserid($username); 
    $sourcePath   = $_FILES['group_logo']['tmp_name'];
    $filename     =  rand(0,10000).$_FILES['group_logo']['name'];
    $targetPath   =  "assets/frontend/img/clublogos/".$filename;
    move_uploaded_file($sourcePath, $targetPath);
	$data=array(
	'c_uni'=>$_POST['c_uni'],
	'c_name'=>$_POST['c_name'],
	'c_desc'=>$_POST['c_desc'],
	'c_by'=>$uid,
    'c_logo' => $targetPath,
	'created'=>date('Y-m-d'),
	);
	
	$path1 = "clublogos/";	
	$valid_formats = array("jpg", "png","jpeg");
    $lastid = $this->mdl_leader->add_club($data);
	 if(!empty($lastid)) {
		$data2 = array(
            'gstudid' =>$uid,
            'gclubid' => $lastid,
            'type'    => 2,
            'created' => date('Y-m-d'),
            );
        $this->mdl_leader->join_club($data2);
		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") 
		{
		for($i=0;$i<count($_FILES["images"]["name"]);$i++)
		{
			$name = $_FILES['images']['name'][$i];
			$size = $_FILES['images']['size'][$i];
			if (strlen($name)) 
			{
				list($txt, $ext) = explode(".", $name);
				if (in_array($ext, $valid_formats)) 
				{
					if ($ext == 'jpg' || $ext == 'png' || $ext == 'jpeg') 
					{
						if ($size < (1024 * 1024)) 
						{ // Image size max 1 MB
							$actual_image_name = time().$i. "." . $ext;
							$img =  'assets/frontend/img/clublogos/' . $actual_image_name;
							$tmp = $_FILES['images']['tmp_name'][$i];
							if (move_uploaded_file($tmp,$img)) 
							{
								$data=array(
								'club_id'=>$lastid,
								'images'=>$img,
								);
								
								
								if($this->db->insert('slider_images',$data))
								{
									//echo $img;
                                     /*$data = array(
                                         'status_code' => 1,
                                         'status' => 'Successful',
                                     );
                                     echo json_encode($data);*/
								}
							}  else {
							 $data = array(
                                         'status_code' => 0,
                                         'status' => 'Failed',
                                     );
                                     echo json_encode($data);
						}
                        } else {
							 $data = array(
                                         'status_code' => 4,
                                         'status' => 'Image is bigger than 1MB',
                                     );
                                     echo json_encode($data);
					}
            
				} else {
                                    $data = array(
                                         'status_code' => 5,
                                         'status' => 'Invalid Image Format',
                                     );
                                     echo json_encode($data);
			} 
                } else {
                                    $data = array(
                                         'status_code' => 6,
                                         'status' => 'Please Select Image',
                                     );
                                     echo json_encode($data);
		}
		
	 }
	 }
	 $this->session->set_userdata('lastid', $lastid);
         $data = array(
            'status_code' => 1,
             'status' => 'Successful',
         );
         echo json_encode($data);
	  }
     }
     }
 }
function delete_event($eid)
{
	$r=null;
	$r = $this->mdl_leader->deleteevent($eid);
	if($r)
	{
		//echo "<script>alert('redirect');</script>";
		echo "<script> swal({
                                html: parsed.status,
                                type: 'success'
                        })</script>";
		redirect('leader/leader_home','refresh');		
	}
	else
	{ 
		echo "<script> swal({
                                html: parsed.status,
                                type: 'error',
                            });</script>";
	}
	
}
 function create_group()
 {
	$this->load->view('vw_updated_header');
    $this->load->view('vw_updated_create_group');
    $this->load->view('vw_updated_footer');
 }
 function edit_group($gid,$uniid)
 {
	$data['groupdata']= $gid;
	$data['unidata']= $uniid;
	$data['members'] = $this->mdl_leader->members($gid);
	$data['sliderimgs'] = $this->mdl_leader->get_slider_imgs($gid)->result();
	$this->load->view('vw_updated_header');
    $this->load->view('vw_updated_edit_grp_frm',$data);
    $this->load->view('vw_updated_footer');
 }
 
 function group_submit()
 {
	 //echo json_encode($_POST);
	 //echo json_encode($_FILES);
	 
	$username = $this->session->userdata['logged_in']['username'];
	$uid      = $this->mdl_leader->getuserid($username);
	$this->form_validation->set_rules('c_uni', 'School Name', 'trim|required');
	$this->form_validation->set_rules('c_name', 'Group Name', 'trim|required');
	$this->form_validation->set_rules('c_desc', 'Group Description', 'trim|required|max_length[200]');
	
	 /*if (empty($_FILES['logo']['name'])) {                        //check file is selected
            $this->form_validation->set_rules('logo','Image','required');
        }*/
    $data = null;
	if($this->form_validation->run() == false)
	{
		 $data = array(
			'st'	  => 'fail',
			'st_code' => 1, /*1- validation errors*/
			'c_uni'   => form_error('c_uni'),
			'c_name'  => form_error('c_name'),
			'c_desc'  => form_error('c_desc'),
			);
		  echo json_encode($data);
	}
	else {
	$filename = "default_logo.png";	 
	$path = "assets/frontend/img/clublogos/".$filename;	
		/*if no img uploaded, this wil be def value to pass in array, so tat ders no err msg
		also, default.jpg file always exits in image folder */
		/* upload banner code*/    
    
		$types = array('image/jpeg', 'image/gif','image/png','image/jpg');
            
        
        if (!in_array($_FILES['logo']['type'], $types))
        {
            
           $ret = array('st'=> 'fail','st_code' => 3);//3 - file type error
            echo json_encode($ret);
        }
      
       
            $sourcePath   = $_FILES['logo']['tmp_name'];
            $filename     =  rand(0,10000).$_FILES['logo']['name'];
            $targetPath   =  "assets/frontend/img/clublogos/".$filename;
            move_uploaded_file($sourcePath, $targetPath);
			
		//}
	$data = array(
		'c_uni'    => $this->input->post('c_uni'),
		'c_name'   => $this->input->post('c_name'),
		'c_desc'   => $this->input->post('c_desc'),
		'c_by'     => $uid,
		'c_logo'   => $targetPath,
		'created'  => date('Y-m-d'),
			);
	$insertedclubid = $this->mdl_leader->add_club($data);
	$data2 = array(
		'gstudid' =>$uid,
		'gclubid' => $insertedclubid,
		'type' 	  => 2,
		'created' => date('Y-m-d'),
				);
	$this->mdl_leader->join_club($data2);
	$return_json = array('st'=> 'success','st_code' => 2);//2 - success
	echo json_encode($return_json);
	
	}
 }
 function editgroup_submit()
 {
	 //echo json_encode(array('data' => $_POST,'Files' => $_FILES));
	$username = $this->session->userdata['logged_in']['username'];
	$uid= $this->mdl_leader->getuserid($username);
	$this->form_validation->set_rules('c_uni', 'School Name', 'trim|required');
	$this->form_validation->set_rules('c_name', 'Group Name', 'trim|required');
	$this->form_validation->set_rules('c_desc', 'Group Description', 'trim|required|max_length[200]');
	/* if(empty($_FILES['logo']['name']))	{
		$this->form_validation->set_rules('logo', 'Logo Error', 'required');
	} */
	$data = null;
	$types = array('image/jpeg', 'image/gif','image/png','image/jpg');
	/*  if (!in_array($_FILES['logo']['type'], $types))
        {
            //echo "<script>alert('File type not allowed')</script>";
           $ret = array(
					'st'=> 'fail',
					'st_code' => 3
					);//3 - file type error
            echo json_encode($ret);
        } */
	
	
	if($this->form_validation->run() == false)
	{
		 $data = array(
			'st'	  => 'fail',
			'st_code' => 1, /*1- validation errors*/
			'c_uni'   => form_error('c_uni'),
			'c_name'  => form_error('c_name'),
			'c_desc'  => form_error('c_desc'),
			'form_error' => validation_errors()
			);
		  echo json_encode($data);
	}
	else {
		$targetPath = $this->input->post('group_logo');
		//check if logo is changed
		if(!empty($_FILES['group_logo']['name'])){	
				$sourcePath   = $_FILES['group_logo']['tmp_name'];
				$filename     =  rand(0,10000).$_FILES['group_logo']['name'];
				$targetPath   =  "assets/frontend/img/clublogos/".$filename;
				move_uploaded_file($sourcePath, $targetPath);
				$data = array(
					'c_uni'    => $this->input->post('c_uni'),
					'c_name'   => $this->input->post('c_name'),
					'c_desc'   => $this->input->post('c_desc'),
					'c_by'     => $uid,
					'c_logo'   => $targetPath,
					'modified' => date('Y-m-d'),
						);
			}
			else
			{
				$data = array(
					'c_uni'    => $this->input->post('c_uni'),
					'c_name'   => $this->input->post('c_name'),
					'c_desc'   => $this->input->post('c_desc'),
					'c_by'     => $uid,
					//'c_logo'   => $this->input->post('logo1'),
					'modified' => date('Y-m-d'),
						);
			}
	       $cid = $this->input->post('groupid');
	       $this->mdl_leader->edit_club($cid,$data);
            $valid_formats = array("jpg", "png","jpeg");
           if(isset($_FILES['images']['name']))    {
                for($i=0;$i<count($_FILES["images"]["name"]);$i++)
		{
			$name = $_FILES['images']['name'][$i];
			$size = $_FILES['images']['size'][$i];
			if (strlen($name)) 
			{
				list($txt, $ext) = explode(".", $name);
				if (in_array($ext, $valid_formats)) 
				{
					if ($ext == 'jpg' || $ext == 'png' || $ext == 'jpeg') 
					{
						if ($size < (1024 * 1024)) 
						{ // Image size max 1 MB
							$actual_image_name = time().$i. "." . $ext;
							$img =  'assets/frontend/img/clublogos/' . $actual_image_name;
							$tmp = $_FILES['images']['tmp_name'][$i];
							if (move_uploaded_file($tmp,$img)) 
							{
								$data=array(
								'club_id'=>$cid,
								'images'=>$img,
								);
								
								
								if($this->db->insert('slider_images',$data))
								{
									//echo $img;
                                     /*$data = array(
                                         'status_code' => 1,
                                         'status' => 'Successful',
                                     );
                                     echo json_encode($data);*/
								}
							}  else {
							 $data = array(
                                         'status_code' => 0,
                                         'status' => 'Failed',
                                     );
                                     echo json_encode($data);
						}
                        } else {
							 $data = array(
                                         'status_code' => 4,
                                         'status' => 'Image is bigger than 1MB',
                                     );
                                     echo json_encode($data);
					}
            
				} else {
                                    $data = array(
                                         'status_code' => 5,
                                         'status' => 'Invalid Image Format',
                                     );
                                     echo json_encode($data);
			} 
                } else {
                                    $data = array(
                                         'status_code' => 6,
                                         'status' => 'Please Select Image',
                                     );
                                     echo json_encode($data);
		}
		
	 }
	 }
            }
	
   // print_r (edit_group($gid,$data2));
	$return_json = array('status_code' => 1, 'status' => 'Data Saved Successfully');
	echo json_encode($return_json);
	} 
 }
 function join_group()
 {
	$username = $this->session->userdata['logged_in']['username'];
	$uid=$this->mdl_leader->getuserid($username);
	$data['allgrps']=$this->mdl_leader->getallgroups();
	$data['newgrps']=$this->mdl_leader->get_new_groups($uid);	
	$data['uid']=$uid;
	
	
	//$this->load->view('vw_header2');
    //$this->load->view('vw_other_groups',$data);
    //$this->load->view('vw_footer');
    $this->load->view('vw_updated_header');
    $this->load->view('vw_updated_other_groups',$data);
    $this->load->view('vw_updated_footer');
 }
 function join_newgroup($clubid)
 {
	$username = $this->session->userdata['logged_in']['username'];
	$uid=$this->mdl_leader->getuserid($username);
	$data5=array(
		'gstudid' =>$uid,
		'gclubid' => $clubid,
		'type'	  => 1,
		'created' => date('Y-m-d'),
	);
	$this->mdl_leader->join_club($data5);
	redirect('leader/join_group','refresh');	 
 }
 
 function delete_group($id)
 {
	$g=null;
	$g = $this->mdl_leader->deletegrp($id);
	if($g)
	{
		echo "<script> swal({
                                html: parsed.status,
                                type: 'success'
                        })</script>";
		redirect('leader/my_groups','refresh');
		
	}
	else
	{ 
		echo "<script> swal({
                                html: parsed.status,
                                type: 'error',
                            });</script>";
	} 
 }
 
 function publish_event($eid)
 {
	$p = $this->mdl_leader->publish_e($eid);
	redirect('leader/leader_home','refresh');
	 
	 
 }
  function unpublish_event($eid)
 {
	$p = $this->mdl_leader->unpublish_e($eid);
	redirect('leader/leader_home','refresh');
 }
 
 function search()
 {
	$username = null;
	if(isset($this->session->userdata['logged_in'])) 
	{
		$username = ($this->session->userdata['logged_in']['username']);
		$email    = ($this->session->userdata['logged_in']['email']);
	}
	$keyword  = $this->input->post('txtsearch');
	$keyword1 = "is";
	$result['events']   = $this->mdl_leader->getsearchdata($keyword,$username);
	 
	$this->load->view('vw_header2');
    $this->load->view('vw_leader_search',$result);
    //$this->load->view('vw_student_leader_localhost');
    $this->load->view('vw_footer');
	
	//$data['events']=$this->mdl_leader->getevents($username);
 }
 
 function search1()
 {
	 $keyword = "is";
        $sql = "(SELECT e_id, e_name, e_desc,'event' as type FROM event_table 
        WHERE e_name LIKE '%".$keyword."%' OR e_desc LIKE '%".$keyword."%')
        UNION
        (SELECT c_id,c_name, c_desc,'club' as type FROM club_table WHERE c_name LIKE '%" . 
           $keyword . "%' OR c_desc LIKE '%" . $keyword ."%')";

    //mysql_query($query);
    $query1= $this->db->query($sql);
    
        foreach($query1->result() as $row)
        {
            echo "<pre>";
            print_r($row);
            echo "</pre>";
            echo "////".$row->type;
        }
        echo "<br>single: ".$query1->row()->type;
 }
 
function add_groupadmin($gid,$groupid,$uniid)
 {
	$query =$this->mdl_leader->make_grp_admin($gid);
	$this->edit_group($groupid,$uniid);	 
 }
 
 function remove_member($gid,$groupid,$uniid)
 {
	$query =$this->mdl_leader->remove_grp_member($gid);
	$this->edit_group($groupid,$uniid);
 }
 
 function get_data($clubid)
 {	
	$uni 	 = $this->input->get('grp_uni');
	$datarow = $this->mdl_leader->get_uni_name($uni);
	
	$university_name = $datarow->u_name;
	
	$result1 	 = $this->mdl_leader->get_slider_imgs($clubid);
	$dataresult1 = $result1->result();
	$dataresult2 = $result1->num_rows();
	
	
	$joinedgroup_data = null;
	$is_joined   = null;
	$is_group_admin   = null;
	$club_id     = $this->input->get('club_id');
	$username    = $this->session->userdata['logged_in']['username'];
	$userid      = $this->mdl_leader->getuserid($username);
	$joined_data = $this->mdl_leader->get_if_joined($club_id,$userid);
	if($joined_data->num_rows() != 0)
	{
		$is_joined  	  = 'yes';
		$joinedgroup_data = $joined_data->row();
        $is_group_admin   = $joined_data->row()->type; 		
	}		
	$arr = array(
				'st' 		  => 'success',
				'rowdata'	  => $uni,
				'uni_name'	  => $university_name,
				'cid'		  => $club_id,
				'imgs'		  => $dataresult1,
				'c'			  => $dataresult2,
				'extra' 	  => 'a',
				'is_joined'	  => $is_joined,
				'joined_data' => $joinedgroup_data,
				'member_type' => $is_group_admin,
			);
	echo json_encode($arr);
 }
 function remove_image()
 {
	$image_id = $this->input->post('image_id');
	$status = 0;
	$this->db->set('status', $status);
	$this->db->where('id', $image_id);
	$result = $this->db->update('slider_images');
	if($result)
		{
		echo json_encode(array('status_code'=> 1, 'status' => 'success'));
		} else  
		{
		echo json_encode(array('status_code'=> 0, 'status' => 'failed'));
		}
  }
  function remove_event_image()
 {
	$image_id = $this->input->post('image_id');
	$status = 0;
	$this->db->set('status', $status);
	$this->db->where('id', $image_id);
	$result = $this->db->update('event_slider_images');
	if($result)
		{
		echo json_encode(array('status_code'=> 1, 'status' => 'success'));
		} else  
		{
		echo json_encode(array('status_code'=> 0, 'status' => 'failed'));
		}
  }
  
  function remove_me_group($clubid)
  {
	$username = $uid = null;
	if(isset($this->session->userdata['logged_in'])) 
	{
		$username = ($this->session->userdata['logged_in']['username']);
		$email    = ($this->session->userdata['logged_in']['email']);
	}
	$uid=$this->mdl_leader->getuserid($username);
	$this->mdl_leader->remove_me_from_group($uid,$clubid);
	redirect('leader/my_groups','refresh');
	  
  }
   function getunidata()
    {
        $uni = $this->input->post('uni_id');
        
        $g = $this->mdl_leader->get_uni_groups($uni);
        $res_grps = $g->result();
        
        $f = $this->mdl_leader->get_uni_faculty($uni);
        //$res_fac = $f->result();
        
        $h = $this->mdl_leader->get_uni_hall($uni);
        //$res_hall = $h->result();
        
        $data_array = array(
                        'st'   => 'success',
                        'grps' => $res_grps,
                        'fac'  => $f,
                        'hall' => $h,
                );
        echo json_encode($data_array);
    }
	
	function browse_events()
	{
		$username = $uid = null;
		if(isset($this->session->userdata['logged_in'])) 
		{
			$username = ($this->session->userdata['logged_in']['username']);
			$email    = ($this->session->userdata['logged_in']['email']);
		}
		$uid=$this->mdl_leader->getuserid($username);
		$data['events'] = $this->mdl_leader->get_my_group_events($uid);
		//print_r($data);
		$this->load->view('vw_updated_header');
		$this->load->view('vw_browse_events',$data);
		$this->load->view('vw_updated_footer');
	}
	
	function join_event($event_id)
	{
		$username = $uid = null;
		if(isset($this->session->userdata['logged_in'])) 
		{
			$username = ($this->session->userdata['logged_in']['username']);
			$email    = ($this->session->userdata['logged_in']['email']);
		}
		$uid=$this->mdl_leader->getuserid($username);
		
		$this->mdl_leader->join_new_event($event_id,$uid);
		
	}


}
?>