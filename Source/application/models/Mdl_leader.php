<?php
Class Mdl_leader extends CI_Model 
{
	function __construct()	{
		parent::__construct();
		$this->tableName = 'user_table';
		$this->primaryKey = 'id';
	}
	function getevents($username)
	{
		$q = $this->db->get_where('user_table',array('user_name'=>$username));
        $i = $q->row()->id;
		$g = $this->db->get_where('event_table',array('e_leader'=>$i,'permit'=>1));
        return $g->result();
		//$this->
	}
	
	function getadmingroups($username)
	{
		$q = $this->db->get_where('user_table',array('user_name'=>$username));
        $i = $q->row()->id;
                
        $g = $this->db->get_where('group_table',array('gstudid'=>$i,'permit'=>1,'type'=>2));/* 'type'=>2: type=2 means he is admin, so he sees only grps of which he is admin*/
      	return $g->result();
      
		
	}
	
	function getgroups($username)/**/
    {		
        $q = $this->db->get_where('user_table',array('user_name'=>$username));
        $i = $q->row()->id;
                
        $g = $this->db->get_where('group_table',array('gstudid'=>$i,'permit'=>1));/* 'type'=>2: type=2 means he is admin, so he sees only grps of which he is admin*/
        return $g->result();
		//print_r($g->result());
        
    }
	
	function add_event($data)
	{
		$query = $this->db->insert('event_table',$data);
		return $this->db->insert_id();;
		
	}
	function add_sponsor($data)
	{ 
		$this->db->insert('sponsor_event',$data);
		return true;
		
	}
	function getuserid($username)
	{
		$q = $this->db->get_where('user_table',array('user_name'=>$username));
        $i = $q->row()->id;
		return $i;
		
	}
	function deleteevent($id)
	{
		$info=array('permit'=>0,'modified'=>date('Y-m-d'));
        $this->db->where('e_id', $id);
        $query = $this->db->update('event_table',$info );
		return $query;
	}
	
	function add_club($data)
	{
		$this->db->insert('club_table',$data);
		$insert_id = $this->db->insert_id();
		return $insert_id;		
	}
	
	function getallgroups()
	{
		/*$this->db->where('gstudid !=', $uid);
		$g = $this->db->get_where('group_table');
        return $g->result();*/
		$g = $this->db->get_where('club_table',array('permit'=>1));
        return $g->result();
		
	}
	function join_club($data2)
	{
		$this->db->insert('group_table',$data2);
	}
	
	function editevent($id)
	{
		$q = $this->db->get_where('event_table',array('e_id'=>$id));
		return $q->row();
	}
	function update_event($eid,$data)	{
		$this->db->where('e_id',$eid);
		$query = $this->db->update('event_table',$data);
		//return $query;
	}
	
	function deletegrp($id)
	{
		$info=array('permit'=>0,'modified'=>date('Y-m-d'));
        $this->db->where('c_id', $id);
        $query = $this->db->update('club_table',$info );
		return $query;
	}
	function edit_group($gid,$data2)
	{
		$this->db->where('g_id', $cid);
		$this->db->update('group_table',$data2);
		
	}
	function edit_club($cid,$data)
	{ 
		$this->db->where('c_id', $cid);
		$this->db->update('club_table', $data);
		return true;
		
	}
	
	function publish_e($eid)
	{		
		$info=array('publish'=>1,'modified'=>date('Y-m-d'));
        $this->db->where('e_id', $eid);
        $query = $this->db->update('event_table',$info );
		return $query;
		
	}
	function unpublish_e($eid)
	{		
		$info=array('publish'=>0,'modified'=>date('Y-m-d'));
        $this->db->where('e_id', $eid);
        $query = $this->db->update('event_table',$info );
		return $query;
		
	}
	
	function getsearchdata($keyword, $username)
	{
		$q = $this->db->get_where('user_table', array('user_name'=>$username));
		$u = $q->row();
		$sql = "(SELECT e_id, e_name, e_desc,e_img,e_start,e_end,e_loc,e_type,e_fblink,e_instalink,e_by,e_leader,
					   publish, permit, created,modified,'event' as type FROM event_table 
        WHERE e_name LIKE '%".$keyword."%' OR e_desc LIKE '%".$keyword."%')";
		$query1= $this->db->query($sql);
    
        /*foreach($query1->result() as $row)
        {
            echo "<pre>";
            print_r($row);
            echo "</pre>";
            echo "////".$row->type;
        }
        echo "<br>single: ".$query1->row()->type;*/
		
		return $query1->result();
	}
	
	function getall_sponsors()
	{
		$q = $this->db->get_where('sponsor_table',array('permit'=>1));
		 return $q->result();
		
	}
	
	function members($groupid)
	{
		 $query =$this->db->get_where('group_table',array('gclubid'=> $groupid,'permit'=>1));
		 return $query->result();
	}
	
	function make_grp_admin($gid)
	{
		$info=array('type'=>2,'modified'=>date('Y-m-d'));
        $this->db->where('gid', $gid);
        $query = $this->db->update('group_table',$info );
		return $query;
	}
	
	function remove_grp_member($gid)
	{
		$info=array('permit'=>0,'modified'=>date('Y-m-d'));
        $this->db->where('gid', $gid);
        $query = $this->db->update('group_table',$info );
		return $query;
	}
	
	function get_uni_name($uid)
	{
		$uni = $this->db->get_where('university',array('u_id'=>$uid));
        return $uni->row();
	}
	
	function get_slider_imgs($clubid)
	{
		$imgs = $this->db->get_where('slider_images',array('club_id'=>$clubid,'status'=>1));
        return $imgs;
	}
	
	function get_if_joined($clubid,$userid)
	{
		$this->db->where('gclubid',$clubid);
		$this->db->where('gstudid',$userid);
		$this->db->where('permit',1);
		$q=$this->db->get_where('group_table');
		return $q;
	}
	function get_new_groups($uid)
	{
		$g = $this->db->get_where('group_table',array('gstudid'=>$uid,'permit'=>1));
		$joinedgrps = $g->result();
		$ids = array();
		//print_r($ids);
		foreach($joinedgrps as $i) 
		{
		 $ids[] = $i->gclubid;
		}

		//if user is new, and has no joined groups 
		if(empty($ids))
		{
			$this->db->select("*");
			$this->db->from('club_table');
			$this->db->where('permit',1);
			//$this->db->where_not_in('c_id', $ids);
			$query = $this->db->get();
			return $query->result();
		}
		else
		{
			$this->db->select("*");
			$this->db->from('club_table');
			$this->db->where('permit',1);
			$this->db->where_not_in('c_id', $ids);
			$query = $this->db->get();
			return $query->result();
		}
	}
	function remove_me_from_group($userid, $clubid)
	{
		$info=array('permit'=>0,'modified'=>date('Y-m-d'));
        $this->db->where('gstudid', $userid);
        $this->db->where('gclubid', $clubid);
        $query = $this->db->update('group_table',$info );
		return $query;
	}
	
	function getusertype($fbid)
	{
		$res = $this->db->get_where('user_table',array('fb_id'=>$fbid));
		return $res->row();
	}
	function make_leader($userid)
	{
		$data = array(
				'type' => 2, /*make user into leader*/
				'modified' => date('Y-m-d'),
				);
		$this->db->where('id', $userid);
		$query = $this->db->update('user_table',$data );
		return $query;
	}
	
	function make_leader_fb($user_fb_id)
	{
		$data = array(
				'type' => 2, /*make fb user into leader*/
				'modified' => date('Y-m-d'),
				);
		$this->db->where('fb_id', $user_fb_id);
		$query = $this->db->update('user_table',$data );
		return $query;
	}
	
	function add_user($data)
    {
		$this->db->insert('user_table',$data);
		return $this->db->insert_id();
		/*$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		return $row['LAST_INSERT_ID()'];*/       
    }
	
	function get_uni_groups($uni_id)
    {
        $query = $this->db->get_where('club_table',array('c_uni'=>$uni_id,'permit'=>1,'c_type'=>1));/*2 is group of type 3=type club*/
        return $query;
    }
    
    function get_uni_faculty($uni_id)
    {
        $query = $this->db->get_where('faculty_table',array('f_uni'=>$uni_id,'permit'=>1 ));
        $faculty = $query->result();
		$b 		= array();
		foreach($faculty as $row)
		{
			$query2 =  $this->db->get_where('club_table',array('c_type'=>3,'faculty_id'=>$row->f_id,'permit'=>1 ));/*2 is group of type 3=type faculty*/
			$b[] = $query2->row();
			
		}
		return $b;
    }
    
    function get_uni_hall($uni_id)
    {
        $query  = $this->db->get_where('hall_table',array('h_uni'=>$uni_id,'permit'=>1 ));
        $halls  = $query->result();
		$a 		= array();
		foreach($halls as $row)
		{
			$query2 =  $this->db->get_where('club_table',array('c_type'=>2,'hall_id'=>$row->h_id,'permit'=>1 ));/*2 is group of type 2=type hall*/
			$a[] = $query2->row();
			
		}
		return $a;
		
		
    }
	
	public function checkUser($data = array())
	{
		$this->db->select($this->primaryKey);
		$this->db->from($this->tableName);
		$this->db->where(array('fb_id' => $data['fb_id']));
		$prevQuery = $this->db->get();
		$prevCheck = $prevQuery->num_rows();
		
		if($prevCheck > 0){
			$prevResult = $prevQuery->row_array();
			$data['modified'] = date("Y-m-d H:i:s");
			$update = $this->db->update($this->tableName,$data,array('id'=>$prevResult['id']));
			$userID = 0;
		}else{
			$data['created'] = date("Y-m-d H:i:s");
			$data['modified'] = date("Y-m-d H:i:s");
			//$insert = $this->db->insert($this->tableName,$data);
			//$userID = $this->db->insert_id();
			$userID = 1;
		}

		return $userID;
    }
	
	function get_my_group_events($uid)
	{          
		echo $uid;
        $g = $this->db->get_where('group_table',array('gstudid'=>$uid,'permit'=>1,'type'=> 1));/* 'type'=>1: type=1 means he is not admin, so he sees only grps of which he has joined*/
        $groups = $g->result();
		$q= array();
		foreach($groups as $row)
		{
			$q = $this->db->get_where('event_table',array('e_by'=>$row->gclubid,'permit'=>1));
		}
		//return $q ->result();
		print_r($q->result);
	}
	function join_new_event($event_id)
	{
		$data = array(
			'e_id' => $event_id,
			'u_id' => $user_id,
			'created' => date(Y-m-d),
			);
		$this->db->insert('join_event_table',$data);
	}
	function getevent_sponsors($eid)
	{
		$q = $this->db->get_where('sponsor_event',array('eve_id'=>$eid,'permit'=>1));
		return $q->result();
		
		
	}

}
?>