<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Events extends MX_Controller
{
    public function __Construct() 
    {
        parent::__Construct();
        $this->load->model('mdl_events');
    }
	
	function studenthome()
	{
		$this->load->view('vw_adminheader');
		$this->load->view('vw_adminmenu');
		$this->load->view('vw_body');
		$this->load->view('vw_adminfooter');
	}
    public function index()
    {
        redirect('events/show_eve');
    }
    public function show_eve()
    {  
        $result = $this->mdl_events->get_eve();
        $res['data'] = $result['data'];
		$this->load->view('vw_adminheader');
		$this->load->view('vw_adminmenu');
		//$this->load->view('vw_adminhome');
		  $this->load->view("vw_searchEVE",$res);
		$this->load->view('vw_adminfooter');
		
    }
    
    function show_events($offset = 0)
    {
        $limit = 10;
        $result = $this->mdl_events->get_events($limit, $offset);
        $data['events'] = $result['events'];
        $data['rows'] = $result['rows'];
        //load pagination library
        $config = array();
        $config['base_url'] = base_url().'events/show_events';
        $config['total_rows'] = $data['rows'];
        $config['per_page'] = $limit;
        //Uri segment indicates pagination number
        $config['uri_segment'] = 3;
        $config['use_page_numbers'] = TRUE;
        //max links on a page will be shown
        $config['num_links'] = 5;
        //various pagination configuration
        $config['full_tag_open'] = '<div class="text-xs-right"><ul class="pagination text-right">';
        $config['full_tag_close'] = '</ul></div>';
        $config['first_tag_open'] = '<li class="page-item">';
        $config['first_tag_close'] = '</li>';
        $config['first_link'] = 'First';
        $config['last_tag_open'] = '<span class="last">';
        $config['last_tag_close'] = '</span>';
        $config['last_link'] = 'Last';
        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['cur_tag_open'] = '<li class="page-item active"><a href="#" class="page-link">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';
        $config['attributes'] = array('class' => 'page-link');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        
        $this->load->view('vw_adminheader');
        $this->load->view('vw_adminmenu');
        $this->load->view('vw_events', $data);
        $this->load->view('vw_adminfooter');

    }
     function add_event()
    { //echo json_encode(array('status' => $_POST));
         $this->form_validation->set_rules('e_name', 'Event Name', 'trim|required');
        $this->form_validation->set_rules('e_desc', 'Description', 'trim|required');
        $this->form_validation->set_rules('e_start', 'Start date', 'trim|required');
        $this->form_validation->set_rules('e_end', 'End date', 'trim|required');
        $this->form_validation->set_rules('e_loc', 'Location', 'trim|required');
        $this->form_validation->set_rules('e_type', 'Type ', 'trim|required');
        $this->form_validation->set_rules('e_fblink', 'Facebook', 'trim|required');
        $this->form_validation->set_rules('e_instalink', 'Instagram', 'trim|required');
        $this->form_validation->set_rules('e_by', 'Event by', 'trim|required');
        
        $data = null;
        if($this->form_validation->run() == false)
        {
             $data = array(
                'st'		    => 'fail',
                'e_name'       => form_error('e_name'),
                'e_desc'       => form_error('e_desc'),
                'e_start'      => form_error('e_start'),
                'e_end'        => form_error('e_end'),
                'e_loc'        => form_error('e_loc'),
                'e_type'       => form_error('e_type'),
                'e_fblink'     => form_error('e_fblink'),
                'e_instalink'   => form_error('e_instalink'),
				'e_by'          => form_error('e_by'),
                   );
		      echo json_encode($data);
        }
        else {
        $data = array(
            'e_name'  => $this->input->post('e_name'),
            'e_desc'  => $this->input->post('e_desc'),
            'e_start'  => $this->input->post('e_start'),
            'e_end'  => $this->input->post('e_end'),
            'e_loc'  => $this->input->post('e_loc'),
            'e_type'  => $this->input->post('e_type'),
            'e_fblink'  => $this->input->post('e_fblink'),
            'e_instalink'  => $this->input->post('e_instalink'),
			'e_by'  => $this->input->post('e_by'),
            'created'  => date('Y-m-d'),
                );
        $this->mdl_events->add_event($data);
        $return_json = array('st'=> 'success');
        echo json_encode($return_json);
        
        } 
    }
    public function event_page()
     {

          // Datatables Variables
          $draw = intval($this->input->get("draw"));
          $start = intval($this->input->get("start"));
          $length = intval($this->input->get("length"));


          $event = $this->mdl_events->get_eve();

          $data = array();

          foreach($student->result() as $r) {

               $data[] = array(
                    $r->e_name,
                    $r->e_desc,
                    $r->e_start,
                    $r->e_end ,
                    $r->e_loc ,
                    $r->e_type ,
                    $r->e_fblink ,
                    $r->e_instalink ,
					$r->e_by,
                   $r->created
					
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $event->num_rows(),
                 "recordsFiltered" => $event->num_rows(),
                 "data" => $data
            );
          echo json_encode($output);
          exit();
     }
    
   function validate_email($email) 
   {
	   if(! preg_match("/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/", $email))	
	   {
		   $this->form_validation->set_message('validate_email', 'Please enter a valid Email.');			
		   return FALSE;		
	   }else{
		   return TRUE;
		   }	
	}
    
    function update_event()
    {
          //  echo json_encode(array('st'=>$_POST));
        
        $this->form_validation->set_rules('e_name', 'Name', 'trim|required');
        $this->form_validation->set_rules('e_desc', 'Description', 'trim|required');
        $this->form_validation->set_rules('e_start', 'Start date', 'trim|required');
        $this->form_validation->set_rules('e_end', 'End date', 'trim|required');
        $this->form_validation->set_rules('e_loc', 'Loaction', 'trim|required');
        $this->form_validation->set_rules('e_type', 'Type', 'trim|required');
        $this->form_validation->set_rules('e_fblink', 'FaceBook Link', 'trim|required');
        $this->form_validation->set_rules('e_instalink', 'Instragram', 'trim|required');
        
      
        if($this->form_validation->run() == false)
        {
              $data = array(
                'st'		    => 'fail',
               'e_name'     => form_error('e_name'),
                'e_desc'    => form_error('e_desc'),
                'e_start'     => form_error('e_start'),
                'e_end'       => form_error('e_end'),
                'e_loc'       => form_error('e_loc'),
                'e_type'       => form_error('e_type'),
                'e_fblink'       => form_error('e_fblink'),
                'e_instalink'       => form_error('e_instalink'),
                
                
             
                );
          
		      echo json_encode($data);
            // echo json_encode(array('st'=>'fail',));
        }
        else {
            $id= $this->input->post('e_id');
            $data = array(
            'e_name'     => $this->input->post('e_name'),
            'e_desc'     => $this->input->post('e_desc'),
            'e_start'      => $this->input->post('e_start'),
            'e_end'    => $this->input->post('e_end'),
            'e_loc'    => $this->input->post('e_loc'),
            'e_type'    => $this->input->post('e_type'),
            'e_fblink'    => $this->input->post('e_fblink'),
            'e_instalink'    => $this->input->post('e_instalink'),
            'modified'      => date('Y-m-d'),    
                );
        $this->mdl_events->update_eventdata($id,$data);
            
        $return_json = array('st'=> 'success','name'=> $this->input->post('user_name'));
        echo json_encode($return_json);
        }
    }
    
    function delete_event()
    {
         $d = array(
            'e_id' => $this->input->post('id'),
            'e_name' => $this->input->post('name'),
                );
        $id= $this->input->post('id');
        $this->mdl_events->delete_event($id);
        echo json_encode($d);
    }
    
    
}
?>