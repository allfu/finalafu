<?php 
class Mdl_faculty extends CI_Model
{
   function get_students($limit, $offset) 
    {
        if($offset > 0) {
            $offset = ($offset - 1) * $limit;
        }
        $result['studs'] = $this->db->get('user_table', $limit, $offset);
        $result['rows'] = $this->db->count_all_results('user_table');
        return $result;
    }
    
	 public function get_faculty()
     {
          $query= $this->db->get("faculty_table");
          $res['data'] = $query->result();
         return $res;
     }
	 
    function add_faculty($data)
    {
        $this->db->insert('faculty_table',$data);
        $insert_id = $this->db->insert_id();
		return  $insert_id;
        
        
    }
    function update_faculty($id,$data)
    {
        $this->db->where('f_id', $id);
        $q = $this->db->update('faculty_table', $data);
        //alert("hieeee"+$q);
         //return $q->result();
    }
    
    function delete_faculty($id)
    {
        $info=array('permit'=>0,'modified'=>date('Y-m-d'));
        $this->db->where('f_id', $id);
        $this->db->update('faculty_table',$info );
    }
	
	
}
?>