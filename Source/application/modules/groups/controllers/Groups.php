<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Groups extends MX_Controller
{
    public function __Construct() 
    {
        parent::__Construct();
        $this->load->model('mdl_groups');
    }
    public function index()
    {
        redirect('groups/show_grp');
    }
    
    function show_grp()
    {
        
         $result = $this->mdl_groups->get_grp();
        $res['data'] = $result['data'];
		$this->load->view('vw_adminheader');
		$this->load->view('vw_adminmenu');
		//$this->load->view('vw_adminhome');
		  $this->load->view("vw_searchGRP",$res);
		$this->load->view('vw_adminfooter');
    }
    function show_groups($offset = 0)
    {
        $limit = 10;
        $result = $this->mdl_groups->get_groups($limit, $offset);
        $data['groups'] = $result['groups'];
        $data['rows'] = $result['rows'];
        //load pagination library
        $config = array();
        $config['base_url'] = base_url().'groups/show_groups';
        $config['total_rows'] = $data['rows'];
        $config['per_page'] = $limit;
        //Uri segment indicates pagination number
        $config['uri_segment'] = 3;
        $config['use_page_numbers'] = TRUE;
        //max links on a page will be shown
        $config['num_links'] = 5;
        //various pagination configuration
        $config['full_tag_open'] = '<div class="text-xs-right"><ul class="pagination text-right">';
        $config['full_tag_close'] = '</ul></div>';
        $config['first_tag_open'] = '<li class="page-item">';
        $config['first_tag_close'] = '</li>';
        $config['first_link'] = 'First';
        $config['last_tag_open'] = '<span class="last">';
        $config['last_tag_close'] = '</span>';
        $config['last_link'] = 'Last';
        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['cur_tag_open'] = '<li class="page-item active"><a href="#" class="page-link">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';
        $config['attributes'] = array('class' => 'page-link');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        
        $this->load->view('vw_adminheader');
        $this->load->view('vw_adminmenu');
        $this->load->view('vw_groups', $data);
        $this->load->view('vw_adminfooter');

    }
     function add_group()
    {
		//echo json_encode($_POST);		
        $this->form_validation->set_rules('c_name', 'Group name', 'trim|required');
        $this->form_validation->set_rules('c_uni', 'University name', 'trim|required');
        $this->form_validation->set_rules('c_by', 'Leader name', 'trim|required');
        $this->form_validation->set_rules('c_desc', 'Group Description', 'trim|required');
                
        $data = null;
        if($this->form_validation->run() == false)
        {
             $data = array(
                'st'		 => 'fail',
                'group_name' => form_error('c_name'),
                'group_uni'  => form_error('c_uni'),
                'group_lead' => form_error('c_by'),                
                'group_desc' => form_error('c_desc'),                
                     );
		      echo json_encode($data);
        }
        else {
		$filename = "default_logo.png";	 
		$path 	  = "assets/frontend/img/clublogos/".$filename;	
		$types 	  = array('image/jpeg', 'image/gif','image/png','image/jpg');
		if (!in_array($_FILES['logo']['type'], $types))
			{
				//echo "<script>alert('File type not allowed')</script>";
			   $ret = array('st'=> 'fail','st_code' => 3);//3 - file type error
			   echo json_encode($ret);
			}
		$sourcePath   =  $_FILES['logo']['tmp_name'];
		$filename     =  rand(0,10000).$_FILES['logo']['name'];
		$targetPath   =  "assets/frontend/img/clublogos/".$filename;
		move_uploaded_file($sourcePath, $targetPath);
		
        $data = array(
            'c_name'   => $this->input->post('c_name'),
            'c_uni'    => $this->input->post('c_uni'),
            'c_by'     => $this->input->post('c_by'),
            'c_desc'   => $this->input->post('c_desc'),
			'c_logo'   => $targetPath,
            'created'  => date('Y-m-d'),
                );
        $this->mdl_groups->add_group($data);
        $return_json = array('st'=> 'success');
        echo json_encode($return_json);
        
        }
    }
    public function group_page()
     {

          // Datatables Variables
          $draw   = intval($this->input->get("draw"));
          $start  = intval($this->input->get("start"));
          $length = intval($this->input->get("length"));


          $group  = $this->mdl_groups->get_grp();

          $data = array();

          foreach($group->result() as $r) {

               $data[] = array(
                    $r->c_name,
                    $r->created
                    
                  
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $group->num_rows(),
                 "recordsFiltered" => $group->num_rows(),
                 "data" => $data
            );
          echo json_encode($output);
          exit();
     }
   function validate_email($email) 
   {
	   if(! preg_match("/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/", $email))	
	   {
		   $this->form_validation->set_message('validate_email', 'Please enter a valid Email.');			
		   return FALSE;		
	   }else{
		   return TRUE;
		   }	
	}
    
    function update_group()
    {
		//echo json_encode(array('data' => $_POST,'files' => $_FILES));
        $this->form_validation->set_rules('editgroup_name', 'Group name', 'trim|required');
        $this->form_validation->set_rules('editgrp_uni', 'Univerity', 'trim|required');
        $this->form_validation->set_rules('editgrp_leader', 'Leader name', 'trim|required');
        $this->form_validation->set_rules('editgrp_desc', 'Group Description', 'trim|required');
        if($this->form_validation->run() == false)
        {
              $data = array(
                'st'	=> 'fail',
                'g_name'  => form_error('editgroup_name'),
                'g_uni'  => form_error('editgrp_uni'),
                'g_lead'  => form_error('editgrp_leader'),
                'g_desc'  => form_error('editgrp_desc'),
                  );
          
		      echo json_encode($data);
            // echo json_encode(array('st'=>'fail',));
        }
        else {
				
			$filename = "default_logo.png";	 
			$path 	  = "assets/frontend/img/clublogos/".$filename;	
			/* $types 	  = array('image/jpeg', 'image/gif','image/png','image/jpg');
			if (!in_array($_FILES['logo']['type'], $types))
				{
					//echo "<script>alert('File type not allowed')</script>";
				   $ret = array('st'=> 'fail','st_code' => 3);//3 - file type error
				   echo json_encode($ret);
				} */
			$sourcePath   =  $_FILES['logo']['tmp_name'];
			$filename     =  rand(0,10000).$_FILES['logo']['name'];
			$targetPath   =  "assets/frontend/img/clublogos/".$filename;
			move_uploaded_file($sourcePath, $targetPath);
				
            $id= $this->input->post('editgroup_id');
            $data = array(
            'c_name'     => $this->input->post('editgroup_name'),
            'c_uni'     => $this->input->post('editgrp_uni'),
            'c_by'     => $this->input->post('editgrp_leader'),
            'c_desc'     => $this->input->post('editgrp_desc'),
            'c_logo'     => $targetPath,
            'modified'  => date('Y-m-d'),    
                );
        $this->mdl_groups->update_groupdata($id,$data);
            
        $return_json = array('st'=> 'success','name'=> $this->input->post('editgroup_name'));
        echo json_encode($return_json);
        }
    }
    
    function delete_group()
    {
         $d = array(
            'g_id' => $this->input->post('c_id'),
            'c_name' => $this->input->post('c_name'),
             
                );
        $id= $this->input->post('c_id');
        $query = $this->mdl_groups->delete_group($id);
        echo json_encode($d);
    }
	
	function undelete_group()
	{
		$d = array(
            'g_id' => $this->input->post('c_id'),
            'c_name' => $this->input->post('c_name'),
             
                );
        $id= $this->input->post('c_id');
        $query = $this->mdl_groups->undelete_group($id);
        echo json_encode($d);
	}
    
    
}
?>