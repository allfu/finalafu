<?php 
class Mdl_Groups extends CI_Model
{
    function get_groups($limit, $offset) 
    {
        if($offset > 0) {
            $offset = ($offset - 1) * $limit;
        }
        $result['groups'] = $this->db->get('club_table', $limit, $offset);
        $result['rows'] = $this->db->count_all_results('club_table');
        return $result;
    }
    
    function get_grp()
    {
        $query= $this->db->get("club_table");
          $res['data'] = $query->result();
         return $res;
    }
    
    function add_group($data)
    {
        $this->db->insert('club_table',$data);
        
    }
    function update_groupdata($id,$data)
    {
        $this->db->where('c_id', $id);
        $q = $this->db->update('club_table', $data);
        //alert("hieeee"+$q);
         //return $q->result();
    }
    
    function delete_group($id)
    {
        $info=array('permit'=>0,'modified'=>date('Y-m-d'));
        $this->db->where('c_id', $id);
        $this->db->update('club_table',$info );
    }
	function undelete_group($id)
	{
		$info=array('permit'=>1,'modified'=>date('Y-m-d'));
        $this->db->where('c_id', $id);
        $this->db->update('club_table',$info );
	}
}
?>