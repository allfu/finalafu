<?php 
class Mdl_hall extends CI_Model
{
   function get_students($limit, $offset) 
    {
        if($offset > 0) {
            $offset = ($offset - 1) * $limit;
        }
        $result['studs'] = $this->db->get('user_table', $limit, $offset);
        $result['rows'] = $this->db->count_all_results('user_table');
        return $result;
    }
    
	 public function get_hall()
     {
          $query= $this->db->get("hall_table");
          $res['data'] = $query->result();
         return $res;
     }
	 
    function add_hall($data)
    {
        $this->db->insert('hall_table',$data);
        $insert_id = $this->db->insert_id();
		return  $insert_id;
        
        
    }
    function update_hall($id,$data)
    {
        $this->db->where('h_id', $id);
        $q = $this->db->update('hall_table', $data);
        //alert("hieeee"+$q);
         //return $q->result();
    }
    
    function delete_hall($id)
    {
        $info=array('permit'=>0,'modified'=>date('Y-m-d'));
        $this->db->where('h_id', $id);
        $this->db->update('hall_table',$info );
    }
	
	
}
?>