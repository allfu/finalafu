<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12">
                <h1>Hall List</h1>
                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <!--<li><a href="#" data-toggle="modal" data-target="#add-product-modal"><i class="icon-plus4"></i> Add Client   </a></li>-->
                                        <li><button type="button" class="btn btn-info btn-block"
                                                    data-toggle="modal" data-target="#add-hall-modal">
                                            <i class="icon-plus4" style="color:white;"></i>   Add Hall</button>
                                        </li>
                                        <!--<li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                                        <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                                        <li><a data-action="reload"><i class="icon-reload"></i></a></li>-->
                                    </ul>
                                </div>
        <table id="hall-table" class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>Hall Name</th>
                    <th>Permit</th>
                    <th>Actions</th>
                </tr>
                
            </thead>
            <tbody>
                <?php foreach($data as $row)   
                                              {
                                                        ?>
                                                        <tr>
                                                        <td class="text-middle"><?php echo $row->h_name; ?></td>
                                                        
                                                       
                                                        <td class="text-middle"><?php if($row->permit == '1')  { ?>
                                                        <span class="tag tag-success">Active</span><?php } else { ?>
                                                        <span class="tag tag-danger">Banned</span>
                                                        <?php } ?></td>
                                                        <td>
                                                            <span class="btn-group">
                                                            <!--<input type="button" class="btn btn-outline-info" value="View" data-toggle="modal" data-target="#view-modal"/>-->
															
                                                                
                                                            <input type="button" class="btn btn-outline-primary" value="Edit" 
                                                            data-toggle="modal" data-target="#edit-hall-modal" 
                                                            data-id="<?php echo $row->h_id;?>" data-name="<?php echo $row->h_name; ?>"  />

                                                            <input type="button" class="btn btn-outline-danger" 
                                                                   <?php 
                                                                    if($row->permit==0)
                                                                    { echo "value='Deleted'";
                                                                    }
                                                                   else{ 
                                                                    echo "value='Delete'"; ?>
                                                                   data-toggle="modal" data-target="#delete-hall-modal" 
                                                                   data-id="<?php echo $row->h_id;?>" 
                                                                   data-hname="<?php echo $row->h_name; ?>" />
                                                                   <?php } ?>
                                                            </span>
                                                        </td>
                </tr>
                <?php } ?>
            </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!--Add Modal-->
<div class="modal fade text-xs-left" id="add-hall-modal" tabindex="-1" role="dialog" aria-labelledby="addModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Add Hall Information</h4>
            </div>
            <form action="<?php //echo base_url().'clients/add_client';?>" method="post" id="addhall_form">
                <div class="modal-body">
                    <label>Hall Name:</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="h_name" value="<?php echo set_value('h_name');?>" id="h_name" />
                        <p id="h_name_error" style="color:red;"></p>
                    </div>
                   	
                </div>
                <div class="modal-footer">
                    <input type="reset" class="btn grey btn-outline-secondary" data-dismiss="modal" value="Cancel">
                    <input type="submit" class="btn btn-outline-primary" value="Submit" />
                </div>
            </form>
        </div>
    </div>
</div>
<!--Edit Modal-->
<div class="modal fade text-xs-left" id="edit-hall-modal" tabindex="-1" role="dialog" aria-labelledby="editModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Edit Hall Information</h4>
            </div>
            <form method="post" id="edithallform" name="edithallform" action="">
                <div class="modal-body">
                    <input type="hidden" name="edithall_id" id="edithall_id" value="<?php echo set_value('edithall_id');?>" /><!--value will be set through javascript-->
                    <label>Hall Name:</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="edit_h_name" id="edit_h_name" value="<?php echo set_value('edit_h_name');?>" />
                    </div>
                    <p id="h_name_error" style="color:red;"></p>
                   </div>
                <div class="modal-footer">
                    <input type="reset" class="btn grey btn-outline-secondary" data-dismiss="modal" value="close">
                    <input type="submit" class="btn btn-outline-primary updatehall_submit" value="Submit" />
                </div>
            </form><p></p>
        </div>
    </div>
</div>
<!--Delete Modal-->
<div class="modal fade text-xs-left" id="delete-hall-modal" tabindex="-1" role="dialog" aria-labelledby="deleteModal" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <form action="<?php echo base_url().'hall_table/delete'; ?>" method="POST" class="delete-form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Delete Hall</h4>
                </div>
                <div class="modal-body deletepara">
                    <p></p> <!--client information will go through here to jscript-->
                    <input type="hidden" name="del_uid" id="del_uid" value="" /><!--value will be set through javascript-->
                    <input type="hidden" name="del_uname" id="del_uname" value="" /><!--value will be set through javascript-->
                    <div id="result">
                        <div id="value"></div>
                        <div id="anothervalue"></div>
                    </div>
                </div>
                </form>
                <div class="modal-footer">
                    <input type="reset" class="btn grey btn-outline-secondary" data-dismiss="modal" value="No">
                    <input type="submit" class="btn grey btn-outline-danger delete-hall-submit" value="Yes">
                </div>
            </div>
    </div>
</div>


<script>

//DELETE STUDENT SCRIPT
$('#delete-hall-modal').on('show.bs.modal', function(e) {
   
    //get data-id attribute of the clicked element
    var deluid = $(e.relatedTarget).data('id');
    var deluname = $(e.relatedTarget).data('hname');
    var modal = $(this);
   
    //populate the textboxes
    $(e.currentTarget).find('input[name="del_uid"]').val(deluid);
    document.getElementById('del_uname').value = deluname;
     modal.find('.modal-body p').html('Do you really want to delete - <b>' + deluname + '</b>?');
   
});

$(document).ready(function()	{
    $(".delete-hall-submit").click(function(event)	{
       event.preventDefault();
       
       var h_id = document.getElementById('del_uid').value;
        var h_name = document.getElementById('del_uname').value;
        
           jQuery.ajax({
               type: "POST",
               url: "<?php echo site_url('hall/delete_hall'); ?>",
               dataType: "json",
               data: {uid: h_id, uname: h_name},
               success: function(res)	{
                   console.log(res);
                   if(res)
                       {   
                           alert('Hall Deleted Successfully');
                           //$('#delete-client-modal').modal().hide();
                           window.location.href = "<?php echo site_url('hall'); ?>";

                       }
                   else{
                       alert('Delete not successful');
                       window.location.href = "<?php echo site_url('hall'); ?>";
                   }
                   }
               });
           });
       });
</script>


<script type="text/javascript">
//ADD STUDENT SCRIPT
$(function() { // <----------------missed the doc ready function
    $('#addhall_form').submit(function(e) 
                                    {
        e.preventDefault(); // <------this will restrict the page refresh
        var form_data = {
            h_name: $('#h_name').val(),
            
        };
        //alert(form_data.stud_fname);
        $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + 'hall/add_hall',
                data: form_data,
                success: function(res)  {
                    var json = JSON.parse(res);
                    console.log(json);
                     if(json.st== 'fail'){
                        $('#h_name_error').html(json.h_name);
                                                        
                    }
                   
                   if(json.st =='success'){
                        
                        alert('Hall added successfully');
                        window.location.href = "<?php echo site_url('hall'); ?>";
                    }
                   
                }
        });

    });

});
</script>

<script>
//EDIT STUDENT SCRIPT
//triggered when modal is about to be shown
$('#edit-hall-modal').on('show.bs.modal', function(e) {
   
    //get data-id attribute of the clicked element
    var edithall_id = $(e.relatedTarget).data('id');
    var edit_h_name = $(e.relatedTarget).data('name');
    
    
    //populate the textboxes
    $(e.currentTarget).find('input[name="edithall_id"]').val(edithall_id);
    document.getElementById('edit_h_name').value = edit_h_name;
    
    
});

$(document).ready(function()	{
$(".updatehall_submit").click(function(e)	{
        e.preventDefault(); //this will restrict the page refresh
        var form_data = {
            h_id:$('#edithall_id').val(),
            h_name: $('#edit_h_name').val(),
            
        };
    //alert(form_data.stud_id+" id "+form_data.stud_uname + form_data.stud_email + form_data.stud_website);
    //var form1 = new FormData(this);working till here.
    //var form2 = $('#editclient_form').serialize();
    $.ajax({
        type: "POST",
        //data:form_data,
        data: {
             h_id: form_data.h_id, 
            h_name: form_data.h_name, 
            
        },
        
        url: "<?php echo site_url('hall/update_hall'); ?>",
        
        success: function(res)  {
        var json = JSON.parse(res);
        console.log(json);
        //alert('herehere');
        
        if(json.st== 'fail'){
                $('#h_name_error').html(json.h_name);
                 
                
                }
        if(json.st =='success') {
               
                console.log(json);
                alert('Hall updated successfully');
                window.location.href = "<?php echo site_url('hall'); ?>";
                }
        }
        });
    });
});
</script>
<script>
    $(document).ready(function() {
        
        $('#hall-table').DataTable();
    });
</script>