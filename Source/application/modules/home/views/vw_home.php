<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="SemiColonWeb" />

    <!-- Stylesheets
	============================================= -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url().'assets/landing_page/css/bootstrap.css'; ?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url().'assets/landing_page/style.css'; ?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url().'assets/landing_page/css/swiper.css'; ?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url().'assets/landing_page/css/dark.css'; ?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url().'assets/landing_page/css/font-icons.css'; ?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url().'assets/landing_page/css/animate.css'; ?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url().'assets/landing_page/css/magnific-popup.css'; ?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url().'assets/landing_page/css/responsive.css'; ?>" type="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- Document Title
	============================================= -->
    <title>AFU</title>


    <style>
        .slider-caption, .nivo-caption, .flex-caption {
            position: absolute;
            top: 0;
            /*left: 15px;
            right: 15px;*/
            z-index: 20;
            min-width: 100%;
            /*min-width: 1300px;*/
            color: #EEE;
            font-size: 20px;
            font-weight: 300;
            text-shadow: 1px 1px 1px rgba(0,0,0,0.15);
            -webkit-transition: top .3s ease;
            -o-transition: top .3s ease;
            transition: top .3s ease;
            padding: 20px 60px 0px 20px;
        }

        .content-wrap {
            position: relative;
            padding: 0px 0 20px 0;
        }

        .owl-carousel .owl-nav [class*="owl-"] {
            position: absolute;
            top: 29%;
            margin-top: -18px;
            left: -36px;
            zoom: 1;
            width: 36px;
            height: 36px;
            line-height: 32px;
            border: 1px solid rgba(0,0,0,0.2);
            color: #666;
            background-color: #FFF;
            font-size: 18px;
            border-radius: 50%;
            opacity: 0;
            -webkit-transition: all .3s ease;
            -o-transition: all .3s ease;
            transition: all .3s ease;
        }

        .dark #header.transparent-header.full-header:not(.sticky-header) #header-wrap:not(.not-dark) #primary-menu > ul, #header.transparent-header.full-header.dark:not(.sticky-header) #header-wrap:not(.not-dark) #primary-menu > ul {
            border-right-color: rgba(255,255,255,0.0);
        }

        #header.full-header #primary-menu > ul {
            float: left;
            padding-right: 15px;
            margin-right: 15px;
            border-right: 1px solid #EEE;
            border-right-color: rgba(255,255,255,0.0);
        }

        #header.transparent-header.full-header #primary-menu > ul {
            border-right-color: rgba(0,0,0,0.0);
        }

        .dark #header-wrap:not(.not-dark) #primary-menu > ul > li:hover > a, .dark #header-wrap:not(.not-dark) #primary-menu > ul > li.current > a, .dark #header-wrap:not(.not-dark) #primary-menu > .container > ul > li:hover > a, .dark #header-wrap:not(.not-dark) #primary-menu > .container > ul > li.current > a {
            color: #fff;
            text-decoration: underline !important;
        }
    </style>

    <style>
        @media only screen and (min-device-width : 320px) and (max-device-width : 480px) {
            .swiper_wrapper:not(.force-full-screen), .swiper_wrapper:not(.force-full-screen):not(.canvas-slider-grid) .swiper-slide, .ei-slider {
                height: 970px !important;
            }

            .slider-caption {
                top: 55.5px !important;
            }
        }
    </style>

</head>

<body class="stretched">

    <!-- Document Wrapper
	============================================= -->
    <div id="wrapper" class="clearfix">

        <!-- Header
		============================================= -->
        <header id="header" class="transparent-header full-header" data-sticky-class="not-dark">

            <div id="header-wrap">

                <div class="container clearfix">

                    <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

                    <!-- Logo
					============================================= -->
                    <div id="logo">
                        <a href="#" class="standard-logo" data-dark-logo="images/logo-dark1.png">
                            <img src="images1/logo.png" alt="AFU" style="display: none"></a>
                        <a href="#" class="retina-logo" data-dark-logo="images/logo-dark@2x1.png">
                            <img src="images1/logo@2x1.png" alt="AFU" style="display: none"></a>
                    </div>
                    <!-- #logo end -->

                    <!-- Primary Navigation
					============================================= -->
                    <nav id="primary-menu" class="dark">
                        <ul>
                            <li><a href="<?php echo base_url().'user_authentication'; ?>">
                                <div>Log In</div>
                            </a>
                            </li>
                            <li><a href="<?php echo base_url().'user_authentication'; ?>">
                                <div>Sign Up</div>
                            </a>
                            </li>
                            <li><a href="shop.html">
                                <div>Contact Us</div>
                            </a>
                            </li>
                            <li><a href="shop.html">
                                <div>FAQ</div>
                            </a>
                            </li>
                        </ul>
                    </nav>
                    <!-- #primary-menu end -->

                </div>

            </div>

        </header>
        <!-- #header end -->

        <section id="slider" class="slider-parallax swiper_wrapper full-screen clearfix">
            <div class="slider-parallax-inner">

                <div class="swiper-container swiper-parent">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide dark" style="background-image: url('../../../assets/landing_page/images/slider/swiper/1.jpg');">
                            <div class="container-fluid clearfix">
                                <div class="slider-caption">
                                    <div class="row">

                                        <div class="col-md-4 col-sm-4 col-xs-12 bottommargin">
                                            <div class="ipost clearfix" style="border: 1px solid #fff; width: 100%">
                                                <div class="entry-image">
                                                    <a href="#">
                                                        <img class="image_fade" src="<?php echo base_url().'assets/landing_page/images/magazine/thumb/1.jpg'; ?>" alt="Image"></a>
                                                </div>
                                                <div class="entry-title" style="text-align: center">
                                                    <h3><a href="blog-single.html">Student</a></h3>
                                                </div>

                                                <div class="entry-content" style="text-align: center">
                                                    <p>Reach out hundreds of sponsors within seconds.</p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4 col-sm-4 col-xs-12 bottommargin">
                                            <div class="ipost clearfix" style="border: 1px solid #fff">
                                                <div class="entry-image">
                                                    <a href="#">
                                                        <img class="image_fade" src="<?php echo base_url().'assets/landing_page/images/magazine/thumb/2.jpg';?>" alt="Image"></a>
                                                </div>
                                                <div class="entry-title" style="text-align: center">
                                                    <h3><a href="blog-single.html">Organizer</a></h3>
                                                </div>

                                                <div class="entry-content" style="text-align: center">
                                                    <p>Host and plan your events using customize tools.</p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4 col-sm-4 col-xs-12 bottommargin">
                                            <div class="ipost clearfix" style="border: 1px solid #fff">
                                                <div class="entry-image">
                                                    <a href="#">
                                                        <img class="image_fade" src="<?php echo base_url().'assets/landing_page/images/magazine/thumb/3.jpg'; ?>" alt="Image"></a>
                                                </div>
                                                <div class="entry-title" style="text-align: center">
                                                    <h3><a href="blog-single.html">Sponsor</a></h3>
                                                </div>

                                                <div class="entry-content" style="text-align: center">
                                                    <p>Manage your vendors and participants seamlessly.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="swiper-slide" style="background-image: url('../../../assets/landing_page/images/slider/swiper/3.jpg'); background-position: center top;">
                            <div class="container clearfix">
                                <div class="slider-caption">
                                    <h2 data-caption-animate="fadeInUp">Great Performance</h2>
                                    <p data-caption-animate="fadeInUp" data-caption-delay="200">You'll be surprised to see the Final Results of your Creation &amp; would crave for more.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--  <div id="slider-arrow-left"><i class="icon-angle-left"></i></div>
                    <div id="slider-arrow-right"><i class="icon-angle-right"></i></div>-->
                </div>

                <a href="#" data-scrollto="#content" data-offset="100" class="dark one-page-arrow"><i class="icon-angle-down infinite animated fadeInDown"></i></a>

            </div>
        </section>

        <!-- Content
		============================================= -->
        <section id="content">
            <div class="content-wrap">
                <div class="section" style="margin: 0px 0; padding: 60px 0">
                    <div class="container clearfix">
                        <div class="row topmargin-sm">
                            <div class="heading-block center nomargin">
                                <h3>Our Profile</h3>
                            </div>
                            <br />
                            <div class="col-md-4 col-sm-6 dark center col-padding" style="background-color: #515875;">
                                <div>
                                    <i class="i-plain i-xlarge divcenter icon-line2-directions"></i>
                                    <div class="counter counter-lined"><span data-from="100" data-to="87" data-refresh-interval="50" data-speed="2000"></span>K</div>
                                    <h5>Joined On Our Platforms</h5>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-6 dark center col-padding" style="background-color: #576F9E;">
                                <div>
                                    <i class="i-plain i-xlarge divcenter icon-line2-graph"></i>
                                    <div class="counter counter-lined"><span data-from="3000" data-to="315" data-refresh-interval="100" data-speed="2500"></span></div>
                                    <h5>Events Sponsored</h5>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-6 dark center col-padding" style="background-color: #6697B9;">
                                <div>
                                    <i class="i-plain i-xlarge divcenter icon-line2-layers"></i>
                                    <div class="counter counter-lined"><span data-from="10" data-to="134200" data-refresh-interval="25" data-speed="3500"></span></div>
                                    <h5>Items Where Given Out</h5>
                                </div>
                            </div>
                            <br />
                            <div class="heading-block center nomargin">
                                <h3>
                                    <button class="button button-border button-dark button-rounded button-large noleftmargin topmargin-sm">Join Now</button></h3>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clear"></div>

                <div class="section nobottommargin" style="background-color: #fff">
                    <div class="container clear-bottommargin clearfix">

                        <div class="row topmargin-sm clearfix">

                            <div class="col-md-4 bottommargin center">
                                <div style="text-align: center">
                                    <i class="i-plain color i-large icon-line2-screen-desktop inline-block"></i>
                                </div>
                                <div class="heading-block nobottomborder" style="margin: 35px 0 15px">
                                    <!-- <span class="before-heading">Scalable on Devices.</span>-->
                                    <h4>Responsive &amp; Retina</h4>
                                </div>
                                <p>Employment respond committed meaningful fight against oppression social challenges rural legal aid governance. Meaningful work, implementation, process cooperation, campaign inspire.</p>
                            </div>

                            <div class="col-md-4 bottommargin center">
                                <div style="text-align: center">
                                    <i class="i-plain color i-large icon-line2-energy inline-block"></i>
                                </div>
                                <div class="heading-block nobottomborder" style="margin: 35px 0 15px">
                                    <!-- <span class="before-heading">Smartly Coded &amp; Maintained.</span>-->
                                    <h4>Powerful Performance</h4>
                                </div>
                                <p>Medecins du Monde Jane Addams reduce child mortality challenges Ford Foundation. Diversification shifting landscape advocate pathway to a better life rights international. Assessment.</p>
                            </div>

                            <div class="col-md-4 bottommargin center">
                                <div style="text-align: center">
                                    <i class="i-plain color i-large icon-line2-equalizer inline-block"></i>
                                </div>
                                <div class="heading-block nobottomborder" style="margin: 35px 0 15px">
                                    <!--  <span class="before-heading">Flexible &amp; Customizable.</span>-->
                                    <h4>Truly Multi-Purpose</h4>
                                </div>
                                <p>Democracy inspire breakthroughs, Rosa Parks; inspiration raise awareness natural resources. Governance impact; transformative donation philanthropy, respect reproductive.</p>
                            </div>

                        </div>

                    </div>
                </div>

                <div class="section">
                    <div class="container clearfix">
                        <div class="row topmargin-sm">
                            <div class="heading-block center nomargin">
                                <h3>Our Featured Events</h3>
                            </div>
                            <br />
                            <br />

                            <div class="col-md-3 col-sm-6 bottommargin">
                                <div class="ipost clearfix">
                                    <div class="entry-image">
                                        <a href="#">
                                            <img class="image_fade" src="<?php echo base_url().'assets/landing_page/images/magazine/thumb/1.jpg';?>" alt="Image">
                                            <!--<img class="image_fade" src="images/magazine/thumb/1.jpg" alt="Image">-->
                                        </a>
                                    </div>
                                    <div class="entry-title">
                                        <h3><a href="blog-single.html">Bloomberg smart cities; change-makers economic security</a></h3>
                                    </div>

                                    <div class="entry-content">
                                        <p>Prevention effect, advocate dialogue rural development lifting people up community civil society. Catalyst, grantees leverage.</p>
                                    </div>
                                    <br />
                                    <div class="product-rating" style="color: #FFD700; font-size: 20px">
                                        <i class="icon-star3"></i>
                                        <i class="icon-star3"></i>
                                        <i class="icon-star3"></i>
                                        <i class="icon-star3"></i>
                                        <i class="icon-star-half-full"></i>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-6 bottommargin">
                                <div class="ipost clearfix">
                                    <div class="entry-image">
                                        <a href="#">
                                            <img class="image_fade" src="<?php echo base_url().'assets/landing_page/images/magazine/thumb/2.jpg';?>" alt="Image">
                                        </a>
                                    </div>
                                    <div class="entry-title">
                                        <h3><a href="blog-single.html">Medicine new approaches communities, outcomes partnership</a></h3>
                                    </div>

                                    <div class="entry-content">
                                        <p>Cross-agency coordination clean water rural, promising development turmoil inclusive education transformative community.</p>
                                    </div>
                                    <br />
                                    <div class="product-rating" style="color: #FFD700; font-size: 20px">
                                        <i class="icon-star3"></i>
                                        <i class="icon-star3"></i>
                                        <i class="icon-star3"></i>
                                        <i class="icon-star3"></i>
                                        <i class="icon-star-half-full"></i>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-6 bottommargin">
                                <div class="ipost clearfix">
                                    <div class="entry-image">
                                        <a href="#">
                                            <img class="image_fade" src="<?php echo base_url().'assets/landing_page/images/magazine/thumb/3.jpg';?>" alt="Image"></a>
                                    </div>
                                    <div class="entry-title">
                                        <h3><a href="blog-single.html">Significant altruism planned giving insurmountable challenges liberal</a></h3>
                                    </div>

                                    <div class="entry-content">
                                        <p>Micro-finance; vaccines peaceful contribution citizens of change generosity. Measures design thinking accelerate progress medical initiative.</p>
                                    </div>
                                    <br />
                                    <div class="product-rating" style="color: #FFD700; font-size: 20px">
                                        <i class="icon-star3"></i>
                                        <i class="icon-star3"></i>
                                        <i class="icon-star3"></i>
                                        <i class="icon-star3"></i>
                                        <i class="icon-star-half-full"></i>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-6 bottommargin">
                                <div class="ipost clearfix">
                                    <div class="entry-image">
                                        <a href="#">
                                            <img class="image_fade" src="<?php echo base_url().'assets/landing_page/images/magazine/thumb/4.jpg';?>" alt="Image"></a>
                                    </div>
                                    <div class="entry-title">
                                        <h3><a href="blog-single.html">Compassion conflict resolution, progressive; tackle</a></h3>
                                    </div>

                                    <div class="entry-content">
                                        <p>Community health workers best practices, effectiveness meaningful work The Elders fairness. Our ambitions local solutions globalization.</p>
                                    </div>
                                    <br />
                                    <div class="product-rating" style="color: #FFD700; font-size: 20px">
                                        <i class="icon-star3"></i>
                                        <i class="icon-star3"></i>
                                        <i class="icon-star3"></i>
                                        <i class="icon-star3"></i>
                                        <i class="icon-star-half-full"></i>
                                    </div>
                                </div>
                            </div>

                            <br />
                            <div class="col-md-12">
                                <div class="heading-block center nomargin">
                                    <h3>
                                        <button class="button button-border button-dark button-rounded button-large noleftmargin topmargin-sm">See All</button></h3>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>



                <br />
                <br />
                <div class="container clearfix">
                    <div class="col_one_third bottommargin-sm center">
                        <img data-animate="fadeInLeft" src="<?php echo base_url().'assets/landing_page/images/iphone6.jpg'; ?>" class="img-rounded">
                    </div>
                    <div class="col_two_third bottommargin-sm col_last">

                        <div class="heading-block topmargin-sm">
                            <h3>Welcome To AllForU</h3>
                        </div>

                        <p>We take the guesswork out for orgnizers by analysing the behaviour of our event participants to millions of others to guide event planning and fulfilment.</p>

                        <p>Hence , organisers can truly focus on delivering the best experience for their audience.</p>

                        <a href="#" class="button button-border button-dark button-rounded button-large noleftmargin topmargin-sm">Learn more</a>

                    </div>
                </div>
                <div class="container clearfix">


                    <div class="fancy-title title-center title-dotted-border topmargin">
                        <h3>What others say</h3>
                    </div>

                    <div id="oc-portfolio" class="owl-carousel portfolio-carousel carousel-widget" data-margin="20" data-nav="true" data-pagi="false" data-items-xxs="1" data-items-xs="2" data-items-sm="3" data-items-md="4">

                        <div class="oc-item">
                            <div class="iportfolio">
                                <div class="portfolio-image">
                                    <a href="portfolio-single.html">
                                        <img src="<?php echo base_url().'assets/landing_page/images/portfolio/4/1.jpg'; ?>" alt="Open Imagination" style="height: 200px">
                                    </a>
                                    <div class="portfolio-overlay">
                                        <a href="<?php echo base_url().'assets/landing_page/images/portfolio/full/1.jpg'; ?>" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
                                        <a href="portfolio-single.html" class="right-icon"><i class="icon-line-ellipsis"></i></a>
                                    </div>
                                </div>
                                <div class="portfolio-desc">
                                    <h3><a href="portfolio-single.html">James Jelki</a></h3>
                                    <span>"Incidunt deleniti blanditiis quas aperiam recusandae consequatur ullam quibusdam cum libero illo rerum!" </span>
                                </div>
                            </div>
                        </div>

                        <div class="oc-item">
                            <div class="iportfolio">
                                <div class="portfolio-image">
                                    <a href="portfolio-single.html">
                                        <img src="<?php echo base_url().'assets/landing_page/images/portfolio/4/2.jpg'; ?>" alt="Locked Steel Gate" style="height: 200px">
                                    </a>
                                    <div class="portfolio-overlay">
                                        <a href="images/portfolio/full/2.jpg" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
                                        <a href="portfolio-single.html" class="right-icon"><i class="icon-line-ellipsis"></i></a>
                                    </div>
                                </div>
                                <div class="portfolio-desc">
                                    <h3><a href="portfolio-single.html">Locked Steel Gate</a></h3>
                                    <span>"Incidunt deleniti blanditiis quas aperiam recusandae consequatur ullam quibusdam cum libero illo rerum!" </span>
                                </div>
                            </div>
                        </div>

                        <div class="oc-item">
                            <div class="iportfolio">
                                <div class="portfolio-image">
                                    <a href="#">
                                        <img src="<?php echo base_url().'assets/landing_page/images/portfolio/4/3.jpg'; ?>" alt="Mac Sunglasses" style="height: 200px">
                                    </a>
                                    <div class="portfolio-overlay">
                                        <a href="http://vimeo.com/89396394" class="left-icon" data-lightbox="iframe"><i class="icon-line-play"></i></a>
                                        <a href="portfolio-single-video.html" class="right-icon"><i class="icon-line-ellipsis"></i></a>
                                    </div>
                                </div>
                                <div class="portfolio-desc">
                                    <h3><a href="portfolio-single-video.html">Mac Sunglasses</a></h3>
                                    <span>"Incidunt deleniti blanditiis quas aperiam recusandae consequatur ullam quibusdam cum libero illo rerum!" </span>
                                </div>
                            </div>
                        </div>

                        <div class="oc-item">
                            <div class="iportfolio">
                                <div class="portfolio-image">
                                    <a href="portfolio-single.html">
                                        <img src="<?php echo base_url().'assets/landing_page/images/portfolio/4/1.jpg'; ?>" alt="Console Activity" style="height: 200px">
                                    </a>
                                    <div class="portfolio-overlay">
                                        <a href="<?php echo base_url().'assets/landing_page/images/portfolio/full/5.jpg'; ?>" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
                                        <a href="portfolio-single.html" class="right-icon"><i class="icon-line-ellipsis"></i></a>
                                    </div>
                                </div>
                                <div class="portfolio-desc">
                                    <h3><a href="portfolio-single.html">Console Activity</a></h3>
                                    <span>"Incidunt deleniti blanditiis quas aperiam recusandae consequatur ullam quibusdam cum libero illo rerum!" </span>
                                </div>
                            </div>
                        </div>

                        <div class="oc-item">
                            <div class="iportfolio">
                                <div class="portfolio-image">
                                    <a href="portfolio-single-video.html">
                                        <img src="<?php echo base_url().'assets/landing_page/images/portfolio/4/3.jpg'; ?>" alt="Backpack Contents" style="height: 200px">
                                    </a>
                                    <div class="portfolio-overlay">
                                        <a href="http://www.youtube.com/watch?v=kuceVNBTJio" class="left-icon" data-lightbox="iframe"><i class="icon-line-play"></i></a>
                                        <a href="portfolio-single-video.html" class="right-icon"><i class="icon-line-ellipsis"></i></a>
                                    </div>
                                </div>
                                <div class="portfolio-desc">
                                    <h3><a href="portfolio-single-video.html">Backpack Contents</a></h3>
                                    <span>"Incidunt deleniti blanditiis quas aperiam recusandae consequatur ullam quibusdam cum libero illo rerum!" </span>
                                </div>
                            </div>
                        </div>

                        <div class="oc-item">
                            <div class="iportfolio">
                                <div class="portfolio-image">
                                    <a href="portfolio-single.html">
                                        <img src="<?php echo base_url().'assets/landing_page/images/portfolio/4/2.jpg'; ?>" alt="Sunset Bulb Glow" style="height: 200px">
                                    </a>
                                    <div class="portfolio-overlay">
                                        <a href="<?php echo base_url().'assets/landing_page/images/portfolio/full/8.jpg'; ?>" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
                                        <a href="portfolio-single.html" class="right-icon"><i class="icon-line-ellipsis"></i></a>
                                    </div>
                                </div>
                                <div class="portfolio-desc">
                                    <h3><a href="portfolio-single.html">Sunset Bulb Glow</a></h3>
                                    <span>"Incidunt deleniti blanditiis quas aperiam recusandae consequatur ullam quibusdam cum libero illo rerum!" </span>
                                </div>
                            </div>
                        </div>

                        <div class="oc-item">
                            <div class="iportfolio">
                                <div class="portfolio-image">
                                    <a href="portfolio-single-video.html">
                                        <img src="<?php echo base_url().'assets/landing_page/images/portfolio/4/1.jpg'; ?>" alt="Study Table" style="height: 200px">
                                    </a>
                                    <div class="portfolio-overlay">
                                        <a href="http://vimeo.com/91973305" class="left-icon" data-lightbox="iframe"><i class="icon-line-play"></i></a>
                                        <a href="portfolio-single-video.html" class="right-icon"><i class="icon-line-ellipsis"></i></a>
                                    </div>
                                </div>
                                <div class="portfolio-desc">
                                    <h3><a href="portfolio-single-video.html">Study Table</a></h3>
                                    <span>"Incidunt deleniti blanditiis quas aperiam recusandae consequatur ullam quibusdam cum libero illo rerum!" </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <br />
        <br />
        <!-- #content end -->

        <!-- Footer
		============================================= -->
        <footer id="footer" class="dark" style="font-size: 20px; color: #fff">
            <!-- Copyrights
			============================================= -->
            <div id="copyrights">

                <div class="container clearfix">
                    <div class="row">
                        <div class="col-md-12">
                            <div style="text-align: center; font-size: 16px; color: #fff">
                                Copyrights &copy; 2017 All Rights Reserved by AFU.<br>
                            </div>

                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <!-- #copyrights end -->
        </footer>
        <!-- #footer end -->

    </div>
    <!-- #wrapper end -->

    <!-- Go To Top
	============================================= -->
    <div id="gotoTop" class="icon-angle-up"></div>

    <!-- External JavaScripts
	============================================= -->
    <script type="text/javascript" src="<?php echo base_url().'assets/landing_page/js/jquery.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets/landing_page/js/plugins.js'; ?>"></script>

    <!-- Footer Scripts
	============================================= -->
    <script type="text/javascript" src="<?php echo base_url().'assets/landing_page/js/functions.js'; ?>"></script>

</body>
</html>

