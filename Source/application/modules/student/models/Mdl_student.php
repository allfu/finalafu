<?php 
class Mdl_student extends CI_Model
{
    function get_events()
    {
        $query = $this->db->get('event_table');
        return $query->result();
    }
	function get_event($e_id)
	{
		$this->db->where('e_id', $e_id);
        $query = $this->db->get('event_table');
        return $query->result();
	}
	function getclub($cid)
	{
		$this->db->where('c_id', $cid);
		//$this->db->where('permit !=', 0);
        $query = $this->db->get('club_table');
        return $query->row();
	}
	  function getgroups($username)
    {		
        $q = $this->db->get_where('user_table',array('user_name'=>$username));
        $i = $q->row()->id;
		
		        
        $g = $this->db->get_where('group_table',array('gstudid'=>$i));
        $r= $g->result();
		
		//$get=array('allgrps'=>$r,'show_grp'=>$c);
		return $r;
		
		//print_r($g->result());
        
    }
	function getallgroups()
	{
		/*$this->db->where('gstudid !=', $uid);
		$g = $this->db->get_where('group_table');
        return $g->result();*/
		$g = $this->db->get_where('club_table');
        return $g->result();
		
	}
	function getuserid($username)
	{
		$q = $this->db->get_where('user_table',array('user_name'=>$username));
        $i = $q->row()->id;
		return $i;
		
	}
	function join_club($data2)
	{
		$this->db->insert('group_table',$data2);
	}
	
	function update_student($id,$data)
	{
			$this->db->where('id',$id);
			$this->db->update('user_table',$data);
	}
	
}
?>