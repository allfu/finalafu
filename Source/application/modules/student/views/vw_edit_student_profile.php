<?php
	//var_dump($user_profile);
	//echo "<pre>";
	//print_r($user_profile);
	//echo "</pre>";
?>
<html>
    <head>
        <!--<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css" rel="stylesheet">-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.8/sweetalert2.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css">
        <link href="<?php echo base_url().'assets/backend/css/bootstrap-select2.css'; ?>" rel="stylesheet" />
		<link href="<?php echo base_url().'assets/frontend/';?>css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
        <script src="<?php echo base_url().'assets/';?>jquery.js"></script>
        <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.8/sweetalert2.min.js"></script>
        <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro|Open+Sans+Condensed:300|Raleway' rel='stylesheet' type='text/css'>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
		
		
        <style>
            body {
               // background-image: url("<?php echo base_url().'assets/img/profileback.jpg'; ?>");
                background-size: cover;
            }
            #login {
                background-color: #fff;
            }
            .center-tag {
                text-align: center;
            }
            .select2-container .select2-selection--single   {
                height: 37px;
                border: 1px solid #ccc;
                box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            }
            .select2-container--default .select2-selection--single .select2-selection__rendered {
                color: #999;
                line-height: 35px;
            }
            .display-name   {
                padding-bottom: 20px;
            }
			::-webkit-input-placeholder {
				text-align: center;
			}

			:-moz-placeholder { /* Firefox 18- */
			   text-align: center;  
			}

			::-moz-placeholder {  /* Firefox 19+ */
			   text-align: center;  
			}

			:-ms-input-placeholder {  
			   text-align: center; 
			}
			input[type="submit"] {
    width: 100%;
    background-color: #5cb85c;
    color: white;
    border: 2px solid #5cb85c;
    padding: 10px;
    font-size: 20px;
    cursor: pointer;
    border-radius: 5px;
    margin-bottom: 15px;
}
        </style>
    </head>
    <body>
		<div class="container-fluid" style="margin-top: 8%">
		
			<div class="row">
			
				<div class="col-md-offset-1 col-md-5">
				<br/>
					<div class="row">
						<div class="col-md-12">
							<div style="text-align:center">
								<img src="<?php echo  $getalldetails->fb_pic;?>" style="border-radius:50%">
							</div>							
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-12">
							 <h2 class="display-name"  style="color:#000000;text-align:center;"><?php echo $getalldetails->user_name;?></h2>
						</div>
					</div>
				</div>
				<div class="col-md-5">
				<br/>
				<br/>
				<br/>
				         
					 <form action="" method="POST" id="edit-studnet-modal">
					     
			
						<div class="row">
				
							<div class="col-md-10">
							 <input type="hidden" name="editstudenty_id" id="editstudenty_id" value="<?php echo $getalldetails->id;?>" />
							
								<!-- <input type="text" name="mat_no" id="mat_no" class="form-control" placeholder="Matriculation Number" style="margin-bottom: 3%;width:100%"/>-->
								 
								 <input type="text" class="form-control" name="edit_matriculation" id="edit_matriculation" style="margin-bottom: 3%;width:100%" value="<?php echo $getalldetails->matno;?>" />
								 
									  <center> 
									  <p id="s_matri_error" style="color:red;"></p>
									  </center>
							</div>
							<br/>
								
								
			        	</div>
						<br/>
								<br/>
						<div class="row">
				
					<div class="col-md-5">
							
						<?php $q = $this->db->get_where('university',array('permit'=>1));
							  $Pr = $q->result();?>
                            <select class="form-control university init-select" name="university" id="university" style="width: 100%">
							<?php $ab = $this->db->get_where('university',array('u_id' => $getalldetails->school));
		                           ?>
							   
							<option value="<?php echo $getalldetails->school;?>" selected="" ><?php echo $ab->row()->u_name;?></option>
                                <?php foreach($Pr as $row){?>
                                <option value="<?php echo $row->u_id;?>"><?php echo $row->u_name;?></option>
                                <?php } ?>
                            </select>
							<p style="color:red;" id="s_university-error"></p>
                       
						
                    </div>
					<div class="col-md-5">
						<?php $q2 = $this->db->get_where('faculty_table',array('permit'=>1));
							  $f = $q2->result();?>
                            <select class="form-control faculty init-select" name="faculty" id="faculty" style="width: 100%;">
							<?php $hr = $this->db->get_where('faculty_table',array('f_id'=>$getalldetails->faculty));
							  ?>
							<option value="<?php echo $getalldetails->faculty;?>" selected="" ><?php echo $hr->row()->f_name;?></option>
                                <?php foreach($f as $row){?>
                                <option value="<?php echo $row->f_id;?>"><?php echo $row->f_name;?></option>
                                <?php } ?>
                            </select>
							<p style="color:red;" id="s_faculty-error"></p>
					</div>
					</div>
					<br/>
						<div class="row">
						
						<div class="col-md-5">
							<?php $q = $this->db->get_where('hall_table',array('permit'=>1));
							  $Pr = $q->result();?>
							   <select class="hall init-select" name="hall" id="hall" class="form-control" style="width: 100%;">
							   <?php $cd = $this->db->get_where('hall_table',array('h_id'=>$getalldetails->hall_name));
							  ?>
							   <option value="<?php echo $getalldetails->hall_name;?>" selected="" ><?php echo $cd->row()->h_name;?></option>

                               <?php foreach($Pr as $row){?>
                                <option value="<?php echo $row->h_id;?>"><?php echo $row->h_name;?></option>
                                <?php } ?>
                            </select>
                           <p style="color:red;" id="s_hall-error"></p>
						</div>
						<div class="col-md-5">
							<?php $q = $this->db->get_where('club_table',array('permit'=>1));
							  $r = $q->result();?>
                            <select class="js-example-basic-single cca init-select"name="cca" id="cca" class="form-control"  style="width: 100%;">
							<?php $lo = $this->db->get_where('club_table',array('c_id'=>$getalldetails->group_cca));
							  $r = $q->result();?>
							<option value="<?php echo $getalldetails->group_cca;?>" selected="" ><?php echo $lo->row()->c_name;?></option>
                              <?php foreach($r as $row){?>
                                <option value="<?php echo $row->c_id;?>"><?php echo $row->c_name;?></option>
                                <?php } ?>
                            </select>
                            <p style="color:red;" id="s_club-error"></p>
						</div>
					</div>
					 
				</div>
			</div>
	   
	   
	   
	  
	   
	 
	   <div class="row">
		<div class="col-md-offset-7 col-md-2">
			<div style="text-align:center;margin-top:5px">
			 
				 <input type="submit" value="Update" id="updatestudent_submit" class="btn btn-success" style="padding:7px;border-radius:5px">
			</div>
		</div>
		<div class="col-md-3"></div>
	   </div>
	   </form>
		</div>
	
       
<!--Initiate Select2-->
<script>
    $(document).ready(function() {
    $('.init-select').select2();
});
</script>
<!--AJAX user authentication-->
<script type="text/javascript">

      $(document).ready(function() {
    $('.js-example-basic-single').select2();
});
        $(document).ready(function() {
    $('.js-example-basic-multiple').select2();
});
        </script>
		
		
		<script>
//EDIT STUDENT PROFILE SCRIPT
//triggered when modal is about to be shown
$(document).ready(function()	{
$("#updatestudent_submit").click(function(e)	{
        e.preventDefault(); //this will restrict the page refresh
        var form_data = {
             id:$('#editstudenty_id').val(),
            mat: $('#edit_matriculation').val(),
			uni: $('#university').val(),
			fac: $('#faculty').val(),
			hl: $('#hall').val(),
			grp: $('#cca').val(),
            
        };
    //alert(form_data.stud_id+" id "+form_data.stud_uname + form_data.stud_email + form_data.stud_website);
    //var form1 = new FormData(this);working till here.
    //var form2 = $('#editclient_form').serialize();
    $.ajax({
        type: "POST",
        //data:form_data,
        data: {
             id: form_data.id,
             mat: form_data.mat, 
            uni: form_data.uni, 
            fac: form_data.fac, 
            hl: form_data.hl, 
            grp: form_data.grp, 
            
        },
        
        url: "<?php echo site_url('student/update_prof'); ?>",
        
        success: function(res)  {
        var json = JSON.parse(res);
        console.log(json);
        //alert('herehere');
        
        if(json.st== 'fail'){
                $('#s_matri_error').html(json.s_matri);
                $('#s_university-error').html(json.s_uni);
                $('#s_faculty-error').html(json.s_fac);
                $('#s_hall-error').html(json.s_hall);
                $('#s_club-error').html(json.s_grp);
                
                }
        if(json.st =='success') {
               
                console.log(json);
                swal(
         '',
         'profile Updated successfully!',
         'success'
       );
                window.location.href = "<?php echo site_url('student/showlist'); ?>";
                }
        }
        });
    });
});
</script>
    