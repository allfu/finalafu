<Script>
    // Load the SDK asynchronously
    (function(d, s, id){
       var js, fjs = d.getElementsByTagName(s)[0];
       if (d.getElementById(id)) {return;}
       js = d.createElement(s); js.id = id;
       js.src = "//connect.facebook.net/en_US/all.js";
       fjs.parentNode.insertBefore(js, fjs);
     }(document, 'script', 'facebook-jssdk'));
</script>
<div class="tabs-container text-center">
    <ul>
        <li>
            <!--<div class="tab__title">
                            <span class="h5">Medium</span>-->
            <div class="tab__content">
                <section class=" bg--secondary">
                    <div class="container">
                        <div class="row">							
                            <?php foreach($res as $row){?>
							<?php //print_r($row); ?>
							<?php $e_fblink = htmlspecialchars(json_encode($row->e_fblink)); ?>
                            <div class="col-sm-4" onclick="openEvent(<?php echo $row->e_id." , ".$row->e_by." , ".$e_fblink; ?>)" >
                                <div class="pricing pricing-1 boxed boxed--lg boxed--border">
                                    <input type="hidden" name="fblink1" id="fblink1" value="<?php echo $row->e_fblink; ?>" />
									<?php if(isset($row->e_img)){?>
									<img alt="Image" class="border--round" src="<?php echo base_url().$row->e_img;?>"  style="height:200px;width:360px;"/>
									<?php } 
									else{?>
									<img alt="Image" class="border--round" src="<?php echo base_url().'assets/frontend/';?>img/xyz.jpg"  style="height:200px;width:360px;"/>
									<?php } ?>
                                    <hr>
                                    <img src="<?php echo base_url()?>assets/frontend/img/upload_image.png" style="height:100px;width:100px;" class="align-center" />
                                    <ul>
                                        <li>
                                            <span style="font-size: 150%;">120 of 155 Attending</span>
                                        </li>
                                        <li>
                                            <span style="font-size: 170%; color:#000000;"><?php echo $row->e_name;?></span>
                                        </li>
                                        
                                        <li>
                                            <?php $q =  $this->db->get_where('club_table',array('c_id' => $row->e_by));
															$i = $q->row(); 
															//print_r($i);
															//$club =  $i->c_name;  ?>
                                            <span style="font-size: 100%; color:#a5a5a5;"><?php echo $i->c_name;?></span>
                                        </li>
                                        <li>
                                            <button type="button" class="btn btn-info">Join</button>
                                        </li>
                                        <br>
                                    </ul>

                                </div>
                                <!--end of pricing-->
                            </div>
                            <?php } ?>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </section>
            </div>
            <!--</div>-->
        </li>
        </li>
    </ul>
    <!-- Modal -->
    <div class="modal fade" id="viewEvent" tabindex="-1" role="dialog" aria-labelledby="viewEvent" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row" style="padding:10px; margin-left: 10px;margin-right: 10px; border: 2px solid gray;">

                        <div class="col-sm-6" style="border-right: 2px solid gray;" >

                            <!--<img id="pop_eimg" alt="Image" class="border--round" src="<?php //echo base_url().'assets/frontend/img/xyz.jpg';?>" />-->
                            <img id="" alt="Image" class="border--round" src="<?php echo base_url().'assets/frontend/img/xyz.jpg';?>" />
							
                            <h4 style="color:#000000; font:bold; "> <b>Your Club Has Worked Hard For Your Welfare
                                 Do Help Them Achieve These Objectives</b> </h4>
                            <br>
							<!--<input type="text" id="fblink" name="fblink">-->
	 						<center>
							<input type="hidden" name="fblink" id="fblink" value="" />
							<!--<button class="btn btn-info share-btn">  Share via facebook  </button>-->
							<div id="fb-root"></div>
							<div class="fb-like" data-href=" My URL " data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>
							<a class="share-btn" id="fb" name="fb" >
							<img src="<?php echo base_url().'assets/img/fb.png'?>" style="width:120px;"/></a>							
							
							<!--<a class="fbsharelink" id="fbsharelink" name="fbsharelink" data-href="" data-show-faces="true" data-share="true">
							<img src="<?php //echo base_url().'assets/img/fb.png'?>" style="width:120px;"/></a>-->

							<div class="addthis_inline_follow_toolbox" style="padding-right: 13px!important;">
							</div>
							</center>                          
							 
                        </div>
                        <div class="col-sm-6 col-md-5" style="padding-left: 38px!important;">
                            <div class="switchable__text">
                                <center>								 

                                    <img alt="Club logo" height="90" width="90" class="border--round" src="<?php echo base_url().'assets/frontend/';?>img/upload_image.png" />
                                   	<h4 style="color:#00aeff;">120 out of 155 Attending</h4>
                                    <h4 id="pop_ename"><b></b></h4><br>
                                    <h4 id="pop_cname"></h4>
                                    <p id="pop_edesc"></p><br>
									<div class="row">
									<div class="row">
									<div class="col-md-6">
									<p style="color:#000000; font-weight:800;">Welfare</p>
									Event 
									</div>
									<div class="col-md-6">
									<p style="color:#000000; font-weight:800;">SPM Atrium</p>
									Location</div>
									</div>
									</div>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- Go to www.addthis.com/dashboard to customize your tools --> 
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5a006ab758a1365f"></script>
<Script>
        function openEvent(e_id, e_by, e_fblink) {
            var e_id = e_id;
			var e_by = e_by;
			var e_fblink = e_fblink;
			//var fcheck = $('#fbsharelink').attr("data-href");
			//alert(fcheck);
			//console.log(e_fblink); 
            $.ajax({
                url: "<?php echo site_url('student/get_event'); ?>",
                data: {
                    e_id: e_id,
                    e_by: e_by
                },
                type: "POST",
                success: function(response) {
                    var json = response;
                    var parsed = JSON.parse(response);
                   // console.log(json);
                    if (parsed.status_code == 1) {
                        //console.log(parsed.status);
                        $('#pop_ename').html(parsed.status[0]['e_name']);
                        $('#pop_edesc').html(parsed.status[0]['e_desc']);
                        $('#pop_eimg').html(parsed.status[0]['e_img']);
                        $('#pop_cname').html(parsed.clubname);
						//$('#fbsharelink').attr('data-href', e_fblink);
						document.getElementById('fblink').value = parsed.status[0]['e_fblink'];
						$('.border--round').attr('src','second.jpg');
												
                        $("#viewEvent").modal('show');
                    } else {
                        console.log(status);
                    }
					
                }
            });
        }

</script>
<div id="fb-root"></div>
<script>
    window.fbAsyncInit = function() {
      // init the FB JS SDK
      FB.init({
        appId      : '1811356122510910',                        // App ID from the app dashboard
        channelUrl : '//WWW.YOUR_DOMAIN.COM/channel.html', // Channel file for x-domain comms
        status     : true,                                 // Check Facebook Login status
        xfbml      : true                                  // Look for social plugins on the page
      });
  
      // Additional initialization code such as adding Event Listeners goes here
	   FB.Event.subscribe('edge.create', function(response) {
        alert('You liked the URL: ' + response);

      });
    };
  

</script>
<script>
function fb_share() {
    FB.ui( {
		//var link1 = "https://m.facebook.com/story.php?story_fbid=711468882379552&id=382628028596974",
        method: 'feed',
       // name: "Facebook API: Tracking Shares",
        link: $('#fblink').val(),
       // picture: "https://stackexchange.com/users/flair/557969.png",
        //caption: "Tracking Facebook Shares"
    }, 
	  function(response) {
    // if (response && response.post_id) {
     if (response) {
		   $.ajax({
           type: "POST",
           url: <?php //echo base_url().'';?>,
          //data: $("#idForm").serialize(), // serializes the form's elements.
	       success: function(data)
           {
			   JSON.parse('{ "name":"John", "age":30, "city":"New York"}');
			   //console.log(data + " "+ status);
			  // jsonparse	= JSON.parse(data);
             //  alert(data); // show response from the php script.
           }
         });
		 
		 // Controller response ajax =  return json_encode(array()) 
		 
		 
    //   alert('Post was published.');
     } else {
       alert('Post was not published.');
     }
   }
   );
  
}
$(document).ready(function(){
  $('.share-btn').on( 'click', fb_share );
});
</script>