<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12">
				<button type="button" style="float: right; margin-right: 0.5em;" class="btn btn-info" data-toggle="modal" data-target="#add-student-modal"><i class="icon-plus4" style="color:white;"></i>Add Student</button>
                <h1>Student List</h1>
                <div class="heading-elements">
                                    <!--<ul class="list-inline mb-0">-->
                                        <!--<li><a href="#" data-toggle="modal" data-target="#add-product-modal"><i class="icon-plus4"></i> Add Client   </a></li>-->
                                        <!--<li><button type="button" class="btn btn-info btn-block"
                                                    data-toggle="modal" data-target="#add-student-modal">
                                            <i class="icon-plus4" style="color:white;"></i>   Add Student</button>
                                        </li>-->
                                        <!--<li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                                        <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                                        <li><a data-action="reload"><i class="icon-reload"></i></a></li>-->
                                    <!--</ul>-->
                                </div>
        <table id="student-table" class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>Student Name</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Contact</th>
                    <th>Website</th>
                    <th>School</th>
                    <th>Year</th>
                    <th>Type</th>
                    <th>Permit</th>
                    <th>Actions</th>
                </tr>
                
            </thead>
            <tbody>
                
                <?php foreach($data as $row)   
                                              {
                                                        ?>
                                                        <tr>
                                                        <td class="text-middle"><?php echo $row->user_name; ?></td>
                                                        <td class="text-middle"><?php echo $row->firstname; ?></td>
                                                        <td class="text-middle"><?php echo $row->lastname; ?></td>
                                                        <td class="text-middle"><?php echo $row->user_email; ?></td>
                                                        <td class="text-middle"><?php echo $row->contact_no; ?></td>
                                                        <td class="text-middle"><?php echo $row->website; ?></td>
                                                        <td class="text-middle"><?php echo $row->school; ?></td>
                                                        <td class="text-middle"><?php echo $row->yr; ?></td> 
                                                        <td class="text-middle">
														<?php if($row->type==1){echo "Student";} else {echo "Student Leader";}?>
														</td>
                                                        <td class="text-middle"><?php if($row->permit == '1')  { ?>
                                                        <span class="tag tag-success">Active</span><?php } else { ?>
                                                        <span class="tag tag-danger">Banned</span>
                                                        <?php } ?></td>
                                                        <td>
                                                            <span class="btn-group">
                                                            <!--<input type="button" class="btn btn-outline-info" value="View" data-toggle="modal" data-target="#view-modal"/>-->
															<?php if($row->type == 1){$typename="Student";}else{$typename="Student Leader";}?>
                                                            <input type="button" class="btn btn-outline-primary" value="Edit" 
                                                            data-toggle="modal" data-target="#edit-student-modal" 
                                                            data-id="<?php echo $row->id;?>" data-name="<?php echo $row->user_name; ?>" 
                                                            data-fname="<?php echo $row->firstname; ?>" data-lname="<?php echo $row->lastname;?>" data-email="<?php echo $row->user_email;?>" data-contact="<?php echo $row->contact_no;?>" data-website="<?php echo $row->website; ?>" data-pass="<?php echo $row->user_password;?>" data-school="<?php echo $row->school; ?>" data-yr="<?php echo $row->yr; ?>"
															data-type="<?php echo $row->type;?>" data-typename="<?php echo $typename;?>" />

                                                            <input type="button" class="btn btn-outline-danger" 
                                                                   <?php 
                                                                    if($row->permit==0)
                                                                    { echo "value='Deleted'";
                                                                    }
                                                                   else{ 
                                                                    echo "value='Delete'"; ?>
                                                                   data-toggle="modal" data-target="#delete-student-modal" 
                                                                   data-id="<?php echo $row->id;?>" 
                                                                   data-uname="<?php echo $row->user_name; ?>" />
                                                                   <?php } ?>
                                                            </span>
                                                        </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
              <!--  <table id="table_id" class="display">
                    <thead>
                        <tr>
                            <th>Column 1</th>
                            <th>Column 2</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Row 1 Data 1</td>
                            <td>Row 1 Data 2</td>
                        </tr>
                        <tr>
                            <td>Row 2 Data 1</td>
                            <td>Row 2 Data 2</td>
                        </tr>
                        <tr>
                            <td>Row 2 Data 1</td>
                            <td>Row 2 Data 2</td>
                        </tr>
                        <tr>
                            <td>Row 2 Data 1</td>
                            <td>Row 2 Data 2</td>
                        </tr>
                        <tr>
                            <td>Row 2 Data 1</td>
                            <td>Row 2 Data 2</td>
                        </tr>
                        <tr>
                            <td>Row 2 Data 1</td>
                            <td>Row 2 Data 2</td>
                        </tr>
                        <tr>
                            <td>Row 2 Data 1</td>
                            <td>Row 2 Data 2</td>
                        </tr>
                        <tr>
                            <td>Row 2 Data 1</td>
                            <td>Row 2 Data 2</td>
                        </tr>
                        <tr>
                            <td>Row 2 Data 1</td>
                            <td>Row 2 Data 2</td>
                        </tr>
                        <tr>
                            <td>Row 2 Data 1</td>
                            <td>Row 2 Data 2</td>
                        </tr>
                        <tr>
                            <td>Row 2 Data 1</td>
                            <td>Row 2 Data 2</td>
                        </tr>
                        <tr>
                            <td>Row 2 Data 1</td>
                            <td>Row 2 Data 2</td>
                        </tr>
                        <tr>
                            <td>Row 2 Data 1</td>
                            <td>Row 2 Data 2</td>
                        </tr>
                        <tr>
                            <td>Row 2 Data 1</td>
                            <td>Row 2 Data 2</td>
                        </tr>
                        <tr>
                            <td>Row 2 Data 1</td>
                            <td>Row 2 Data 2</td>
                        </tr>
                    </tbody>
                </table>-->
            </div>
        </div>
    </div>
</div>
<!--Add Modal-->
<div class="modal fade text-xs-left" id="add-student-modal" tabindex="-1" role="dialog" aria-labelledby="addModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Add Student Information</h4>
            </div>
            <form action="<?php //echo base_url().'clients/add_client';?>" method="post" id="addstudent_form">
                <div class="modal-body">
                    <label> Student User Name:</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="student_uname" value="<?php echo set_value('student_uname');?>" id="student_uname" />
                        <p id="student_uname_error" style="color:red;"></p>
                    </div>
                    <label>First Name:</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="student_fname" value="<?php echo set_value('student_fname');?>" id="student_fname" />
                        <p id="student_fname_error" style="color:red;"></p>
                    </div>
                    <label>Last Name:</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="student_lname" value="<?php echo set_value('student_lname');?>" id="student_lname" />
                        <p id="student_lname_error" style="color:red;"></p>
                    </div>
                    <label>Contact no:</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="student_contact" value="<?php echo set_value('student_contact');?>" id="student_contact" />
                        <p id="student_contact_error" style="color:red;"></p>
                    </div>
                    <label>Email:</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="student_email" value="<?php echo set_value('student_email');?>" id="student_email" />
                        <p id="student_email_error" style="color:red;"></p>
                    </div>
                    <label>Website:</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="student_website" value="<?php echo set_value('student_website');?>" id="student_website" />
                        <p id="student_website_error" style="color:red;"></p>
                    </div>
                    <label>Password:</label>
                    <div class="form-group">
                        <input type="password" class="form-control" name="student_password" value="<?php echo set_value('student_password');?>" id="student_password" />
                        <p id="student_password_error" style="color:red;"></p>
                    </div>
                    <!--<label>Confirm Password:</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="student_repass" value="<?php //echo set_value('student_repass');?>" id="student_repass" />
                        <p id="product_name_error" style="color:red;"></p>
                    </div>-->
					 <label>Group Name:</label>
                    <div class="form-group">
                        <!--<input type="text" class="form-control" name="student_group" value="<?php //echo set_value('student_group');?>" id="student_group" />-->
						<select class="form-control" name="student_group" id="student_group">
						   <option value="none" selected="" disabled=""></option>
						   <?php $this->db->where('permit !=',0);
								 $query = $this->db->get('club_table');
								 foreach($query->result() as $row){
						   ?>
						   <option value="<?php echo $row->c_id;?>"><?php echo $row->c_name;?></option>
								 <?php } ?>
						</select>
                        <p id="student_group_error" style="color:red;"></p>
                    </div>
                     <label>School:</label>
                    <div class="form-group">
                        <!--<input type="text" class="form-control" name="student_school" value="<?php echo set_value('student_school');?>" id="student_school" />-->
						<select class="js-example-basic-single form-control" name="student_school" id="student_school">
						<option value="none" selected="" disabled=""></option>
						<option value="National_Universtity_of_Singapore">National Universtity of Singapore</option>
						<option value="Singapore_Institute_Of_Technology">Singapore Institute Of Technology</option>
						<option value="Singapore_Institute_Of_Technology & Design">Singapore Institute Of Technology & Design</option>
						<option value="Singapore_Management_University">Singapore Management University</option>
						<option value="Singapore_University_Of_Social_Science</">Singapore University Of Social Science</option>
						</select>
                        <p id="student_school_error" style="color:red;"></p>
                    </div>
                     <label>Year of Study:</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="student_yr" value="<?php echo set_value('student_yr');?>"  
                               id="student_yr" />
                        <p id="student_yr_error" style="color:red;"></p>
                    </div>
					<label>Type of Student:</label>
                    <div class="form-group">
                        <select class="form-control" name="student_type" id="student_type">
						<option value="none" selected="" disabled=""></option>
						<option value="1">Student</option>
						<option value="2">Student Leader</option>
						</select>
                        <p id="student_type_error" style="color:red;"></p>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="reset" class="btn grey btn-outline-secondary" data-dismiss="modal" value="Cancel">
                    <input type="submit" class="btn btn-outline-primary" value="Submit" />
                </div>
            </form>
        </div>
    </div>
</div>
<!--Edit Modal-->
<div class="modal fade text-xs-left" id="edit-student-modal" tabindex="-1" role="dialog" aria-labelledby="editModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Edit Student Information</h4>
            </div>
            <form method="post" id="editstudentform" name="editstudentform" action="">
                <div class="modal-body">
                    <input type="hidden" name="editstud_id" id="editstud_id" value="<?php echo set_value('editstud_id');?>" /><!--value will be set through javascript-->
                    <label>Student User Name:</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="edit_studuname" id="edit_studuname" value="<?php echo set_value('edit_studuname');?>" />
                    </div>
                    <p id="editstudu_name_error" style="color:red;"></p>
                    <label>First Name:</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="edit_studfname" id="edit_studfname" value="<?php echo set_value('edit_studfname');?>" />
                    </div>
                    <p id="editstud_fname_error" style="color:red;"></p>
                    <label>Last Name:</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="edit_studlname" id="edit_studlname" value="<?php echo set_value('edit_studlname');?>" />
                    </div>
                    <p id="editstud_lname_error" style="color:red;"></p>
                    <label>Contact:</label>
                    <div class="form-group">
                        <input type="tel" class="form-control" name="editstud_contact" id="editstud_contact" value="<?php echo set_value('editstud_contact');?>" />
                    </div>
                    <p id="editstud_contact_error" style="color:red;"></p>
                    <label>Email:</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="editstud_email" id="editstud_email" value="<?php echo set_value('editstud_email');?>" />
                    </div>
                    <p id="editstud_email_error" style="color:red;"></p>
                    <label>Website:</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="editstud_website" id="editstud_website" value="<?php echo set_value('editstud_website');?>" />
                    </div>
                    <p id="editstud_website_error" style="color:red;"></p>
                    <label>Password:</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="editstud_pass" id="editstud_pass" value="<?php echo set_value('editstud_pass');?>" />
                    </div>
                    <p id="editstud_pass_error" style="color:red;"></p>
                    <label>School:</label>
                    <div class="form-group">
                        <!--<input type="text" class="form-control" name="editstud_school" id="editstud_school" value="12345678" />-->
						<select class="js-example-basic-single form-control" name="editstud_school" id="editstud_school">
						<option value="none" selected="" disabled=""></option>
						<option value="National_Universtity_of_Singapore">National Universtity of Singapore</option>
						<option value="Singapore_Institute_Of_Technology">Singapore Institute Of Technology</option>
						<option value="Singapore_Institute_Of_Technology & Design">Singapore Institute Of Technology & Design</option>
						<option value="Singapore_Management_University">Singapore Management University</option>
						<option value="Singapore_University_Of_Social_Science</">Singapore University Of Social Science</option>
						</select>
                    </div>
                    <p id="editstud_school_error" style="color:red;"></p>
					<!--<label>CCA:</label>
                    <div class="form-group">
                        <!--<input type="text" class="form-control" name="editstud_cca" id="editstud_cca" value="<?php echo set_value('editstud_cca');?>"/>--
						<select class="js-example-basic-single form-control" name="editstud_cca" id="editstud_cca">
						<option value="none" selected="" disabled=""></option>
						<?php $this->db->where('permit !=',0);
								 $query = $this->db->get('club_table');
								 foreach($query->result() as $row){
						   ?>
						   <option value="<?php echo $row->c_id;?>"><?php echo $row->c_name;?></option>
								 <?php } ?>
						</select>
                    </div>-->
                    <p id="editstud_cca_error" style="color:red;"></p>
                    <label>Year of Study:</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="editstud_yr" id="editstud_yr" value="<?php echo set_value('editstud_yr');?>"/>
                    </div>
                    <p id="editstud_yr_error" style="color:red;"></p>
					 <label>Type of Student:</label>
                    <div class="form-group">
                        <!--<input type="text" class="form-control" name="editstud_type" id="editstud_type" value="<?php echo set_value('editstud_type');?>"/>-->
						<select class="form-control" name="editstud_type" id="editstud_type">
						<option value="none" selected="" disabled=""></option>
						<option value="1">Student</option>
						<option value="2">Student Leader</option>
						</select>
                    </div>
                    <p id="editstud_yr_error" style="color:red;"></p>
                    
                </div>
                <div class="modal-footer">
                    <input type="reset" class="btn grey btn-outline-secondary" data-dismiss="modal" value="close">
                    <input type="submit" class="btn btn-outline-primary updatestudent_submit" value="Submit" />
                </div>
            </form><p></p>
        </div>
    </div>
</div>
<!--Delete Modal-->
<div class="modal fade text-xs-left" id="delete-student-modal" tabindex="-1" role="dialog" aria-labelledby="deleteModal" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <form action="<?php echo base_url().'students/delete'; ?>" method="POST" class="delete-form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Delete Student</h4>
                </div>
                <div class="modal-body deletepara">
                    <p></p> <!--client information will go through here to jscript-->
                    <input type="hidden" name="del_uid" id="del_uid" value="" /><!--value will be set through javascript-->
                    <input type="hidden" name="del_uname" id="del_uname" value="" /><!--value will be set through javascript-->
                    <div id="result">
                        <div id="value"></div>
                        <div id="anothervalue"></div>
                    </div>
                </div>
                </form>
                <div class="modal-footer">
                    <input type="reset" class="btn grey btn-outline-secondary" data-dismiss="modal" value="No">
                    <input type="submit" class="btn grey btn-outline-danger delete-student-submit" value="Yes">
                </div>
            </div>
    </div>
</div>


<script>

//DELETE STUDENT SCRIPT
$('#delete-student-modal').on('show.bs.modal', function(e) {
   
    //get data-id attribute of the clicked element
    var deluid = $(e.relatedTarget).data('id');
    var deluname = $(e.relatedTarget).data('uname');
    var modal = $(this);
   
    //populate the textboxes
    $(e.currentTarget).find('input[name="del_uid"]').val(deluid);
    document.getElementById('del_uname').value = deluname;
     modal.find('.modal-body p').html('Do you really want to delete - <b>' + deluname + '</b>?');
   
});

$(document).ready(function()	{
    $(".delete-student-submit").click(function(event)	{
       event.preventDefault();
       
       var u_id = document.getElementById('del_uid').value;
        var u_name = document.getElementById('del_uname').value;
        
           jQuery.ajax({
               type: "POST",
               url: "<?php echo site_url('students/delete_student'); ?>",
               dataType: "json",
               data: {uid: u_id, uname: u_name},
               success: function(res)	{
                   console.log(res);
                   if(res)
                       {   
                           alert('Student Deleted Successfully');
                           //$('#delete-client-modal').modal().hide();
                           window.location.href = "<?php echo site_url('students'); ?>";

                       }
                   else{
                       alert('Delete not successful');
                       window.location.href = "<?php echo site_url('students'); ?>";
                   }
                   }
               });
           });
       });
</script>


<script type="text/javascript">
//ADD STUDENT SCRIPT
$(function() { // <----------------missed the doc ready function
    $('#addstudent_form').submit(function(e) {
        e.preventDefault(); // <------this will restrict the page refresh
        var form_data = {
            stud_name: $('#student_uname').val(),
            stud_fname: $('#student_fname').val(),
            stud_lname: $('#student_lname').val(),
            stud_email: $('#student_email').val(),
            stud_contact: $('#student_contact').val(),
            stud_website: $('#student_website').val(),
            stud_password: $('#student_password').val(),
            stud_grp: $('#student_group').val(),
            stud_school: $('#student_school').val(),
            stud_yr: $('#student_yr').val(),
            stud_type: $('#student_type').val(),
        };
        //alert(form_data.stud_fname);
        $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + 'students/add_student',
                data: form_data,
                success: function(res)  {
                    var json = JSON.parse(res);
                    console.log(json);
                     if(json.st== 'fail'){
                        $('#student_uname_error').html(json.user_name);
                        $('#student_fname_error').html(json.first_name);
                        $('#student_lname_error').html(json.last_name);
                        $('#student_contact_error').html(json.contact);
                        $('#student_email_error').html(json.email);
                        $('#student_website_error').html(json.website);
                        $('#student_password_error').html(json.pass);
                        $('#student_group_error').html(json.grp);
                        //$('#re_pass-error').html(json.re_pass);
                        $('#student_school_error').html(json.school);
                        $('#student_yr_error').html(json.yr);                                   
                        $('#student_type_error').html(json.type);                                   
                    }
                   
                   if(json.st =='success'){
                        
                        alert('Student added successfully');
                        window.location.href = "<?php echo site_url('students'); ?>";
                    }
                   
                }
        });

    });

});
</script>

<script>
//EDIT STUDENT SCRIPT
//triggered when modal is about to be shown
$('#edit-student-modal').on('show.bs.modal', function(e) {
   
    //get data-id attribute of the clicked element
    var editstudid = $(e.relatedTarget).data('id');
    var editstuduname = $(e.relatedTarget).data('name');
    var editstudfname = $(e.relatedTarget).data('fname');
    var editstudlname = $(e.relatedTarget).data('lname');
    var editstudemail= $(e.relatedTarget).data('email');
    var editstudcontact = $(e.relatedTarget).data('contact');
    var editstudwebsite = $(e.relatedTarget).data('website');
    var editstudpass= $(e.relatedTarget).data('pass');
    var editstudschool = $(e.relatedTarget).data('school');
    var editstudyear = $(e.relatedTarget).data('yr');
    var editstudtype = $(e.relatedTarget).data('type');
    var editstudtypename = $(e.relatedTarget).data('typename');
    
    //populate the textboxes
    $(e.currentTarget).find('input[name="editstud_id"]').val(editstudid);
    document.getElementById('edit_studuname').value = editstuduname;
    document.getElementById('edit_studfname').value = editstudfname;
    document.getElementById('edit_studlname').value = editstudlname;
    document.getElementById('editstud_email').value = editstudemail;
    document.getElementById('editstud_contact').value = editstudcontact;
    document.getElementById('editstud_website').value = editstudwebsite;
    document.getElementById('editstud_pass').value = editstudpass;
    document.getElementById('editstud_school').value = editstudschool;
    document.getElementById('editstud_yr').value = editstudyear;
    document.getElementById('editstud_type').value = editstudtypename;
    //document.getElementById('editstud_typename').value = editstudtypename;
    
});

$(document).ready(function()	{
$(".updatestudent_submit").click(function(e)	{
        e.preventDefault(); //this will restrict the page refresh
        var form_data = {
            stud_id:$('#editstud_id').val(),
            stud_uname: $('#edit_studuname').val(),
            stud_fname: $('#edit_studfname').val(),
            stud_lname: $('#edit_studlname').val(),
            stud_email: $('#editstud_email').val(),
            stud_contact: $('#editstud_contact').val(),
            stud_website: $('#editstud_website').val(),
            stud_pass: $('#editstud_pass').val(),
            stud_school: $('#editstud_school').val(),
            stud_yr: $('#editstud_yr').val(),
        };
    //alert(form_data.stud_id+" id "+form_data.stud_uname + form_data.stud_email + form_data.stud_website);
    //var form1 = new FormData(this);working till here.
    //var form2 = $('#editclient_form').serialize();
    $.ajax({
        type: "POST",
        //data:form_data,
        data: {
            id: form_data.stud_id, 
            user_name: form_data.stud_uname, 
            user_email:form_data.stud_email, 
            user_password: form_data.stud_pass,
            firstname:form_data.stud_fname,
            lastname:form_data.stud_lname,
            contact_no:form_data.stud_contact, 
            website:form_data.stud_website,
            school:form_data.stud_school, 
            yr:form_data.stud_yr,
        },
        
        url: "<?php echo site_url('students/update_student'); ?>",
        
        success: function(res)  {
        var json = JSON.parse(res);
        console.log(json);
        //alert('herehere');
        
        if(json.st== 'fail'){
                $('#editstudu_name_error').html(json.user_name);
                $('#editstud_fname_error').html(json.first_name);
                $('#editstud_lname_error').html(json.last_name);
                $('#editstud_email_error').html(json.email);                                    
                $('#editstud_contact_error').html(json.contact);                                    
                $('#editstud_website_error').html(json.website);                                    
                $('#editstud_pass_error').html(json.pass);                                    
                $('#editstud_school_error').html(json.school);                                    
                $('#editstud_yr_error').html(json.yr);  
                
                }
        if(json.st =='success') {
               
                console.log(json);
                alert('Student updated successfully');
                window.location.href = "<?php echo site_url('students'); ?>";
                }
        }
        });
    });
});
</script>
<script>
    $(document).ready(function() {
        $('#student-table').DataTable();
		$('#student-table_filter input').removeClass('form-control-sm');
		$('#student-table_filter input').addClass('form-control-md');
		$('#student-table_length select').removeClass('form-control-sm');
		$('#student-table_length select').addClass('form-control-md');
    });
</script>