<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12">
                <h1>University List</h1>
                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <!--<li><a href="#" data-toggle="modal" data-target="#add-product-modal"><i class="icon-plus4"></i> Add Client   </a></li>-->
                                        <li><button type="button" class="btn btn-info btn-block"
                                                    data-toggle="modal" data-target="#add-university-modal">
                                            <i class="icon-plus4" style="color:white;"></i>   Add University</button>
                                        </li>
                                        <!--<li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                                        <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                                        <li><a data-action="reload"><i class="icon-reload"></i></a></li>-->
                                    </ul>
                                </div>
        <table id="university-table" class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>University Name</th>
                    <th>Permit</th>
                    <th>Actions</th>
                </tr>
                
            </thead>
            <tbody>
                <?php foreach($data as $row)   
                                              {
                                                        ?>
                                                        <tr>
                                                        <td class="text-middle"><?php echo $row->u_name; ?></td>
                                                        
                                                       
                                                        <td class="text-middle"><?php if($row->permit == '1')  { ?>
                                                        <span class="tag tag-success">Active</span><?php } else { ?>
                                                        <span class="tag tag-danger">Banned</span>
                                                        <?php } ?></td>
                                                        <td>
                                                            <span class="btn-group">
                                                            <!--<input type="button" class="btn btn-outline-info" value="View" data-toggle="modal" data-target="#view-modal"/>-->
															
                                                                
                                                            <input type="button" class="btn btn-outline-primary" value="Edit" 
                                                            data-toggle="modal" data-target="#edit-university-modal" 
                                                            data-id="<?php echo $row->u_id;?>" data-name="<?php echo $row->u_name; ?>"  />

                                                            <input type="button" class="btn btn-outline-danger" 
                                                                   <?php 
                                                                    if($row->permit==0)
                                                                    { echo "value='Deleted'";
                                                                    }
                                                                   else{ 
                                                                    echo "value='Delete'"; ?>
                                                                   data-toggle="modal" data-target="#delete-university-modal" 
                                                                   data-id="<?php echo $row->u_id;?>" 
                                                                   data-uname="<?php echo $row->u_name; ?>" />
                                                                   <?php } ?>
                                                            </span>
                                                        </td>
                </tr>
                <?php } ?>
            </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!--Add Modal-->
<div class="modal fade text-xs-left" id="add-university-modal" tabindex="-1" role="dialog" aria-labelledby="addModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Add University Information</h4>
            </div>
            <form action="<?php //echo base_url().'clients/add_client';?>" method="post" id="adduniversity_form">
                <div class="modal-body">
                    <label>University Name:</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="university_uname" value="<?php echo set_value('university_uname');?>" id="university_uname" />
                        <p id="university_uname_error" style="color:red;"></p>
                    </div>
                   	
                </div>
                <div class="modal-footer">
                    <input type="reset" class="btn grey btn-outline-secondary" data-dismiss="modal" value="Cancel">
                    <input type="submit" class="btn btn-outline-primary" value="Submit" />
                </div>
            </form>
        </div>
    </div>
</div>
<!--Edit Modal-->
<div class="modal fade text-xs-left" id="edit-university-modal" tabindex="-1" role="dialog" aria-labelledby="editModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Edit University Information</h4>
            </div>
            <form method="post" id="editstudentform" name="editstudentform" action="">
                <div class="modal-body">
                    <input type="hidden" name="edituniversty_id" id="edituniversty_id" value="<?php echo set_value('edituniversty_id');?>" /><!--value will be set through javascript-->
                    <label>University Name:</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="edit_universityname" id="edit_universityname" value="<?php echo set_value('edit_universityname');?>" />
                    </div>
                    <p id="edituniversity_name_error" style="color:red;"></p>
                   </div>
                <div class="modal-footer">
                    <input type="reset" class="btn grey btn-outline-secondary" data-dismiss="modal" value="close">
                    <input type="submit" class="btn btn-outline-primary updateunivrsity_submit" value="Submit" />
                </div>
            </form><p></p>
        </div>
    </div>
</div>
<!--Delete Modal-->
<div class="modal fade text-xs-left" id="delete-university-modal" tabindex="-1" role="dialog" aria-labelledby="deleteModal" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <form action="<?php echo base_url().'university/delete_university'; ?>" method="POST" class="delete-form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Delete University</h4>
                </div>
                <div class="modal-body deletepara">
                    <p></p> <!--client information will go through here to jscript-->
                    <input type="hidden" name="del_uid" id="del_uid" value="" /><!--value will be set through javascript-->
                    <input type="hidden" name="del_uname" id="del_uname" value="" /><!--value will be set through javascript-->
                    <div id="result">
                        <div id="value"></div>
                        <div id="anothervalue"></div>
                    </div>
                </div>
                </form>
                <div class="modal-footer">
                    <input type="reset" class="btn grey btn-outline-secondary" data-dismiss="modal" value="No">
                    <input type="submit" class="btn grey btn-outline-danger delete-university-submit" value="Yes">
                </div>
            </div>
    </div>
</div>


<script>

//DELETE STUDENT SCRIPT
$('#delete-university-modal').on('show.bs.modal', function(e) {
   
    //get data-id attribute of the clicked element
    var deluid = $(e.relatedTarget).data('id');
    var deluname = $(e.relatedTarget).data('uname');
    var modal = $(this);
   
    //populate the textboxes
    $(e.currentTarget).find('input[name="del_uid"]').val(deluid);
    document.getElementById('del_uname').value = deluname;
     modal.find('.modal-body p').html('Do you really want to delete - <b>' + deluname + '</b>?');
   
});

$(document).ready(function()	{
    $(".delete-university-submit").click(function(event)	{
       event.preventDefault();
       
       var u_id = document.getElementById('del_uid').value;
        var u_name = document.getElementById('del_uname').value;
        
           jQuery.ajax({
               type: "POST",
               url: "<?php echo site_url('university/delete_university'); ?>",
               dataType: "json",
               data: {uid: u_id, uname: u_name},
               success: function(res)	{
                   console.log(res);
                   if(res)
                       {   
                           alert('University Deleted Successfully');
                           //$('#delete-client-modal').modal().hide();
                           window.location.href = "<?php echo site_url('university'); ?>";

                       }
                   else{
                       alert('Delete not successful');
                       window.location.href = "<?php echo site_url('university'); ?>";
                   }
                   }
               });
           });
       });
</script>


<script type="text/javascript">
//ADD STUDENT SCRIPT
$(function() { // <----------------missed the doc ready function
    $('#adduniversity_form').submit(function(e) 
                                    {
        e.preventDefault(); // <------this will restrict the page refresh
        var form_data = {
            u_name: $('#university_uname').val(),
            
        };
        //alert(form_data.stud_fname);
        $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + 'university/add_university',
                data: form_data,
                success: function(res)  {
                    var json = JSON.parse(res);
                    console.log(json);
                     if(json.st== 'fail'){
                        $('#university_uname_error').html(json.user_name);
                                                        
                    }
                   
                   if(json.st =='success'){
                        
                        alert('University added successfully');
                        window.location.href = "<?php echo site_url('university'); ?>";
                    }
                   
                }
        });

    });

});
</script>

<script>
//EDIT STUDENT SCRIPT
//triggered when modal is about to be shown
$('#edit-university-modal').on('show.bs.modal', function(e) {
   
    //get data-id attribute of the clicked element
    var edituniversty_id = $(e.relatedTarget).data('id');
    var edit_universityname = $(e.relatedTarget).data('name');
    
    
    //populate the textboxes
    $(e.currentTarget).find('input[name="edituniversty_id"]').val(edituniversty_id);
    document.getElementById('edit_universityname').value = edit_universityname;
    
    
});

$(document).ready(function()	{
$(".updateunivrsity_submit").click(function(e)	{
        e.preventDefault(); //this will restrict the page refresh
        var form_data = {
            u_id:$('#edituniversty_id').val(),
            u_name: $('#edit_universityname').val(),
            
        };
    //alert(form_data.stud_id+" id "+form_data.stud_uname + form_data.stud_email + form_data.stud_website);
    //var form1 = new FormData(this);working till here.
    //var form2 = $('#editclient_form').serialize();
    $.ajax({
        type: "POST",
        //data:form_data,
        data: {
             u_id: form_data.u_id, 
            u_name: form_data.u_name, 
            
        },
        
        url: "<?php echo site_url('university/update_university'); ?>",
        
        success: function(res)  {
        var json = JSON.parse(res);
        console.log(json);
        //alert('herehere');
        
        if(json.st== 'fail'){
                $('#edituniversity_name_error').html(json.u_name);
                 
                
                }
        if(json.st =='success') {
               
                console.log(json);
                alert('University updated successfully');
                window.location.href = "<?php echo site_url('university'); ?>";
                }
        }
        });
    });
});
</script>
<script>
    $(document).ready(function() {
        
        $('#university-table').DataTable();
    });
</script>