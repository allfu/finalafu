<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-xs-12 mb-1">
                <h2 class="content-header-title">University</h2>
            </div>
            <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
                <div class="breadcrumb-wrapper col-xs-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url().'admin/admin_home'; ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo base_url().'events'; ?>">Events</a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!--Main content body section start-->
            <section id="card-actions">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">University information</h4>
                                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">

                                        <li><button type="button" class="btn btn-info btn-block" data-toggle="modal" data-target="#add-event-modal">
                                            <i class="icon-plus4" style="color:white;"></i>   Add University</button>
                                        </li>

                                        <!--<li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                                        <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                                        <li><a data-action="reload"><i class="icon-reload"></i></a></li>-->
                                    </ul>
                                </div>
                            </div>
                            <div class="card-body collapse in">
                                <div class="card-block">
                                    <div class="row">
                                        <div class="">
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped ">
                                                    <thead>
                                                        <tr>
                                                            <th>University Name</th>

                                                            <th>Permit</th>
                                                            <th>Actions</th>
                                                         </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php foreach($university->result() as $row)    {
                                                        ?>
                                                        <tr>
                                                            <td class="text-middle">
                                                                <?php echo $row->u_name; ?>
                                                            </td>

                                                    <td class="text-middle">
                                                                <?php echo $g; ?>
                                                            </td>
                                                            <td class="text-middle">
                                                                <?php if($row->permit == '1')  { ?>
                                                                <span class="tag tag-success">Active</span>
                                                                <?php } else { ?>
                                                                <span class="tag tag-danger">Banned</span>
                                                                <?php } ?>
                                                            </td>
                                                            <td><span class="btn-group">
                                                            <!--<input type="button" class="btn btn-outline-info" value="View" data-toggle="modal" data-target="#view-modal"/>-->
                                                            
															 <?php $query=$this->db->get_where('club_table',array('c_id'=>$row->e_by)); $i=$query->row(); 
															$grpby=$i->c_name;?>
                                                            
<input type="button" class="btn btn-outline-primary" value="Edit" 
data-toggle="modal" data-target="#edit-event-modal"
data-id="<?php echo $row->u_id;?>" data-name="<?php echo $row->u_name; ?>" 
                                                                
															/> 
<input type="button" class="btn btn-outline-danger" <?php 
if($row->permit==0){ echo "value='Deleted'";}else{ echo "value='Delete'"; ?>
data-toggle="modal" data-target="#delete-university-modal" 
data-id="<?php echo $row->u_id;?>" data-u_name="<?php echo $row->u__name; ?>" />
<?php } ?></span></td>
</tr>
 <?php } ?>
</tbody>
                                                </table>
                                                <?php echo $pagination; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
<!--Add Modal-->
<div class="modal fade text-xs-left" id="add-event-modal" tabindex="-1" role="dialog" aria-labelledby="addModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Add University Information</h4>
            </div>
            <form action="<?php //echo base_url().'clients/add_client';?>" method="post" id="addevent_form">
                <div class="modal-body">
                    <label> University Name:</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="u_name" value="<?php echo set_value('u_name');?>" id="u_name" />
                        <p id="u-name" style="color:red;"></p>
                    </div>




                </div>
                <div class="modal-footer">
                    <input type="reset" class="btn grey btn-outline-secondary" data-dismiss="modal" value="Cancel">
                    <input type="submit" class="btn btn-outline-primary" value="Submit" />
                </div>
            </form>
        </div>
    </div>
</div>
<!--Edit Modal-->
<div class="modal fade text-xs-left" id="edit-event-modal" tabindex="-1" role="dialog" aria-labelledby="editModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Edit University Information</h4>
            </div>
            <form method="post" id="editeventform" name="editeventform" action="">
                <div class="modal-body">
                    <input type="hidden" name="editevent_id" id="editevent_id" value="<?php echo set_value('editevent_id');?>" />
                    <!--value will be set through javascript-->
                    <label>University Name:</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="u_name" id="u_name" value="<?php echo set_value('u_name');?>" />
                    </div>
                    <p id="u-name" style="color:red;"></p>



                </div>
                <div class="modal-footer">
                    <input type="reset" class="btn grey btn-outline-secondary" data-dismiss="modal" value="close">
                    <input type="submit" class="btn btn-outline-primary updateevent_submit" value="Submit" />
                </div>
            </form>
            <p></p>
        </div>
    </div>
</div>
<!--Delete Modal-->
<div class="modal fade text-xs-left" id="delete-event-modal" tabindex="-1" role="dialog" aria-labelledby="deleteModal" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <form action="<?php echo base_url().'events/delete'; ?>" method="POST" class="delete-form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Delete University</h4>
                </div>
                <div class="modal-body deletepara">
                    <p></p>
                    <!--client information will go through here to jscript-->
                    <input type="hidden" name="del_uid" id="del_uid" value="" />
                    <!--value will be set through javascript-->
                    <input type="hidden" name="u_name" id="u_name" value="" />
                    <!--value will be set through javascript-->
                    <div id="result">
                        <div id="value"></div>
                        <div id="anothervalue"></div>
                    </div>
                </div>
            </form>
            <div class="modal-footer">
                <input type="reset" class="btn grey btn-outline-secondary" data-dismiss="modal" value="No">
                <input type="submit" class="btn grey btn-outline-danger delete-event-submit" value="Yes">
            </div>
        </div>
    </div>
</div>


<script>
    //DELETE EVENT SCRIPT
    $('#delete-event-modal').on('show.bs.modal', function(e) {

        //get data-id attribute of the clicked element
        var deleid = $(e.relatedTarget).data('id');
        var delename = $(e.relatedTarget).data('u_name');
        var modal = $(this);

        //populate the textboxes
        $(e.currentTarget).find('input[name="del_uid"]').val(deleid);
        document.getElementById('u_name').value = delename;
        modal.find('.modal-body p').html('Do you really want to delete - <b>' + delename + '</b>?');

    });

    $(document).ready(function() {
        $(".delete-event-submit").click(function(university) {
            university.preventDefault();

            var u_id = document.getElementById('del_uid').value;
            var u_name = document.getElementById('u_name').value;
            alert('deleting');
            jQuery.ajax({
                type: "POST",
                url: "<?php echo site_url('university/delete_university'); ?>",
                dataType: "json",
                data: {
                    id: u_id,
                    name: u_name
                },
                success: function(res) {
                    console.log(res);
                    if (res) {
                        alert('Event Deleted Successfully');
                        //$('#delete-client-modal').modal().hide();
                        window.location.href = "<?php echo site_url('university'); ?>";

                    } else {
                        alert('Delete not successful');
                        //window.location.href = "<?php echo site_url('university'); ?>";
                    }
                }
            });
        });
    });

</script>

<script type="text/javascript">
    //ADD EVENT SCRIPT
    $(function() { // <----------------missed the doc ready function
        $('#addevent_form').submit(function(e) {

            e.preventDefault(); // <------this will restrict the page refresh
            var form_data = {
                u_name: $('#u_name').val(),


            };
            //alert(form_data.e_name);
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + 'university/add_university',
                data: form_data,
                success: function(res) {
                    var json = JSON.parse(res);
                    console.log(json);
                    if (json.st == 'fail') {
                        $('#u-error').html(json.u_name);


                    }

                    if (json.st == 'success') {

                        alert('University added successfully');
                        window.location.href = "<?php //echo site_url('events'); ?>";
                    }

                }
            });

        });

    });

</script>

<script>
    //EDIT EVENT SCRIPT
    //triggered when modal is about to be shown
    $('#edit-event-modal').on('show.bs.modal', function(e) {

        //get data-id attribute of the clicked element
        var edituniversityid = $(e.relatedTarget).data('id');
        var edituniversityname = $(e.relatedTarget).data('u_name');


        //alert(editeventid);
        //populate the textboxes
        /* $(e.currentTarget).find('input[name="editevent_id"]').val(editeventid);
    document.getElementById('editevent_nam1').value = editeventname;
    document.getElementById('edit_eventname').value = editeventdesc;
    document.getElementById('edit_eventdesc').value = editeventdesc;
    document.getElementById('edit_eventstart').value = editeventstart;
    document.getElementById('edit_eventend').value = editeventlend;  */
        $('#edituniversityid').val(edituniversityid);
        $('#edituniversityname').val(edituniversityname);




    });

    $(document).ready(function() {
        $(".updateevent_submit").click(function(e) {
            e.preventDefault(); //this will restrict the page refresh
            var form_data = {
                u_id: $('#edituniversityid').val(),
                u_name: $('#edituniversityname').val(),


            };
            //alert(form_data.stud_id+" id "+form_data.stud_uname + form_data.stud_email + form_data.stud_website);
            //var form1 = new FormData(this);working till here.
            //var form2 = $('#editclient_form').serialize();
            $.ajax({
                type: "POST",
                //data:form_data,
                data: {
                    u_id: form_data.u_id,
                    u_name: form_data.u_name,


                },

                url: "<?php echo site_url('university/update_university'); ?>",

                success: function(res) {
                    var json = JSON.parse(res);
                    console.log(json);
                    //alert('herehere');

                    if (json.st == 'fail') {
                        $('#u-name').html(json.u_name);




                    }
                    if (json.st == 'success') {

                        console.log(json);
                        alert('University updated successfully');
                        window.location.href = "<?php echo site_url('university'); ?>";
                    }
                }
            });
        });
    });

    $(document).ready(function() {
        $('.js-example-basic-single').select2();
    });

</script>
