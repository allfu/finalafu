<style>
	input[type=text] {
		width: 240px;
		box-sizing: border-box;
		border: 1px solid #ccc;
		border-radius: 25px;
		font-size: 14px;
		background-color: white;
		background-image: url('<?php echo base_url().'assets/updatedFrontend/images/icon-search.png'?>');
		background-position: 10px 10px; 
		background-repeat: no-repeat;
		padding: 6px 20px 6px 40px;
		-webkit-transition: width 0.4s ease-in-out;
		transition: width 0.4s ease-in-out;
	}

	input[type=text]:focus {
		width: 100%;
	}
	@media only screen and (min-device-width : 320px) and (max-device-width : 480px) {
		.pricing {
			width: 240px;
			height: 850px;
		}
	}
</style>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div>
				<ul class="nav nav-tabs editabletabbar">
					<li role="presentation" class="<?php if($this->uri->segment(2) == 'leader_home'){echo 'active';} ?>" ><a href="<?php echo base_url().'leader/leader_home';?>" class="tabmenu" data-target="manage-event">My Events</a></li>
					<li role="presentation" class=""><a href="<?php echo base_url().'leader/my_groups';?>" class="tabmenu" data-target="manage-group">My Groups</a></li>
				</ul>
			</div>
			<div class="content-wrapsec">
				<div class="row">
					<div class="col-md-10 col-sm-10 col-xs-12">
						<h3>Source Sponsor</h3>
					</div>								
				</div>
				<div class="borderbottomsec"></div>
				<div class="row">
					<div class="col-md-2 col-sm-2">
						<label>Filter Sponsors :</label>
					</div>
					<div class="col-md-2 col-sm-2">
						<input type="radio" name="status" value="male" checked> <span style="padding-left:7px">  All</span>
					</div>
					<div class="col-md-1 col-sm-2">
						<input type="radio" name="status" value="male" checked> <span style="padding-left:7px"> New </span>
					</div>
					<div class="col-md-2 col-sm-2">
						<input type="radio" name="status" value="male" checked> <span style="padding-left:7px"> Pending Approval</span>
					</div>
					<div class="col-md-2 col-sm-2">
						<input type="radio" name="status" value="male" checked> <span style="padding-left:7px">  Approved</span>
					</div>
					<div class="col-md-2 col-sm-2">
						<input type="radio" name="status" value="male" checked> <span style="padding-left:7px">  Rejected</span>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-2 col-sm-2">
						<label>Filter By Rewards :</label>
					</div>
					<div class="col-md-6 col-sm-6">
						<select class="form-control">
						  <option value="volvo">Cash</option>
						  <option value="saab">Products</option>
						  <option value="mercedes">Venue</option>
						  <option value="audi">Vouchers</option>
						</select>
					</div>
					<div class="visible-xs">
						<br/>
					</div>
					<div class="col-md-4 col-sm-4">
						<input type="text" name="search" placeholder="Search..">
					</div>
				</div>
				<div class="borderbottomsec"></div>			
				<!--Block Section-->
				<div class="row">
					<div class="col-md-12">
						<form id="sponsor_insert"  name="sponsor_insert" action="" method="POST">
							<div class="row main-sec">
								<?php foreach($e_sponsors as $s)
									{  $row = $this->db->get_where('sponsor_table',array('s_id'=> $s->spn_id,'permit'=>1))->row();
									{?>
									<div class="col-sm-6 col-sm-6">
										<div class="pricing pricing-1 boxed--border container-sec" style="width:100%">
											<section class="allowpadding">
												<div class="content-center" style="padding-top:5px">
													<label><?php echo $row->s_name;?></label>
												</div>
												<div class="content-center form-group">
													<input type="button" value="New !" class="btn btn-danger"/>
												</div>
												<!--<div class="content-center form-group">
													<input type="button" value="Pending Approval" class="btn btn-default" style="background-color:#f2f2f2"/>
												</div>-->
												<div class="row">
													<div class="col-md-5">
														<div class="sponsorimg-sec">
															<?php if(isset($row->s_logo)){?>
																<img alt="Image"  src="<?php echo base_url().$row->s_logo;?>" width="100%" height="150px" />
																<?php } 
																else{?>
																<img alt="Image"  src="<?php echo base_url().'assets/frontend/';?>img/xyz.jpg" width="100%" height="150px" />
															<?php } ?>												
														</div>	
													</div>
													<div class="col-md-7">
														<div>
															<label>Description</label>
															<p><?php echo $row->s_desc;?></p>
														</div>
														<div class="form-group">
															<label>Reward Type</label>
															<p><?php echo $row->reward_type;?></p>
														</div>	
													</div>
												</div>
												<div class="borderbottomsec"></div>		
												<div class="row" style="padding:10px">
													<div class="col-md-6">
														<label>Tasks To Be Done</label><br/>
														<?php echo $row->tasks;?>
													</div>
													<div class="col-md-6">
														<label>Rewards</label><br/>
														<?php echo $row->reward;?>
													</div>
												</div>
												<br/>
												<div class="row">
													<div class="col-md-12">
													<?php if($s == 'new'){ echo "new"; } 
														  else if($s== 'pending'){echo "pending approval";	}
														  else if($s== 'rejected'){echo "rejected";	}
													?>
														<div style="text-align:center">
															
															<a class="btn btn-info btn-join" href="">Request Now !</a>
															
														</div>
													<!--	<div style="text-align:center">
															<a class="btn btn-danger btn-join" href="javascript:void(0)">Cancel Request !</a>
														</div>-->
													</div>
												</div>	
											</section>
										</div>
									</div>
									<?php }} ?>
							</div>
						</form>						
					</div>
				</div>					
			</div>
		</div>
	</div>
</div>

<script>
$(document).ready(function() {
    $('.js-example-basic-single').select2();
});
        $(document).ready(function() {
    $('.js-example-basic-multiple').select2();
});
</script>
	  <script>
$(document).ready(function(e)  {
        $("#upload_sponsor").click(function(e)  {
		e.preventDefault();
		//var formData = $('#sponsor_insert');
		var formData =  $('#sponsor_insert').serialize()
		//var formData =  new FormData(this);
		console.log(formData);
		$.ajax({
                type: "POST",
                url: "<?php echo base_url().'leader/sponsor_submit'; ?>",
				processData: false,
                data: formData,
                success: function(res)  {
					var parsed = JSON.parse(res);
                    console.log(parsed);
                   if(parsed.status_code == 1) {
                       // alert('data inserted successfully');
                       // window.location.href = "<?php echo site_url('leader/leader_home'); ?>";
                        swal({
                                html: parsed.status,
                                type: 'success'
                        }).then(function() {
                            window.location.href = "<?php echo site_url('leader/leader_home'); ?>";
                        });
                    }
                    else if(parsed.status_code == 0)    {
                        alert(parsed.status);
                        swal({
                                html: parsed.status,
                                type: 'error',
                            });
                        }
                    else    {
                            swal({
                                html: parsed.status,
                                type: 'error',
                            });
                            //alert(parsed.status);
                        //$('#form-error').html(parsed.status);
					
					}
				}
		});
		});
	});
</script>
