<html>

<?php
if (isset($this->session->userdata['logged_in'])) {

//header("location: http://localhost/login/index.php/user_authentication/user_login_process");
}
?>
<head>
<title>AFU</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.8/sweetalert2.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css">
<script src="<?php echo base_url().'assets/';?>jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.8/sweetalert2.min.js"></script>
<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro|Open+Sans+Condensed:300|Raleway' rel='stylesheet' type='text/css'>
<style>
	body{
		background-image: url("<?php echo base_url().'assets/img/back.jpg'; ?>");
		background-size: cover;
		font-family:Calibri;
	}
	#login 	{
		background-color: #fff;
		font-family:Calibri;
		font-weight:500;
		}
	#main{
    width:500px;
    margin:0px auto;
    font-family:Calibri;
}

span{
    color:black;
}

h2{
    background-color: #fcfdff;
    text-align:center;
    border-radius: 10px 10px 0 0;
    margin: -10px -40px;
    padding: 50px;
	font-family:Calibri;
}


select{
    width:99.5%;
    padding: 10px;
    margin-top: 8px;
    border: 1px solid #ccc;
    padding-left: 5px;
    font-size: 16px;
    font-family:Calibri;
}
input[type=text],input[type=password], input[type=email]{
    width:99.5%;
    padding: 10px;
    margin-top: 8px;
    border: 1px solid #ccc;
    padding-left: 5px;
    font-size: 16px;
    font-family:Calibri;
}





#profile{
    padding:50px;
    border:1px dashed grey;
    font-size:20px;
    background-color:#DCE6F7;
}

#logout{
    float:right;
    padding:5px;
    border:dashed 1px gray;
    margin-top: -168px;
	font-family:Calibri;
}

a{
    text-decoration:none;
    color: cornflowerblue;
}

i{
color: cornflowerblue;
}

.error_msg{
color:red;
font-size: 16px;
}

.message{
position: absolute;
font-weight: bold;
font-size: 28px;
color: #6495ED;
left: 262px;
width: 800px;
text-align: center;
}
 input[type=text],input[type=password], input[type=email]{
    width:99.5%;
    padding: 10px;
    margin-top: 8px;
    border: 1px solid #ccc;
    padding-left: 5px;
    font-size: 16px;
   font-family:Calibri;
} 

 
</style>
</head>
<body>
<?php
if (isset($logout_message)) {
echo "<div class='message'>";
echo $logout_message;
echo "</div>";
}
?>
<?php
if (isset($message_display)) {
echo "<div class='message'>";
echo $message_display;
echo "</div>";
}
?>
<div id="">
	<div class="container-fluid">
			<div class="row">
				<div class="col-md-offset-4 col-md-4">
					<div id="login">
						<h2>Leader Login Form</h2>
						<hr/>
						<?php echo form_open('user_authentication/user_login'); ?>
						<?php
						echo "<div class='error_msg'>";
						if (isset($error_message)) {
						echo $error_message;
						}
						echo validation_errors();
						echo "</div>";
						?>
						<label>UserName :</label>
						<div class="row">
							<div class="col-md-12">
								<input type="text" name="username" id="name" placeholder="Name" required/><br /><br />
							</div>
						</div>
						
						<label>Password :</label>
						<input type="password" name="password" id="password" placeholder="Password" required/><br/><br />
						<input type="submit" class="btn btn-primary" value=" Login " name="submit"/><br />
						<br /><br />
						<div style="text-align:center">
							<a href="<?php echo base_url() ?>user_authentication/user_registration">New User ? Register Here</a>
						</div>
						

						<?php echo form_close(); ?>
						</div>
					</div>
				</div>
		</div>

</div>
</body>
</html>