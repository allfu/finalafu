<!-- navbar-fixed-top-->
    <nav class="header-navbar navbar navbar-with-menu navbar-fixed-top navbar-semi-dark navbar-shadow">
      <div class="navbar-wrapper">
        <div class="navbar-header">
          <ul class="nav navbar-nav">
            <li class="nav-item mobile-menu hidden-md-up float-xs-left"><a class="nav-link nav-menu-main menu-toggle hidden-xs"><i class="icon-menu5 font-large-1"></i></a></li>
            <li class="nav-item" style="margin-left: 20%;"><!--<a href="<?php //echo base_url().'students/student_home'; ?>" class="navbar-brand nav-link">-->
			<img alt="branding logo" src="<?php echo base_url().'assets/backend/img/logo/afu.png'; ?>" data-expand="<?php echo base_url().'assets/backend/img/logo/afu.png'; ?>" data-collapse="<?php echo base_url().'assets/backend/img/logo/afu.png'; ?>" class="brand-logo" style="height: auto; width: 100px; padding-top: 10px;">
			<!--<h2 class="brand-logo" style="color:#fff;">AFU</h2>-->
			</a></li>
            <li class="nav-item hidden-md-up float-xs-right"><a data-toggle="collapse" data-target="#navbar-mobile" class="nav-link open-navbar-container"><i class="icon-ellipsis pe-2x icon-icon-rotate-right-right"></i></a></li>
          </ul>
        </div>
        <div class="navbar-container content container-fluid">
          <div id="navbar-mobile" class="collapse navbar-toggleable-sm">
            <ul class="nav navbar-nav">
              <li class="nav-item hidden-sm-down"><a class="nav-link nav-menu-main menu-toggle hidden-xs"><i class="icon-menu5">         </i></a></li>
              <li class="nav-item hidden-sm-down"><a href="#" class="nav-link nav-link-expand"><i class="ficon icon-expand2"></i></a></li>
            </ul>
            <ul class="nav navbar-nav float-xs-right">
              <!--<li class="dropdown dropdown-language nav-item"><a id="dropdown-flag" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle nav-link"><i class="flag-icon flag-icon-gb"></i><span class="selected-language">English</span></a>
                <div aria-labelledby="dropdown-flag" class="dropdown-menu"><a href="#" class="dropdown-item"><i class="flag-icon flag-icon-gb"></i> English</a><a href="#" class="dropdown-item"><i class="flag-icon flag-icon-fr"></i> French</a><a href="#" class="dropdown-item"><i class="flag-icon flag-icon-cn"></i> Chinese</a><a href="#" class="dropdown-item"><i class="flag-icon flag-icon-de"></i> German</a></div>
              </li>-->
              <li class="dropdown dropdown-notification nav-item"><a href="#" data-toggle="dropdown" class="nav-link nav-link-label"><i class="ficon icon-bell4"></i><span class="tag tag-pill tag-default tag-danger tag-default tag-up">5</span></a>
                <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                  <li class="dropdown-menu-header">
                    <h6 class="dropdown-header m-0"><span class="grey darken-2">Notifications</span><span class="notification-tag tag tag-default tag-danger float-xs-right m-0">5 New</span></h6>
                  </li>
                  <li class="list-group scrollable-container"><a href="javascript:void(0)" class="list-group-item">
                      <div class="media">
                        <div class="media-left valign-middle"><i class="icon-cart3 icon-bg-circle bg-cyan"></i></div>
                        <div class="media-body">
                          <h6 class="media-heading">You have new order!</h6>
                          <p class="notification-text font-small-3 text-muted">Lorem ipsum dolor sit amet, consectetuer elit.</p><small>
                            <time datetime="2015-06-11T18:29:20+08:00" class="media-meta text-muted">30 minutes ago</time></small>
                        </div>
                      </div></a><a href="javascript:void(0)" class="list-group-item">
                      <div class="media">
                        <div class="media-left valign-middle"><i class="icon-monitor3 icon-bg-circle bg-red bg-darken-1"></i></div>
                        <div class="media-body">
                          <h6 class="media-heading red darken-1">99% Server load</h6>
                          <p class="notification-text font-small-3 text-muted">Aliquam tincidunt mauris eu risus.</p><small>
                            <time datetime="2015-06-11T18:29:20+08:00" class="media-meta text-muted">Five hour ago</time></small>
                        </div>
                      </div></a><a href="javascript:void(0)" class="list-group-item">
                      <div class="media">
                        <div class="media-left valign-middle"><i class="icon-server2 icon-bg-circle bg-yellow bg-darken-3"></i></div>
                        <div class="media-body">
                          <h6 class="media-heading yellow darken-3">Warning notifixation</h6>
                          <p class="notification-text font-small-3 text-muted">Vestibulum auctor dapibus neque.</p><small>
                            <time datetime="2015-06-11T18:29:20+08:00" class="media-meta text-muted">Today</time></small>
                        </div>
                      </div></a><a href="javascript:void(0)" class="list-group-item">
                      <div class="media">
                        <div class="media-left valign-middle"><i class="icon-check2 icon-bg-circle bg-green bg-accent-3"></i></div>
                        <div class="media-body">
                          <h6 class="media-heading">Complete the task</h6><small>
                            <time datetime="2015-06-11T18:29:20+08:00" class="media-meta text-muted">Last week</time></small>
                        </div>
                      </div></a><a href="javascript:void(0)" class="list-group-item">
                      <div class="media">
                        <div class="media-left valign-middle"><i class="icon-bar-graph-2 icon-bg-circle bg-teal"></i></div>
                        <div class="media-body">
                          <h6 class="media-heading">Generate monthly report</h6><small>
                            <time datetime="2015-06-11T18:29:20+08:00" class="media-meta text-muted">Last month</time></small>
                        </div>
                      </div></a></li>
                  <li class="dropdown-menu-footer"><a href="javascript:void(0)" class="dropdown-item text-muted text-xs-center">Read all notifications</a></li>
                </ul>
              </li>
              <li class="dropdown dropdown-notification nav-item"><a href="#" data-toggle="dropdown" class="nav-link nav-link-label"><i class="ficon icon-mail6"></i><span class="tag tag-pill tag-default tag-info tag-default tag-up">8</span></a>
                <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                  <li class="dropdown-menu-header">
                    <h6 class="dropdown-header m-0"><span class="grey darken-2">Messages</span><span class="notification-tag tag tag-default tag-info float-xs-right m-0">4 New</span></h6>
                  </li>
                  <li class="list-group scrollable-container"><a href="javascript:void(0)" class="list-group-item">
                      <div class="media">
                        <div class="media-left"><span class="avatar avatar-sm avatar-online rounded-circle"><img src="<?php echo base_url().'assets/backend/img/portrait/small/avatar-s-1.png'; ?>" alt="avatar"><i></i></span></div>
                        <div class="media-body">
                          <h6 class="media-heading">Margaret Govan</h6>
                          <p class="notification-text font-small-3 text-muted">I like your portfolio, let's start the project.</p><small>
                            <time datetime="2015-06-11T18:29:20+08:00" class="media-meta text-muted">Today</time></small>
                        </div>
                      </div></a><a href="javascript:void(0)" class="list-group-item">
                      <div class="media">
                        <div class="media-left"><span class="avatar avatar-sm avatar-busy rounded-circle"><img src="<?php echo base_url().'assets/backend/img/portrait/small/avatar-s-2.png'; ?>" alt="avatar"><i></i></span></div>
                        <div class="media-body">
                          <h6 class="media-heading">Bret Lezama</h6>
                          <p class="notification-text font-small-3 text-muted">I have seen your work, there is</p><small>
                            <time datetime="2015-06-11T18:29:20+08:00" class="media-meta text-muted">Tuesday</time></small>
                        </div>
                      </div></a><a href="javascript:void(0)" class="list-group-item">
                      <div class="media">
                        <div class="media-left"><span class="avatar avatar-sm avatar-online rounded-circle"><img src="../../app-assets/images/portrait/small/avatar-s-3.png" alt="avatar"><i></i></span></div>
                        <div class="media-body">
                          <h6 class="media-heading">Carie Berra</h6>
                          <p class="notification-text font-small-3 text-muted">Can we have call in this week ?</p><small>
                            <time datetime="2015-06-11T18:29:20+08:00" class="media-meta text-muted">Friday</time></small>
                        </div>
                      </div></a><a href="javascript:void(0)" class="list-group-item">
                      <div class="media">
                        <div class="media-left"><span class="avatar avatar-sm avatar-away rounded-circle"><img src="<?php echo base_url().'assets/backend/img/portrait/small/avatar-s-6.png'; ?>" alt="avatar"><i></i></span></div>
                        <div class="media-body">
                          <h6 class="media-heading">Eric Alsobrook</h6>
                          <p class="notification-text font-small-3 text-muted">We have project party this saturday night.</p><small>
                            <time datetime="2015-06-11T18:29:20+08:00" class="media-meta text-muted">last month</time></small>
                        </div>
                      </div></a></li>
                  <li class="dropdown-menu-footer"><a href="javascript:void(0)" class="dropdown-item text-muted text-xs-center">Read all messages</a></li>
                </ul>
              </li>
              <li class="dropdown dropdown-user nav-item"><a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link dropdown-user-link"><span class="avatar avatar-online">
			  <img src="<?php echo base_url().'assets/backend/img/portrait/small/avatar-s-1.png'; ?>" alt="avatar"><i></i></span>
			  <span class="user-name">
			  <?php if (isset($this->session->userdata['adminlogged_in'])) {
				$username = ($this->session->userdata['adminlogged_in']['adminname']);
			    echo $username;}?>
				</span></a>
                <div class="dropdown-menu dropdown-menu-right"><a href="#" class="dropdown-item"><i class="icon-head"></i> Edit Profile</a><a href="#" class="dropdown-item"><i class="icon-mail6"></i> My Inbox</a><a href="#" class="dropdown-item"><i class="icon-clipboard2"></i> Task</a><a href="#" class="dropdown-item"><i class="icon-calendar5"></i> Calender</a>
                  <div class="dropdown-divider"></div>
				  <a href="<?php echo base_url().'admin/logout'; ?>" class="dropdown-item"><i class="icon-power3"></i> Logout</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </nav>

    <!-- ////////////////////////////////////////////////////////////////////////////-->


    <!-- main menu-->
    <div data-scroll-to-active="true" class="main-menu menu-fixed menu-dark menu-accordion menu-shadow">
      <!-- main menu header-->
      <div class="main-menu-header">
        <input type="text" placeholder="Search" class="menu-search form-control round"/>
      </div>
      <!-- / main menu header-->
      <!-- main menu content-->
      <div class="main-menu-content">
        <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
          <li class="nav-item <?php if($this->uri->segment(1) == 'students'){ echo 'active';} ?>"><a href="<?php echo base_url().'students/student_home'; ?>"><i class="icon-home3"></i><span data-i18n="nav.dash.main" class="menu-title">Dashboard</span><!--<span class="tag tag tag-primary tag-pill float-xs-right mr-2">2</span>--></a>
          </li>
	
          <li class="nav-item <?php if($this->uri->segment(1) == 'admin'){ echo 'active';} ?>"><a href="#"><i class="icon-users"></i><span data-i18n="nav.page_layouts.main" class="menu-title">Students</span></a>
            <ul class="menu-content">
              <li><a href="<?php echo base_url().'students'; ?>" data-i18n="nav.page_layouts.1_column" class="menu-item">
			   Manage students</a>
              </li>
              
              <!--<li><a href="<?php //echo base_url().'users/reset'; ?>" data-i18n="nav.page_layouts.boxed_layout" class="menu-item">Reset Password</a>
              </li>-->
            </ul>
          </li>
		  
          <li class="nav-item <?php if($this->uri->segment(1) == 'groups'){ echo 'active';} ?>"><a href="#"><i class="icon-group"></i><span data-i18n="nav.page_layouts.main" class="menu-title">Groups</span></a>
            <ul class="menu-content">
              <li><a href="<?php echo base_url().'groups'; ?>" data-i18n="nav.page_layouts.1_column" class="menu-item">Manage groups</a>
              </li>
              
            </ul>
          </li>
		  
		  <?php //echo $this->session->userdata['logged_in']['sessiontype'];?>
          <li class="nav-item <?php if($this->uri->segment(1) == 'events') { echo 'active';} ?>"><a href="#"><i class="icon-shop"></i><span data-i18n="nav.project.main" class="menu-title">Events</span></a>
            <ul class="menu-content">
              <li><a href="<?php echo base_url().'events'; ?>" data-i18n="nav.invoice.invoice_template" class="menu-item"> Manage Events</a>
              </li>
              
            </ul>
          </li>
            <?php //echo $this->session->userdata['logged_in']['sessiontype'];?>
          <li class="nav-item <?php if($this->uri->segment(1) == 'university') { echo 'active';} ?>"><a href="#"><i class="icon-drawer"></i><span data-i18n="nav.project.main" class="menu-title">University</span></a>
            <ul class="menu-content">
              <li><a href="<?php echo base_url().'university'; ?>" data-i18n="nav.invoice.invoice_template" class="menu-item"> Manage  University</a>
              </li>
              
            </ul>
          </li>
               
           <!--<li class="nav-item <?php //if($this->uri->segment(1) == 'Points') { echo 'active';} ?>"><a href="#"><i class="icon-shop"></i><span data-i18n="nav.project.main" class="menu-title">Manage Point Rules</span></a>
            <ul class="menu-content">
              <li><a href="<?php //echo base_url().'points'; ?>" data-i18n="nav.invoice.invoice_template" class="menu-item">Manage Point Rules</a>
              </li>
              
            </ul>
          </li>-->
            <?php //echo $this->session->userdata['logged_in']['sessiontype'];?>
          <li class="nav-item <?php if($this->uri->segment(1) == 'hall') { echo 'active';} ?>"><a href="#"><i class="icon-envelope"></i><span data-i18n="nav.project.main" class="menu-title">Hall</span></a>
            <ul class="menu-content">
              <li><a href="<?php echo base_url().'hall'; ?>" data-i18n="nav.invoice.invoice_template" class="menu-item"> Manage  Hall</a>
              </li>
              
            </ul>
          </li>
		 
            <li class="nav-item <?php if($this->uri->segment(1) == 'faculty') { echo 'active';} ?>"><a href="#"><i class="icon-magic-wand"></i><span data-i18n="nav.cards.main" class="menu-title">Faculty</span></a>
            <ul class="menu-content">
              <li><a href="<?php echo base_url().'faculty';?>" data-i18n="nav.cards.card_bootstrap" class="menu-item"> Manage Faculty</a>
              </li>
              
            </ul>
          </li>
		  
		   <li class="nav-item <?php if($this->uri->segment(1) == 'sponsors') { echo 'active';} ?>"><a href="#"><i class="icon-list"></i><span data-i18n="nav.cards.main" class="menu-title">Sponsors</span></a>
            <ul class="menu-content">
              <li><a href="<?php echo base_url().'sponsors';?>" data-i18n="nav.cards.card_bootstrap" class="menu-item"> Manage Sponsors</a>
           </li>
              
            </ul>
          </li>
		  
		   <li class="nav-item <?php if($this->uri->segment(1) == 'sponsors') { echo 'active';} ?>"><a href="#"><i class="icon-list"></i><span data-i18n="nav.cards.main" class="menu-title">Approve Sponsors</span></a>
            <ul class="menu-content">
              <li><a href="<?php echo base_url().'sponsors';?>" data-i18n="nav.cards.card_bootstrap" class="menu-item"> Approve Sponsors for events</a>
              </li>
              
            </ul>
          </li>
          <!--<li class="nav-item <?php if($this->uri->segment(1) == 'appointment') { echo 'active';} ?>"><a href="#"><i class="icon-calendar"></i><span data-i18n="nav.content.main" class="menu-title">Manage Appointment</span></a>
            <ul class="menu-content">
              <li><a href="content-grid.html" data-i18n="nav.content.content_grid" class="menu-item">View User / Patient Request</a>
              </li>
              <li><a href="content-typography.html" data-i18n="nav.content.content_typography" class="menu-item">Book Appointment</a>
              </li>
              <li><a href="content-text-utilities.html" data-i18n="nav.content.content_text_utilities" class="menu-item">Cancel Appointment</a>
              </li>
            </ul>
          </li>
          <li class="nav-item <?php if($this->uri->segment(1) == 'category') { echo 'active';} ?>"><a href="#"><i class="icon-list"></i><span data-i18n="nav.components.main" class="menu-title">Manage Category</span></a>
            <ul class="menu-content">
              <li><a href="component-alerts.html" data-i18n="nav.components.component_alerts" class="menu-item">Add Category & Sub Category</a>
              </li>
              <li><a href="component-buttons-basic.html" data-i18n="nav.components.components_buttons.component_buttons_basic" class="menu-item">Update Category & Sub Category</a>
              </li>
              <li><a href="component-carousel.html" data-i18n="nav.components.component_carousel" class="menu-item">View Category & Sub Category</a>
              </li>
              <li><a href="component-collapse.html" data-i18n="nav.components.component_collapse" class="menu-item">Delete Category & Sub Category</a>
              </li>
            </ul>
          </li>
          <li class="nav-item <?php if($this->uri->segment(1) == 'products') { echo 'active';} ?>"><a href="#"><i class="icon-drawer"></i><span data-i18n="nav.icons.main" class="menu-title">Manage Products</span></a>
            <ul class="menu-content">
              <li><a href="icons-feather.html" data-i18n="nav.icons.icons_feather" class="menu-item">View Products</a>
              </li>
              <li><a href="icons-ionicons.html" data-i18n="nav.icons.icons_ionicons" class="menu-item">Add / Delete Products</a>
              </li>
              <li><a href="icons-fps-line.html" data-i18n="nav.icons.icons_fps_line" class="menu-item">Upload Photos for Products</a>
              </li>
            </ul>
          </li>
          <li class="nav-item <?php if($this->uri->segment(1) == 'featured') { echo 'active';} ?>"><a href="#"><i class="icon-magic-wand"></i><span data-i18n="nav.chartjs.main" class="menu-title">Manage Feature products</span></a>
            <ul class="menu-content">
              <li><a href="chartjs-line-charts.html" data-i18n="nav.chartjs.chartjs_line_charts" class="menu-item">View Products added per category</a>
              </li>
              <li><a href="chartjs-bar-charts.html" data-i18n="nav.chartjs.chartjs_bar_charts" class="menu-item">Add / Delete Products info per category</a>
              </li>
            </ul>
          </li>
          <li class="nav-item <?php if($this->uri->segment(1) == 'showcase'){ echo 'active';} ?>"><a href="#"><i class="icon-bookmark"></i><span data-i18n="nav.flot_charts.main" class="menu-title">Manage Showcase</span></a>
            <ul class="menu-content">
              <li><a href="flot-line-charts.html" data-i18n="nav.flot_charts.flot_line_charts" class="menu-item">View Advertisements</a>
              </li>
              <li><a href="flot-bar-charts.html" data-i18n="nav.flot_charts.flot_bar_charts" class="menu-item">Edit / Delete Advertisement</a>
              </li>
            </ul>
          </li>
          <li class="nav-item <?php if($this->uri->segment(1) == 'issues'){echo 'active';} ?>"><a href="#"><i class="icon-map22"></i><span data-i18n="nav.maps.main" class="menu-title">Manage issues</span></a>
            <ul class="menu-content">
              <li><a href="gmaps-basic-maps.html" data-i18n="nav.maps.google_maps.gmaps_basic_maps" class="menu-item">View / Reply Issues</a>
              </li>
            </ul>
          </li>
          <li class="nav-item <?php if($this->uri->segment(1) == 'newsletter'){echo 'active';} ?>"><a href="#"><i class="icon-envelope"></i><span data-i18n="nav.color_palette.main" class="menu-title">Manage newsletter</span></a>
            <ul class="menu-content">
              <li><a href="color-palette-primary.html" data-i18n="nav.color_palette.color_palette_primary" class="menu-item">Add / Delete News Articles</a>
              </li>
              <li><a href="color-palette-danger.html" data-i18n="nav.color_palette.color_palette_danger" class="menu-item">Subscribe / Unsubscribe Users</a>
              </li>
            </ul>
          </li>
          <li class="nav-item <?php if($this->uri->segment(1) == 'forum'){echo 'active';} ?>"><a href="#"><i class="icon-feed"></i><span data-i18n="nav.menu_levels.main" class="menu-title">Manage Forum & Blogs</span></a>
            <ul class="menu-content">
              <li><a href="#" data-i18n="nav.menu_levels.second_level" class="menu-item">View Messages</a>
              </li>
              <li><a href="#" data-i18n="nav.menu_levels.second_level_child.main" class="menu-item">Post / Edit Messages</a>
              </li>
            </ul>
          </li>
          <li class="nav-item <?php if($this->uri->segment(1) == 'reports'){echo 'active';} ?>"><a href="#"><i class="icon-stack"></i><span data-i18n="nav.menu_levels.main" class="menu-title">Reports</span></a>
            <ul class="menu-content">
              <li><a href="#" data-i18n="nav.menu_levels.second_level" class="menu-item">Reports for Registered users</a>
              </li>
              <li><a href="#" data-i18n="nav.menu_levels.second_level_child.main" class="menu-item">Per Status</a>
              </li>
              <li><a href="#" data-i18n="nav.menu_levels.second_level_child.main" class="menu-item">Report on Products listed</a>
              </li>
              <li><a href="#" data-i18n="nav.menu_levels.second_level_child.main" class="menu-item">Report on Advertisements Posted</a>
              </li>
            </ul>
          </li> -->
          <li class="nav-item"><a href="<?php echo base_url().'admin/logout'; ?>"><i class="icon-exit"></i><span data-i18n="nav.menu_levels.main" class="menu-title">Logout</span></a>
          </li>
        </ul>
      </div>
      <!-- /main menu content-->
      <!-- main menu footer-->
      <!-- include includes/menu-footer-->
      <!-- main menu footer-->
    </div>
    <!-- / main menu-->