<?php
	//var_dump($user_profile);
	//echo "<pre>";
	//print_r($user_profile);
	//echo "</pre>";
?>
<html>
    <head>
        <!--<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css" rel="stylesheet">-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.8/sweetalert2.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css">
        <link href="<?php echo base_url().'assets/backend/css/bootstrap-select2.css'; ?>" rel="stylesheet" />
		<link href="<?php echo base_url().'assets/frontend/';?>css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
        <script src="<?php echo base_url().'assets/';?>jquery.js"></script>
        <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.8/sweetalert2.min.js"></script>
        <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro|Open+Sans+Condensed:300|Raleway' rel='stylesheet' type='text/css'>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
		
		 <!--SweetAlert 2 CSS-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.7/sweetalert2.min.css" />
		
		
        <style>
            body {
                background-image: url("<?php echo base_url().'assets/img/profileback.jpg'; ?>");
                background-size: cover;
            }
            #login {
                background-color: #fff;
            }
            .center-tag {
                text-align: center;
            }
            .select2-container .select2-selection--single   {
                height: 37px;
                border: 1px solid #ccc;
                box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            }
            .select2-container--default .select2-selection--single .select2-selection__rendered {
                color: #999;
                line-height: 35px;
            }
            .display-name   {
                padding-bottom: 20px;
            }
			::-webkit-input-placeholder {
				text-align: center;
			}

			:-moz-placeholder { /* Firefox 18- */
			   text-align: center;  
			}

			::-moz-placeholder {  /* Firefox 19+ */
			   text-align: center;  
			}

			:-ms-input-placeholder {  
			   text-align: center; 
			}
			input[type="submit"] {
    width: 100%;
    background-color: #5cb85c;
    color: white;
    border: 2px solid #5cb85c;
    padding: 10px;
    font-size: 20px;
    cursor: pointer;
    border-radius: 5px;
    margin-bottom: 15px;
}
        </style>
    </head>
    <body>
		<?php //print_r($this->session->userdata('user_profile')); ?>
		<div class="container-fluid" style="margin-top: 8%">
		
			<div class="row">
			
				<div class="col-md-offset-1 col-md-5">
				<br/>
					<div class="row">
						<div class="col-md-12">
							<div style="text-align:center">
								<img src="<?php echo $this->session->userdata['user_profile']['fb_pic'];?>" style="border-radius:50%; height: 120px;">
							</div>							
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-12">
							 <h2 class="display-name"  style="color:#fff;text-align:center;"><?php echo $this->session->userdata['user_profile']['user_name'];?></h2>
						</div>
					</div>
				</div>
				<div class="col-md-5">
				<br/>
				<br/>
				<br/>
					 <form action="" method="POST" id="studreg_form">
						<input type="hidden" id="fbpic" name="fbpic" value="<?php echo $this->session->userdata['user_profile']['fb_pic'];?>"/>
						<input type="hidden" id="fbdisplayname" name="fbdisplayname" value="<?php echo $this->session->userdata['user_profile']['user_name'];?>"/>
						<input type="hidden" id="fbfname" name="fbfname" value="<?php echo $this->session->userdata['user_profile']['firstname'];?>"/>
						<input type="hidden" id="fblname" name="fblname" value="<?php echo $this->session->userdata['user_profile']['lastname'];?>"/>
						<input type="hidden" id="fbmail" name="fbmail" value="<?php echo $this->session->userdata['user_profile']['user_email'];?>"/>
						<input type="hidden" id="fbid" name="fbmid" value="<?php echo $this->session->userdata['user_profile']['fb_id'];?>"/>
						
						<div class="row">				
							<div class="col-md-10">
								 <input type="text" name="mat_no" id="mat_no" class="form-control" placeholder="Matriculation Number" style="margin-bottom: 3%;width:100%"/>
									  <center>  <?php echo "<p style='color:red' id='matriculation_number-error'></p>"; ?> </center>
							</div>
							<br/>
								
								
			        	</div>
						<br/>
								<br/>
						<div class="row">
				
					<div class="col-md-5">
							
						<?php $q = $this->db->get_where('university',array('permit'=>1));
							  $Pr = $q->result();?>
                            <select class="form-control university init-select" name="university" id="university" style="width: 100%"  onChange="changeuni(this.value);">
                                <option value="">Select University</option>
                                <?php foreach($Pr as $row){?>
                                <option value="<?php echo $row->u_id;?>"><?php echo $row->u_name;?></option>
                                <?php } ?>
                            </select>
							<p style="color:red;" id="university-error"></p>					
                    </div>
					
					<div class="col-md-5">
						<?php $q = $this->db->get_where('club_table',array('permit'=>1));
							  $r = $q->result();?>
                            <select class="js-example-basic-multiple cca init-select" multiple="multiple" name="cca[]" id="cca" class="form-control"  style="width: 100%;">
                                <option value="">Select CCA</option>
								<?php /*foreach($r as $row){?>
                                <option value="<?php echo $row->c_id;?>"><?php echo $row->c_name;?></option>
                                <?php }*/ ?>
                            </select>
                            <?php echo "<p style='color:red' id='cca-error'></p>"; ?>						
					</div>
					</div>
					<br/>
					<div class="row">						
						<div class="col-md-5">
							<?php $q = $this->db->get_where('hall_table',array('permit'=>1));
							  $Pr = $q->result();?>
							   <select class="hall init-select" name="hall" id="hall" class="form-control" style="width: 100%;">
							 <option value="">Select Hall clubs</option>
                               <?php /*foreach($Pr as $row){?>
                                <option value="<?php echo $row->h_id;?>"><?php echo $row->h_name;?></option>
                                <?php }*/ ?>
                            </select>
                            <?php echo "<p style='color:red' id='hall-error'></p>"; ?>
						</div>
						<div class="col-md-5">
							<?php $q2 = $this->db->get_where('faculty_table',array('permit'=>1));
							  $f = $q2->result();?>
                            <select class="form-control faculty init-select" name="faculty" id="faculty" style="width: 100%;">
                                <option value="">Select Faculty clubs</option>
                                 <?php /*foreach($f as $row){?>
                                <option value="<?php echo $row->f_id;?>"><?php echo $row->f_name;?></option>
                                <?php }*/ ?>
                            </select>
							<p style="color:red;" id="faculty-error"></p>
						</div>
					</div>
					 
				</div>
			</div>
	    
	   <div class="row">
		<div class="col-md-offset-7 col-md-2">
			<div style="text-align:center;margin-top:5px">
			 <?php $lnk = base_url().'students';?>
				 <input type="submit" value="Save and Continue" id="reg_submit" class="btn btn-success" style="padding:7px;border-radius:5px">
			</div>
		</div>
		<div class="col-md-3"></div>
	   </div>
	   </form>
		</div>
	
       
<!--Initiate Select2-->
<script>
    $(document).ready(function() {
    $('.init-select').select2();
});
</script>
<script>
function selectgroups() {
    var universityid = document.getElementById("university").value;
	
    //document.getElementById("demo").innerHTML = "You selected: " + x;
}
</script>

<script>
   
  function changeuni(value) {
        if (value.length == 0) document.getElementById("university").innerHTML = "<option></option>";
        else {
            var grpOptions  = "<option value='' disbaled>Select CCA</option>";
            var facOptions  = "<option value='' disbaled>Select Faculty clubs</option>";
            var hallOptions = "<option value='' disbaled>Select Hall clubs</option>";
              $.ajax({
            url: "<?php echo base_url('leader/getunidata'); ?>",
            type: 'POST',
            data: {uni_id:value},
            success: function(msg) {
                
                var json = JSON.parse(msg);
                              
                console.log(json.st);
                if (json.st == 'fail') {
                    alert('errors');
                   
                }
                if (json.st == 'success') {
                   
                   //console.log(json.grps);
                   // alert(json.grps);
                    var groups = json.grps;
                    
			        var i = '';
                    for(i=0; i<groups.length;i++)	{
                        clubid = groups[i]["c_id"];
                        clubname = groups[i]["c_name"];
                        //console.log(groups[i]["c_id"]);
                        //console.log(groups[i]["c_name"]);
                         //console.log();
                          
                            grpOptions += "<option value='"+clubid+"'>" + clubname+ "</option>";
                        }
                    document.getElementById("cca").innerHTML = grpOptions;
                    
                    var faculty = json.fac;
                    for(j=0; j<faculty.length;j++)	{
                        facultyid = faculty[j]["c_id"];
                        facultyname = faculty[j]["c_name"];
                        console.log(faculty[j]["c_id"]);
                        console.log(faculty[j]["c_name"]);
                         //console.log();
                          
                            facOptions += "<option value='"+facultyid+"'>"+ facultyname+ "</option>";
                        }
                    document.getElementById("faculty").innerHTML = facOptions;
                    
                    var hall = json.hall;
                    for(k=0; k<hall.length;k++)	{
                        hallid = hall[k]["c_id"];
                        hallname = hall[k]["c_name"];
                        //console.log(hall[k]["c_id"]);
                        //console.log(hall[k]["c_name"]);
                         //console.log();
                          
                            hallOptions += "<option value='"+hallid+"'>"+ hallname+ "</option>";
                        }
                    document.getElementById("hall").innerHTML = hallOptions;
                    }
            }
        });
        }
    }
</script> 

<!--AJAX user authentication-->
<script type="text/javascript">
    $(function() {
         $('#studreg_form').submit(function(e) {
        e.preventDefault(); // <------this will restrict the page refresh
        //alert('form started');
        var form_data = {
            matriculation_number: $('#mat_no').val(),
            university: $('#university').val(),
            faculty: $('#faculty').val(),
            hall_name: $('#hall').val(),
            cca_name: $('#cca').val(),
			fb_pic: $('#fbpic').val(),
			user_name:$('#fbdisplayname').val(),
			fisrtname:$('#fblname').val(),
			lastname:$('#fbfname').val(),
			user_mail:$('#fbmail').val(),
			fb_id:$('#fbid').val(),
			//*send fb values too*/
        };
        //alert('here'+form_data.phone_number);
        $.ajax({
            url: "<?php echo base_url('leader/stud_register'); ?>",
            type: 'POST',
            data: form_data,
            success: function(msg1) {
                var json = JSON.parse(msg1);
				//console.log(msg1);
               if (json.st == 'fail') {
                    //alert('errors');
                    $('#matriculation_number-error').html(json.matriculation_number);
                    $('#university-error').html(json.university);
                    $('#faculty-error').html(json.faculty);
                    $('#hall-error').html(json.hall_name);
                    $('#cca-error').html(json.ccaname);
                }
                if (json.st == 'success') {
                    //alert('Successfully registered!');
					swal({
						  text: 'Successfully registered!',
						  type: 'success',
					}).then(function() {
					window.location.href = "<?php echo site_url('leader/leader_home'); ?>";
                    });
				}
            }
        });
    });
});
      
	  </script>
	  <script>
	  $(document).ready(function() {
    $('.js-example-basic-single').select2();
});
        $(document).ready(function() {
    $('.js-example-basic-multiple').select2();
});
        </script>
    </body>
</html>