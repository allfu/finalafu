<!doctype html>
<html lang="en">
<?php
if (isset($this->session->userdata['logged_in'])) {

//header("location: http://localhost/login/index.php/user_authentication/user_login_process");
}
?>
    <head>
        <meta charset="utf-8">
        <title>AFU|Student Leader Registration</title>
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
        <link href="<?php echo base_url().'assets/frontend/';?>css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?php echo base_url().'assets/frontend/';?>css/stack-interface.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?php echo base_url().'assets/frontend/';?>css/socicon.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?php echo base_url().'assets/frontend/';?>css/lightbox.min.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?php echo base_url().'assets/frontend/';?>css/flickity.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?php echo base_url().'assets/frontend/';?>css/iconsmind.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?php echo base_url().'assets/frontend/';?>css/jquery.steps.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?php echo base_url().'assets/frontend/';?>css/theme.css" rel="stylesheet" type="text/css" media="all" />
		<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
        <link href="<?php echo base_url().'assets/frontend/';?>css/custom.css" rel="stylesheet" type="text/css" media="all" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i%7CMaterial+Icons" rel="stylesheet">
		 	<script src="<?php echo base_url().'assets/frontend/';?>js/jquery-3.1.1.min.js"></script>
			<script type="text/js" src="<?php echo base_url().'assets/js/';?>bootstrap.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
    </head>
     <body class=" ">
	 <?php
if (isset($logout_message)) {
echo "<div class='message'>";
echo $logout_message;
echo "</div>";
}
?>
<?php
echo "<div class='error_msg'>";
if (isset($message_display)) {
echo $message_display;
}
echo "</div>";
?>

        <a id="start"></a>
        <div class="nav-container ">
            <div class="bar bar--sm visible-xs">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-3 col-sm-2">
                            <a href="index.html">
                                <img class="logo logo-dark" alt="logo" src="<?php echo base_url().'assets/frontend/';?>img/logo-dark.png" />
                                <img class="logo logo-light" alt="logo" src="<?php echo base_url().'assets/frontend/';?>img/logo-light.png" />
                            </a>
                        </div>
                        <div class="col-xs-9 col-sm-10 text-right">
                            <a href="#" class="hamburger-toggle" data-toggle-class="#menu1;hidden-xs">
                                <i class="icon icon--sm stack-interface stack-menu"></i>
                            </a>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </div>
         </div>
            <!--end bar-->
            <nav id="menu1" class="bar bar--sm bar-1 hidden-xs bar--transparent bar--absolute">
                <div class="container">
                    <div class="row">
                        <div class="col-md-1 col-sm-2 hidden-xs">
                            <div class="bar__module">
                                <a href="index.html">
                                    <img class="logo logo-dark" alt="AFU" src="<?php echo base_url().'assets/frontend/';?>img/logo-dark.png" />
                                    <!--<img class="logo logo-light" alt="logo" src="img/logo-light.png" />-->
                                </a>
                            </div>
                        </div>
                      </div>
					</div>
					</nav>        
      
        <div class="main-container">
            <section class="height-100 imagebg text-center" data-overlay="4">
                <div class="background-image-holder">
                    <img alt="background" src="<?php echo base_url().'assets/frontend/';?>img/inner-6.jpg" />
                </div>
				<?php
                 echo "<div class='error_msg'>";
                 echo validation_errors();
                 echo "</div><br>";
                 ?>
                <div class="container pos-vertical-center">
                    <div class="row">
                        <div class="col-sm-7 col-md-5">
                             <h2> Registration Form </h2>
                          <?php form_open('user_authentication/new_user_registration',array('class' => '', 'id' => 'reg_form')); ?>
                                <div class="row">
                                    <div class="col-sm-12">
                                     <?php   echo"<br/>";
                                       echo form_input('user_name','',$data = array('id'=>'user_name',
									                                                  'placeholder'=>'User Name',));/*name,value,extra*/
                                       echo "<p style='color:red' id='username-error'></p>"; ?>
                                       </div>
                                    <div class="col-sm-12">
                                     <?php   echo"<br/>";
                                             echo form_input('first_name','',$data = array('id'=>'first_name','placeholder'=>'First Name',));
                                             echo "<p style='color:red' id='fname-error'></p>"; ?>
                                    </div>
									 <div class="col-sm-12">
                                       <?php   echo"<br/>";
                                               echo form_input('last_name','',$data = array('id'=>'last_name','placeholder'=>'Last Name',));
                                               echo "<p style='color:red' id='lname-error'></p>"; ?>
                                    </div>
									 <div class="col-sm-12">
                                      <?php   echo"<br/>";
                                              echo form_input('contact_no','',$data = array('id'=>'contact','placeholder'=>'Contact No',));
                                              echo "<p style='color:red' id='contact-error'></p>"; ?>
                                    </div>
									 <div class="col-sm-12">
                                       <?php  echo"<br/>";
                                              $data = array(
                                              'type' => 'email',
                                              'name' => 'email',
                                               'id'   => 'email',
											   'placeholder'=>'Email',
                                                  );
                                              echo form_input($data);
                                              echo "<p style='color:red' id='email-error'></p>"; ?>
                                    </div>
									 <div class="col-sm-12">
                                      <?php   echo"<br/>";
                                              echo form_input('website','',$data = array('id'=>'website', 'placeholder'=>'Website',));
                                              echo "<p style='color:red' id='website-error'></p>";?>
                                    </div>
									 <div class="col-sm-12">
                                       <?php   echo"<br/>";
                                               echo form_password('password','',$data = array('id'=>'pass', 'placeholder'=>'Password',));
                                               echo "<p style='color:red' id='pass-error'></p>"; ?>
                                    </div>
									 <div class="col-sm-12">
                                      <?php   
                                              echo"<br/>";
                                              echo form_password('confirm_password','',$data = array('id'=>'re_pass','placeholder'=>'Repeate Password',));
                                              echo "<p style='color:red' id='re_pass-error'></p>";?>
                                    </div>
									 <div class="col-sm-12">
									 <?php echo "<br/>"; ?>
                                      <select class="js-example-basic-single" name="school" id="school_name">
                                      <option value="none" selected="" disabled=""></option>
                                      <option value="National_Universtity_of_Singapore">National Universtity of Singapore</option>
                                       <option value="Singapore_Institute_Of_Technology">Singapore Institute Of Technology</option>
                                       <option value="Singapore_Institute_Of_Technology & Design">Singapore Institute Of Technology & Design</option>
                                       <option value="Singapore_Management_University">Singapore Management University</option>
                                       <option value="Singapore_University_Of_Social_Science</">Singapore University Of Social Science</option>
                                       </select>
											<?php echo "<p style='color:red' id='school-error'></p>";?>
                                    </div>
									 <div class="col-sm-12">
                                       <?php  
                                              echo"<br/>";
                                              echo form_input('yr','',$data = array('id'=>'yr','placeholder'=>'year Of Study',));
                                              echo "<p style='color:red' id='yr-error'></p>"; ?>
                                    </div>
                                    <div class="col-sm-12">
                                     <?php	
									 echo "<br/>";
									 echo form_submit('submit', 'Sign Up',$data=array('id'=>'reg_submit')); ?>
                                    </div>
                                </div>
                                <!--end of row-->
                          <?php echo form_close(); ?>
						  <span class="type--fine-print block">
                                <a href="<?php echo base_url() ?> ">For Login Click Here</a>
                            </span>
                            <hr>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>
        </div>
		<script type="text/javascript">
            $(function() { // <----------------missed the doc ready function
                $('#reg_form').submit(function(e) {
                    e.preventDefault(); // <------this will restrict the page refresh
                    //alert('form started');
                    var form_data = {
                        user_name: $('#user_name').val(),
                        first_name: $('#first_name').val(),
                        last_name: $('#last_name').val(),
                        contact: $('#contact').val(),
                        email: $('#email').val(),
                        website: $('#website').val(),
                        pass: $('#pass').val(),
                        re_pass: $('#re_pass').val(),
                        school: $('#school_name').val(),
                        yr: $('#yr').val(),
                    };
                    //alert('here'+form_data.user_name);
                    $.ajax({
                        url: "<?php echo site_url('user_authentication/new_user_registration'); ?>",
                        type: 'POST',
                        data: form_data,
                        success: function(msg) {
                            var json = JSON.parse(msg);
                            //alert(json.error);
                            //alert(json.username);
                            console.log(json);
                            if (json.error == 'fail') {
                                $('#username-error').html(json.user_name);
                                $('#fname-error').html(json.first_name);
                                $('#lname-error').html(json.last_name);
                                $('#contact-error').html(json.contact);
                                $('#email-error').html(json.email);
                                $('#website-error').html(json.website);
                                $('#pass-error').html(json.pass);
                                $('#re_pass-error').html(json.re_pass);
                                $('#school-error').html(json.school);
                                $('#yr-error').html(json.yr);
                            }
                            if (json.error == 'success') {
                                if (json.msg == true) {
                                    alert('Registration successful');
                                    window.location.href = "<?php echo site_url('user_authentication/user_login'); ?>";
                                } else {
                                    alert('Username already exists!');
                                    window.location.href = "<?php echo site_url('user_authentication/user_registration'); ?>"
                                }

                            }
                        }

                    });

                });
                $(document).ready(function() {
                $('.js-example-basic-single').select2();
            });
        </script>
        <!--<div class="loader"></div>-->
        <a class="back-to-top inner-link" href="#start" data-scroll-class="100vh:active">
            <i class="stack-interface stack-up-open-big"></i>
        </a>

        <script src="<?php echo base_url().'assets/frontend/';?>js/flickity.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/easypiechart.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/parallax.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/typed.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/datepicker.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/isotope.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/ytplayer.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/lightbox.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/granim.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/jquery.steps.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/countdown.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/twitterfetcher.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/spectragram.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/smooth-scroll.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/scripts.js"></script>
	
		
    </body>
</html>