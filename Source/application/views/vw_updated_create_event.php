
<!--TimePicker Css-->
<link rel="stylesheet" href="<?php echo base_url().'assets/updatedFrontend/css/styletimepicki.css'; ?>" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url().'assets/updatedFrontend/css/timepicki.css'; ?>" type="text/css" />
<!--DatePicker Js-->
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
  
<script>
	  $( function() {
		$( ".datepicker" ).datepicker();
	  } );
</script>
  
<style>
	 #dropzone {
			  position: relative;
			  color: #cecece;
			  font: bold 24px/220px arial;
			  height: 220px;
			  text-align: center;
			  width: 100%;
	}

	#dropzone.hover {
	  border: 10px solid #FE5;
	  color: #FE5;
	}

	#dropzone div {
	  position: absolute;
	  top: 0;
	  right: 0;
	  bottom: 0;
	  left: 0;
	}

	#dropzone img {
	  border-radius: 10px;
	  vertical-align: middle;
	  max-width: 95%;
	  max-height: 95%;
	}

	#dropzone [type="file"] {
	  cursor: pointer;
	  position: absolute;
	  opacity: 0;
	  top: 0;
	  right: 0;
	  bottom: 0;
	  left: 0;
	}
	 /*slider dropzone*/
	#slider-dropzone {
	  position: relative;
	  border: 5px dashed #cecece;
	  border-radius: 20px;
	  color: #cecece;
	  /*font: bold 15px/40px arial;*/
	  height: 100px;
	  margin: 30px auto;
	  text-align: center;
	  width: 100px;
	}

	#slider-dropzone.hover {
	  border: 10px solid #FE5;
	  color: #FE5;
	}

	/*#slider-dropzone.dropped {
	  background: #f5f5f5;
	  border: 10px solid #444;
	}*/

	#slider-dropzone div {
	  position: absolute;
	  top: 0;
	  right: 0;
	  bottom: 0;
	  left: 0;
	  font-size: 15px;
	}

	#slider-dropzone img {
	  border-radius: 5px;
	  vertical-align: middle;
	  max-width: 95%;
	  max-height: 95%;
	}

	#slider-dropzone [type="file"] {
	  cursor: pointer;
	  position: absolute;
	  opacity: 0;
	  top: 0;
	  right: 0;
	  bottom: 0;
	  left: 0;
	}
	   /* input[type="file"] {
		display: none;
	}*/
		#add_more {
			display: none;
		}
	.custom-file-upload {
		border: 1px solid #ccc;
		display: inline-block;
		padding: 6px 12px;
		cursor: pointer;
	}
	   /* input[type="file"] {
		display: none;
	}*/
		#file-input {
			display: none;
		}
	.custom-file-upload {
		border: 1px solid #ccc;
		display: inline-block;
		padding: 6px 12px;
		cursor: pointer;
	}
	/*Form load effect*/
	svg {
		width: 100px;
		height: 100px;
		margin: 10px;
		display:inline-block;
	}
	.cls-1,.cls-2 {
		fill:none;
		stroke-linecap:bevel;
		stroke-linejoin:round;
	}
	.cls-1 {
		stroke-width:2px;
		stroke: #dadada;
	}
	.cls-2 {
		fill:none;
		stroke:#fff;
		stroke-width:4px;
	}
	#form_loading{
		position: absolute;
		top: 50%;
		left: 50%;
		margin-top: -50px;
		margin-left: -50px;
		width: 100px;
		height: 100px;
		z-index: 9999;
	}
	.full-width{
		position: absolute;
		height: 100%;
		width:100%;
		background-color: #fbfbfb;
	} 
</style>


<div class="full-width" id="loader">
<div id="form_loading"">
    <svg viewBox="-25 -25 100 100" preserveAspectRatio>
        <defs>
            <linearGradient id="gr-simple" x1="0" y1="0" x2="100%" y2="100%">
            <stop stop-color="rgba(255,255,255,.2)" offset="10%"/>
            <stop stop-color="rgba(255,255,255,.7)" offset="90%"/>
            </linearGradient>
        </defs>	
        <circle class="cls-1" cx="26" cy="27" r="26" stroke="url(#gr-simple)"/>
        <path class="cls-2" d="M25,0A24.92,24.92,0,0,1,42.68,7.32" transform="translate(1 2)">
            <animateTransform 
                     attributeName="transform" 
             type="rotate"
             dur="1s" 
             from="0 26 27"
             to="360 26 27" 
             repeatCount="indefinite"/>
            </path>
    </svg>
</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div>
				<ul class="nav nav-tabs editabletabbar">
					<li role="presentation"><a href="<?php echo base_url().'leader/leader_home';?>" class="tabmenu" data-target="manage-event">My Events</a></li>
					<li role="presentation" class="<?php if($this->uri->segment(2) == 'my_groups'){echo 'active';} ?>"><a href="<?php echo base_url().'leader/my_groups';?>" class="tabmenu" data-target="manage-group">My Groups</a></li>				
				</ul>
			</div>
			<div class="content-wrapsec">
				<div class="row">
					<div class="col-md-12">
						<h3>Create New Event</h3>
					</div>
				</div>
				<div class="borderbottomsec"></div>
				<div class="row">
					<div class="col-md-12">
						<form method="post" id="event_insert" action="" enctype="multipart/form-data">														
							<div class="row">
								<div class="col-md-4 col-sm-4">
									<div id="image_preview">
										<!--<img id="previewing" src="<?php echo base_url().'assets/frontend/img/xyz.jpg';?>" />-->
										<div id="dropzone">
											<div>Choose Logo</div>
												<input type="file" name="group_logo" accept="image/png, image/jpeg, image/jpg" />
											</div>
									</div>
									<div class="form-group">
										<hr />
											<div class="row">
												<div class="col-md-6 col-md-offset-3 text-center">
													<b class="text-center">Choose Slider Images</b>
												</div>
											</div>
											<div class="row" id="slider-row">
											</div>
											<div class="row">
												<div class="col-md-6 col-md-offset-2 text-right"> 
													<label for="add_more" class="custom-file-upload" style="margin-left: 20%;">
														<i class="glyphicon glyphicon-cloud-upload"></i> Add More
													</label>
													<input type="button" id="add_more" class="upload" value="Add More Files"/>
												</div>
											</div>
									</div>
								</div>
								<div class="col-md-7 col-sm-8">
									<div class="form-group">
										<input type="text" value="" class="form-control" name="e_name" id="e_name" placeholder="Enter event name"/>
									</div>
									<div class="form-group">
										<input type="text-area" value="" class="form-control" name="e_type" id="e_type" placeholder="Enter event type"/>
									</div>
									<div class="form-group">										
										<select class="js-example-basic-single form-control" name="e_by" id="e_by" placeholder="Select CCA">
											<option value="none" selected="" disabled="" >Select group</option>
											<?php $i=1;  
											foreach($grps as $glist){
											$q = $this->db->get_where('club_table',array('c_id'=>$glist->gclubid,/*'permit'=>1*/));
											$a = $q->result();
											foreach($a as $row){											
											?>										   
										   <option value="<?php echo $row->c_id;?>"><?php echo $row->c_name;?> </option>
											<?php } }?>
										</select>
									</div>
									<div class="form-group">
										<input type="text-area" value="" class="form-control"  name="e_loc" id="e_loc" placeholder="Enter event location"/>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-md-6">
												<input type="text" class="form-control datepicker" value="" name="e_start"  placeholder="Start date"/>
											</div>
											<div class="visible-xs">
												<br/>
											</div>
											<div class="col-md-6">
												<div class="inner cover indexpicker">
													<input id="e_start_time" class="timepicker1 form-control" placeholder="Start time" type="text" name="e_start_time" />
												</div>
												<!--<input type="text" value="" class="form-control" name="e_start_time"  placeholder="Start time"/>-->
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-md-6">
												<input type="text" class="form-control datepicker" value="" name="e_end" id="e_end" placeholder="End date"/>
											</div>
											<div class="visible-xs">
												<br/>
											</div>
											<div class="col-md-6">
												<div class="inner cover indexpicker">
													<input id="e_end_time" class="timepicker1 form-control"  type="text" placeholder="End Time"  name="e_end_time" />
												</div>
												<!--<input type="text" class="form-control" value="" name="e_end_time" id="e_end_time" placeholder="End time"/>-->
											</div>
										</div>
									</div>
									<div class="form-group">
										<textarea class="form-control" value="" name="e_desc" cols="30" rows="4" placeholder="Enter event description"  aria-required="true"></textarea>										
									</div>
									<br/>
									<div class="form-group">
										<div class="row">
											<div class="col-md-4 col-sm-4">
												<a class="btn btn-success btn-join btn-save"  id="btn-join" href="javascript:void(0)" style="width:100%">Save Event</a>
											</div>
											<div class="visible-xs">
												<br/>
											</div>
											<div class="col-md-4 col-sm-4">
												<a class="btn btn-warning btn-join" id="btn-join" href="javascript:void(0)" style="width:100%">Publish</a>
											</div>
											<div class="visible-xs">
												<br/>
											</div>
											<div class="col-md-4 col-sm-4">
												<a class="btn btn-danger btn-join" id="btn-join" href="javascript:void(0)" style="width:100%">Delete Events</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="borderbottomsec"></div>
							<div class="row">							
								<div class="col-md-12">
									<div style="border: 1px solid gray;" class="table-responsive">
										<table id="member_table" class="table table-bordered table-striped table-hover">
											<thead>
												<tr>
													<th>Student Name</th>
													<th>Matriculation No.</th>
													<th>Faculty</th>
													<th>Hall</th>
													<th>Roles</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												<td>Sam</td>
												<td>121234</td>
												<td>Arts Faculty</td>
												<td>Hall No 2</td>
												<td>Admin</td>
												<td>
													<input type="button" value="Assign Admin" class="btn btn-success" />
													<input type="button" value="Remove" class="btn btn-danger" />
												</td>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>				
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="<?php echo base_url().'assets/updatedFrontend/js/timepicki.js'; ?>"></script>
 <script>
	 $('.timepicker1').timepicki();
 </script>

<script>
    $(document).ready(function(){
       $('#loader').hide(); 
    });
</script>
<script>
    var html='';

for (var i = 0; i < 3; i++) {
   $('#slider-row').append($('<div class="col-md-4">').append($('<div id="filediv">').append($('<div id="slider-dropzone">').append($('<div class="choose_text">Choose Image</div>')).fadeIn('slow').append($("<input/>",{
                name:'images[]',
                type: 'file',
                id: 'file',
                
            })))));
}
$("#slider-row").append(html);
</script>
<script>
   var abc = 0;      // Declaring and defining global increment variable.
    $(document).ready(function() {
        //  To add new input file field dynamically, on click of "Add More Files" button below function will be executed.
        $('#add_more').click(function() {
            /*$('#slider-row').append($('<div/>',{
               class:'col-md-2'
            }).fadeIn('slow').append($("<input/>", {
            name: 'images[]',
            type: 'file',
            id: 'file'
            }), $("<br/><br/>")));*/
            $('#slider-row').append($('<div class="col-md-4">').append($('<div id="filediv">').append($('<div id="slider-dropzone">').append($('<div class="choose_text">Choose Image</div>')).fadeIn('slow').append($("<input/>",{
                name:'images[]',
                type: 'file',
                id: 'file',
                
            })))));
            });
// Following function will executes on change event of file input to select different file.
$('body').on('change', '#file', function() {
if (this.files && this.files[0]) {
abc += 1; // Incrementing global variable by 1.
var z = abc - 1;
var x = $(this).parent().find('#previewimg' + z).remove();

$(this).before("<div id='abcd" + abc + "' class='abcd'><img id='previewimg" + abc + "' src='' style='height:100%'/></div>");
var reader = new FileReader();
reader.onload = imageIsLoaded;
reader.readAsDataURL(this.files[0]);
$(this).hide();
$("#abcd" + abc).append($("<img/>", {
id: 'img',
src: "<?php echo base_url().'assets/img/x.png'; ?>",
alt: 'delete'
}).click(function() {
$(this).parent().parent().parent().parent().remove();
}));
}
});
// To Preview Image
function imageIsLoaded(e) {
$('#previewimg' + abc).attr('src', e.target.result);
};
$('#upload').click(function(e) {
var name = $(":file").val();
if (!name) {
alert("First Image Must Be Selected");
e.preventDefault();
}
});
});
</script>
 <script>
		  $(document).ready(function()
		  {
			   var minDate = new Date();
			  $("#e_start").datepicker({
				   dateFormat:"yy-mm-dd",
				   showAnim:'drop',
				   minDate :minDate,
				   onClose:function(selectedDate)
				   {
					   $('#e_end').datepicker("option","minDate",selectedDate);
				   }
				   
				   });
		  });
		  
		  $(function()
		  { 
		  var minDate1 = new Date();
			  $("#e_end").datepicker(
			  { 
				   dateFormat:"yy-mm-dd",
				   showAnim:'drop',
				   minDate :minDate1,
				   onClose:function(selectedDate1)
				   {
					   $('#e_start').datepicker("option","minDate1",selectedDate1);
				   }
			  });
		  });
		  </script>
		  <script>
$(document).ready(function(e)  {
        $(".btn-save").click(function(e)  {
		e.preventDefault();
		$('#loader').show();
		var form = $('#event_insert');
		var formData = new FormData(form[0]);
		$.ajax({
                type: "POST",
                url: "<?php echo base_url().'leader/eventsubmit'; ?>",
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function(res)  {
					$('#loader').hide();
					var parsed = JSON.parse(res);
                    //var parsed = data;
                    console.log(parsed);
                    if(parsed.status_code == 1) {
                        //alert('data inserted successfully');
                       //window.location.href = "<?php echo site_url('leader/leader_home'); ?>";
                        swal({
                                html: parsed.status,
                                type: 'success',
                        }).then(function() {
                            window.location.href = "<?php echo site_url('leader/leader_home'); ?>";
                        });
                    }
					
                    else if(parsed.status_code == 0)    {
                        //alert(parsed.status);
                        swal({
                                html: parsed.status,
                                type: 'error',
                            });
                        }
                    else    {
                            swal({
                                html: parsed.status,
                                type: 'error',
                            });
                            //alert(parsed.status);
                        //$('#form-error').html(parsed.status);
					
					}
					}
		});
		});
	});
</script>
<script>
// Function to preview image after validation
$(function() {
$("#file").change(function() {
$("#message").empty(); // To remove the previous error message
var file = this.files[0];
var imagefile = file.type;
var match= ["image/jpeg","image/png","image/jpg"];
if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
{
$('#previewing').attr('src','noimage.png');
$("#message").html("<p id='error'>Please Select A valid Image File</p>"+"<h4>Note</h4>"+"<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
return false;
}
else
{
var reader = new FileReader();
reader.onload = imageIsLoaded;
reader.readAsDataURL(this.files[0]);
}
});
});
function imageIsLoaded(e) {
$("#file").css("color","green");
$('#image_preview').css("display", "block");
$('#previewing').attr('src', e.target.result);
$('#previewing').attr('width', '250px');
$('#previewing').attr('height', '230px');
};
</script>
<script>
// Function to preview image after validation
$(function() {
$("#file1").change(function() {
$("#message").empty(); // To remove the previous error message
var file = this.files[0];
var imagefile = file.type;
var match= ["image/jpeg","image/png","image/jpg"];
if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
{
$('#preview1').attr('src','noimage.png');
$("#message").html("<p id='error'>Please Select A valid Image File</p>"+"<h4>Note</h4>"+"<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
return false;
}
else
{
var reader = new FileReader();
reader.onload = image1IsLoaded;
reader.readAsDataURL(this.files[0]);
}
});
});
function image1IsLoaded(e) {
$("#file1").css("color","green");
$('#preview_image1').css("display", "block");
$('#preview1').attr('src', e.target.result);
$('#preview1').attr('width', '90px');
$('#preview1').attr('height', '90px');
};
</script>
<!--choose logo dropzone-->
<script>
    $(function() {
  
  $('#dropzone').on('dragover', function() {
    $(this).addClass('hover');
  });
  
  $('#dropzone').on('dragleave', function() {
    $(this).removeClass('hover');
  });
  
  $('#dropzone input').on('change', function(e) {
    var file = this.files[0];

    $('#dropzone').removeClass('hover');
    
    if (this.accept && $.inArray(file.type, this.accept.split(/, ?/)) == -1) {
      return alert('File type not allowed.');
    }
    
    $('#dropzone').addClass('dropped');
    $('#dropzone img').remove();
    
    if ((/^image\/(gif|png|jpeg)$/i).test(file.type)) {
      var reader = new FileReader(file);

      reader.readAsDataURL(file);
      
      reader.onload = function(e) {
        var data = e.target.result,
            $img = $('<img />').attr('src', data).fadeIn();
        
        $('#dropzone div').html($img);
      };
    } else {
      var ext = file.name.split('.').pop();
      
      $('#dropzone div').html(ext);
    }
  });
});
</script>

