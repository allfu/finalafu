<style>
	.tabmenu {
		padding: 10px 40px 10px 10px !important;
		font-size: 16px;
	}
	.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
		color: #555555;
		background-color: #f2f2f2 !important;
		border: 1px solid #dddddd;
		border-bottom-color: rgb(221, 221, 221);
		border-bottom-color: transparent;
		cursor: default;
}
    #dropzone {
  position: relative;
  border: 5px dashed #cecece;
  border-radius: 20px;
  color: #cecece;
  font: bold 24px/200px arial;
  height: 200px;
  margin: 30px auto;
  text-align: center;
  width: 200px;
}

#dropzone.hover {
  border: 10px solid #FE5;
  color: #FE5;
}

#dropzone.dropped {
  background: #f5f5f5;
  border: 10px solid #444;
}

#dropzone div {
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
}

#dropzone img {
  border-radius: 10px;
  vertical-align: middle;
  max-width: 95%;
  max-height: 95%;
}

#dropzone [type="file"] {
  cursor: pointer;
  position: absolute;
  opacity: 0;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
}
   /* input[type="file"] {
    display: none;
}*/
    #file-input {
        display: none;
    }
.custom-file-upload {
    border: 1px solid #ccc;
    display: inline-block;
    padding: 6px 12px;
    cursor: pointer;
}
/*Slider image view*/
/*slider dropzone*/
#slider-dropzone {
  position: relative;
  border: 5px dashed #cecece;
  border-radius: 20px;
  color: #cecece;
  /*font: bold 15px/40px arial;*/
  height: 100px;
  margin: 30px auto;
  text-align: center;
  width: 100px;
}

#slider-dropzone.hover {
  border: 10px solid #FE5;
  color: #FE5;
}

/*#slider-dropzone.dropped {
  background: #f5f5f5;
  border: 10px solid #444;
}*/

#slider-dropzone div {
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  font-size: 15px;
}

#slider-dropzone img {
  border-radius: 5px;
  vertical-align: middle;
  max-width: 95%;
  max-height: 95%;
}

#slider-dropzone [type="file"] {
  cursor: pointer;
  position: absolute;
  opacity: 0;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
}
   /* input[type="file"] {
    display: none;
}*/
    #add_more {
        display: none;
    }
.custom-file-upload {
    border: 1px solid #ccc;
    display: inline-block;
    padding: 6px 12px;
    cursor: pointer;
}
   /* input[type="file"] {
    display: none;
}*/
    #file-input {
        display: none;
    }
.custom-file-upload {
    border: 1px solid #ccc;
    display: inline-block;
    padding: 6px 12px;
    cursor: pointer;
}
/*Form load effect*/
svg {
	width: 100px;
    height: 100px;
	margin: 10px;
    display:inline-block;
}
.cls-1,.cls-2 {
	fill:none;
	stroke-linecap:bevel;
	stroke-linejoin:round;
}
.cls-1 {
	stroke-width:2px;
	stroke: #dadada;
}
.cls-2 {
	fill:none;
	stroke:#fff;
	stroke-width:4px;
}
#form_loading{
    position: absolute;
    top: 50%;
    left: 50%;
    margin-top: -50px;
    margin-left: -50px;
    width: 100px;
    height: 100px;
	z-index: 9999;
}
.full-width{
	position: absolute;
	height: 100%;
	width:100%;
	background-color: #fbfbfb;
}

</style>
<div class="full-width" id="loader">
<div id="form_loading"">
    <svg viewBox="-25 -25 100 100" preserveAspectRatio>
        <defs>
            <linearGradient id="gr-simple" x1="0" y1="0" x2="100%" y2="100%">
            <stop stop-color="rgba(255,255,255,.2)" offset="10%"/>
            <stop stop-color="rgba(255,255,255,.7)" offset="90%"/>
            </linearGradient>
        </defs>	
        <circle class="cls-1" cx="26" cy="27" r="26" stroke="url(#gr-simple)"/>
        <path class="cls-2" d="M25,0A24.92,24.92,0,0,1,42.68,7.32" transform="translate(1 2)">
            <animateTransform 
                     attributeName="transform" 
             type="rotate"
             dur="1s" 
             from="0 26 27"
             to="360 26 27" 
             repeatCount="indefinite"/>
            </path>
    </svg>
</div>
</div>
<div class="container">
	<div class="row">	
		<div class="col-md-12">
			<div>
				<ul class="nav nav-tabs">
					<li role="presentation"><a href="<?php echo base_url().'leader/leader_home';?>" class="tabmenu" data-target="manage-event">My Events</a></li>
					<li role="presentation" class="active"><a href="<?php echo base_url().'leader/my_groups';?>" class="tabmenu" data-target="manage-group">My Groups</a></li>
				</ul>
			</div>
			<div class="content-wrapsec">
				<div class="row">
					<div class="col-md-10 col-sm-10">
						<h3>Create New Group</h3>
					</div>
					<div class="col-md-2 col-sm-2">
						<a class="btn btn--primary" href="<?php echo base_url().'leader/join_group';?>">
							<span class="btn btn-info">Join new group</span>
						</a>
					</div>					
				</div>
				<div class="borderbottomsec"></div>	
				 <form method="post" action="" id="imageform" name="groupcreate_form dropzone">
					<div class="row">
						<div class="col-md-4 col-sm-4">
							<div class="form-group">
								<!--<label>Group Logo:</label>-->
                                <div id="dropzone">
                                    <div>Choose Logo</div>
                                    <input type="file" name="group_logo" accept="image/png, image/jpeg, image/jpg" />
                                </div>
                                <!--<div>
                                   <input type="file" class="images" id="file-input" name="images[]" multiple/>
                                </div>-->
                                <!-- <label for="file-input" class="custom-file-upload" style="margin-left: 20%;">
                                    <i class="glyphicon glyphicon-cloud-upload"></i> Choose Slider images
                                </label>
                                <input id="file-input" type="file" name="images[]" multiple/>
								<div class="gallery"></div>
								<p id="group_logo_error" style="color:red;"></p> -->
								<hr />
                                <div class="row">
                                    <div class="col-md-6 col-md-offset-3 text-center">
                                        <b class="text-center">Choose Slider Images</b>
                                    </div>
                                </div>
                                <div class="row" id="slider-row">
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-md-offset-2 text-right">
                                        <label for="add_more" class="custom-file-upload" style="margin-left: 20%;">
                                            <i class="glyphicon glyphicon-cloud-upload"></i> Add More
                                        </label>
                                        <input type="button" id="add_more" class="upload" value="Add More Files"/>
                                    </div>
                                </div>
							</div>											
						</div>
						<div class="visible-xs"><br/></div>
						<div class="col-md-8 col-sm-8">
							<div class="form-group">
								<label>School Name:</label>
								<select required class="form-control" name="c_uni" id="c_uni">
									<option value="none" selected="" disabled=""></option>
									<?php $this->db->where('permit !=',0);
										 $query = $this->db->get('university');
										 foreach($query->result() as $row){
									?>
									<option value="<?php echo $row->u_id;?>"><?php echo $row->u_name;?></option>
									<?php } ?>
								</select>
								<p id="school_name_error" style="color:red;"></p>
							</div>
							<div class="form-group">
								<label>Group Name:</label>
								<input type="text" class="form-control" name="c_name" value="<?php echo set_value('c_name');?>" id="c_name" />
								<p id="group_name_error" style="color:red;"></p>
							</div>	
							<div class="form-group">
								<label>Group Description:</label>
								<!--<input type="text-area" class="form-control" name="c_desc" value="<?php echo set_value('c_desc');?>" id="c_desc" wrap="soft"/>-->
								<textarea id="c_desc" name="c_desc" class="form-control"></textarea>
								<p id="group_desc_error" style="color:red;"></p>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-4 col-sm-4">
										<input type="button" name="sub" class="btn btn-primary btn-save" id="submit" value="Save Group">						
									</div>								
								</div>
							</div>
						</div>
					</div>
				</form>
				<div id="imgprv" style="display:none">
					<table>
						<tr>
							<?php 
								$this->load->model('mdl_leader');
								$id=$this->session->userdata('lastid');
								$query = $this->db->where('club_id',$id)->get('slider_images');  
								 foreach($query->result() as $row)
								 {?>
									 <td><a href="<?=$row->id;?>"><img src="<?=base_url().$row->images;?>"/></a></td>
									 <?php							 
								 }
							?>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
    $(document).ready(function(){
       $('#loader').hide(); 
    });
</script>
<script>
    var html='';

for (var i = 0; i < 3; i++) {
   $('#slider-row').append($('<div class="col-md-4">').append($('<div id="filediv">').append($('<div id="slider-dropzone">').append($('<div class="choose_text">Choose Image</div>')).fadeIn('slow').append($("<input/>",{
                name:'images[]',
                type: 'file',
                id: 'file',
                
            })))));
}
$("#slider-row").append(html);
</script>
<!--ADD GROUP SCRIPT-->
 <script>
    $(document).ready(function()  {
        $("#submit").click(function(e)  {
            e.preventDefault();
			$('#loader').show();
           // alert('ajax call');
          var form = $('#imageform')[0]; // You need to use standart javascript object here
          var formData = new FormData(form);
            //console.log(formData);
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'leader/image_submit'; ?>",
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function (result) {
						$('#loader').hide();
                        var parsed = JSON.parse(result);
                        //console.log(parsed);
                        if(parsed.status_code == 1) {
                             swal({
                                html: parsed.status,
                                type: 'success',
                                    }).then(function() {
                                         window.location = "<?php echo base_url().'leader/my_groups'; ?>";
                                    });
                        } else  {
                            swal({
                                html: parsed.status,
                                type: 'error'
                            });
                        }
					//$("#imgprv").show();
                    }
                });
        });
    });
     </script>
    <!-- <script>
	$('#sub').click(function(e) {
        e.preventDefault(); // <------this will restrict the page refresh
        /*var form_data = {
            grpuni: $('#group_uni').val(),
            grpname: $('#group_name').val(),            
            grpdesc: $('#group_desc').val(),            
        };*/
		var form = $('#groupcreate_form');
            var formData = new FormData(form[0]);
            console.log(formData);
		
        //alert(form_data.e_name);
        $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + 'leader/group_submit',
                data: formData,
				processData: false,
				contentType: false,
                success: function(res)  {
                    var json = JSON.parse(res);
                    console.log(json);
                     if(json.st== 'fail'){
                        $('#school_name_error').html(json.c_uni);
                        $('#group_name_error').html(json.c_name);
                        $('#group_desc_error').html(json.c_desc);
                                                     
                    }
					 if(json.st_code== 3){
						 swal(
						  '',
						  'File type not allowed!',
						  'error'
						)
					 }
                   
                   if(json.st =='success'){
                        
                        //alert('Group added successfully');
						swal(
						  '',
						  'Group added successfully!',
						  'success'
						)
                        window.location.href = "<?php echo site_url('leader/my_groups'); ?>";
                    }
                   
                }
        });
    });
</script>-->
<script>
     $(function() {
    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    $($.parseHTML('<img class="gallery-image">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                    $('.gallery-image').attr({ width: '110px', height: '100px' });
                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    };

    $('#file-input').on('change', function() {
        imagesPreview(this, 'div.gallery');
    });
});
</script>
<script>
    $(function() {
  
  $('#dropzone').on('dragover', function() {
    $(this).addClass('hover');
  });
  
  $('#dropzone').on('dragleave', function() {
    $(this).removeClass('hover');
  });
  
  $('#dropzone input').on('change', function(e) {
    var file = this.files[0];

    $('#dropzone').removeClass('hover');
    
    if (this.accept && $.inArray(file.type, this.accept.split(/, ?/)) == -1) {
      return alert('File type not allowed.');
    }
    
    $('#dropzone').addClass('dropped');
    $('#dropzone img').remove();
    
    if ((/^image\/(gif|png|jpeg)$/i).test(file.type)) {
      var reader = new FileReader(file);

      reader.readAsDataURL(file);
      
      reader.onload = function(e) {
        var data = e.target.result,
            $img = $('<img />').attr('src', data).fadeIn();
        
        $('#dropzone div').html($img);
      };
    } else {
      var ext = file.name.split('.').pop();
      
      $('#dropzone div').html(ext);
    }
  });
});
</script>
<script>
   var abc = 0;      // Declaring and defining global increment variable.
    $(document).ready(function() {
        //  To add new input file field dynamically, on click of "Add More Files" button below function will be executed.
        $('#add_more').click(function() {
            /*$('#slider-row').append($('<div/>',{
               class:'col-md-2'
            }).fadeIn('slow').append($("<input/>", {
            name: 'images[]',
            type: 'file',
            id: 'file'
            }), $("<br/><br/>")));*/
            $('#slider-row').append($('<div class="col-md-4">').append($('<div id="filediv">').append($('<div id="slider-dropzone">').append($('<div class="choose_text">Choose Image</div>')).fadeIn('slow').append($("<input/>",{
                name:'images[]',
                type: 'file',
                id: 'file',
                
            })))));
            });
// Following function will executes on change event of file input to select different file.
$('body').on('change', '#file', function() {
if (this.files && this.files[0]) {
abc += 1; // Incrementing global variable by 1.
var z = abc - 1;
var x = $(this).parent().find('#previewimg' + z).remove();

$(this).before("<div id='abcd" + abc + "' class='abcd'><img id='previewimg" + abc + "' src='' style='height:100%'/></div>");
var reader = new FileReader();
reader.onload = imageIsLoaded;
reader.readAsDataURL(this.files[0]);
$(this).hide();
$("#abcd" + abc).append($("<img/>", {
id: 'img',
src: "<?php echo base_url().'assets/img/x.png'; ?>",
alt: 'delete'
}).click(function() {
$(this).parent().parent().parent().parent().remove();
}));
}
});
// To Preview Image
function imageIsLoaded(e) {
$('#previewimg' + abc).attr('src', e.target.result);
};
$('#upload').click(function(e) {
var name = $(":file").val();
if (!name) {
alert("First Image Must Be Selected");
e.preventDefault();
}
});
});
</script>
<script>
    $(document).ready(function() {
    $(document).on('click','#more_files', function() {
        var numOfInputs = 1;
        while($('#files'+numOfInputs).length) { numOfInputs++; }//once this loop breaks, numOfInputs is greater than the # of browse buttons

        $("<input type='file' multiple/>")
            .attr("id", "files"+numOfInputs)
            .attr("name", "files"+numOfInputs+"[]")
            .insertAfter("#files"+(numOfInputs-1));

        $("<br/>").insertBefore("#files"+numOfInputs);
    });
});
</script>