<!--TimePicker Css-->
<link rel="stylesheet" href="<?php echo base_url().'assets/updatedFrontend/css/styletimepicki.css'; ?>" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url().'assets/updatedFrontend/css/timepicki.css'; ?>" type="text/css" />
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
	  $( function() {
		$( ".datepicker" ).datepicker();
	  } );
  </script>
	<style>
		 #dropzone {
  position: relative;
  color: #cecece;
  font: bold 24px/220px arial;
  height: 220px;
  text-align: center;
  width: 100%;
}

#dropzone.hover {
  border: 10px solid #FE5;
  color: #FE5;
}

#dropzone div {
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
}

#dropzone img {
  border-radius: 10px;
  vertical-align: middle;
  max-width: 95%;
  max-height: 95%;
}

#dropzone [type="file"] {
  cursor: pointer;
  position: absolute;
  opacity: 0;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
}
 /*slider dropzone*/
#slider-dropzone {
  position: relative;
  border: 5px dashed #cecece;
  border-radius: 20px;
  color: #cecece;
  /*font: bold 15px/40px arial;*/
  height: 100px;
  margin: 30px auto;
  text-align: center;
  width: 100px;
}

#slider-dropzone.hover {
  border: 10px solid #FE5;
  color: #FE5;
}

/*#slider-dropzone.dropped {
  background: #f5f5f5;
  border: 10px solid #444;
}*/

#slider-dropzone div {
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  font-size: 15px;
}

#slider-dropzone img {
  border-radius: 5px;
  vertical-align: middle;
  max-width: 95%;
  max-height: 95%;
}

#slider-dropzone [type="file"] {
  cursor: pointer;
  position: absolute;
  opacity: 0;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
}
   /* input[type="file"] {
    display: none;
}*/
    #add_more {
        display: none;
    }
.custom-file-upload {
    border: 1px solid #ccc;
    display: inline-block;
    padding: 6px 12px;
    cursor: pointer;
}
   /* input[type="file"] {
    display: none;
}*/
    #file-input {
        display: none;
    }
.custom-file-upload {
    border: 1px solid #ccc;
    display: inline-block;
    padding: 6px 12px;
    cursor: pointer;
}
    svg {
	width: 100px;
    height: 100px;
	margin: 10px;
    display:inline-block;
}
.cls-1,.cls-2 {
	fill:none;
	stroke-linecap:bevel;
	stroke-linejoin:round;
}
.cls-1 {
	stroke-width:2px;
}
.cls-2 {
	fill:none;
	stroke:#fff;
	stroke-width:4px;
}
#page_loading{
    position: absolute;
    top: 50%;
    left: 50%;
    margin-top: -50px;
    margin-left: -50px;
    width: 100px;
    height: 100px;
    background-color: aliceblue;
    z-index: 9999;
}    
	</style>
<div class="full-width" id="loader">
<div id="form_loading"">
    <svg viewBox="-25 -25 100 100" preserveAspectRatio>
        <defs>
            <linearGradient id="gr-simple" x1="0" y1="0" x2="100%" y2="100%">
            <stop stop-color="rgba(255,255,255,.2)" offset="10%"/>
            <stop stop-color="rgba(255,255,255,.7)" offset="90%"/>
            </linearGradient>
        </defs>	
        <circle class="cls-1" cx="26" cy="27" r="26" stroke="url(#gr-simple)"/>
        <path class="cls-2" d="M25,0A24.92,24.92,0,0,1,42.68,7.32" transform="translate(1 2)">
            <animateTransform 
                     attributeName="transform" 
             type="rotate"
             dur="1s" 
             from="0 26 27"
             to="360 26 27" 
             repeatCount="indefinite"/>
            </path>
    </svg>
</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div>
				<ul class="nav nav-tabs editabletabbar">
					<li role="presentation"><a href="<?php echo base_url().'leader/leader_home';?>" class="tabmenu" data-target="manage-event">My Events</a></li>
					<li role="presentation" class="<?php if($this->uri->segment(2) == 'my_groups'){echo 'active';} ?>"><a href="<?php echo base_url().'leader/my_groups';?>" class="tabmenu" data-target="manage-group">My Groups</a></li>				
				</ul>
			</div>
			<div class="content-wrapsec">
				<div class="row">
					<div class="col-md-12">
						<h3>Create New Event</h3>
					</div>
				</div>
				<div class="borderbottomsec"></div>
				<div class="row">
					<div class="col-md-12">
						<form method="post" id="event_editform" action="" enctype="multipart/form-data">
						<input type="hidden" name="e_id" id="e_id" value="<?php echo $event->e_id;?>"/>							
							<div class="row">
								<div class="col-md-4 col-sm-4">
									<div id="image_preview">
										<!--<img id="previewing" src="<?php //echo base_url().'assets/frontend/img/xyz.jpg';?>" />-->
										<div id="dropzone">
											<div>Choose Logo</div>
												<img id="old_logo" src="<?php echo base_url().''.$event->e_img; ?>"/>
												<input type="file" name="event_logo" id="event_logo" />
											</div>
									</div>
									<div class="form-group">
										<hr />
									<div class="row">
										<div class="col-md-6 col-md-offset-3 text-center">
											<b class="text-center">Slider Images</b>
										</div>
									</div>
									<div class="row">
									<?php 
										$sliderimgs = $this->db->get_where('event_slider_images',array('event_id' => $event->e_id))->result();
										foreach($sliderimgs as $row1){?>
										<?php //print_r($row1); ?>
										<?php if($row1->status == 1)    { ?>
										<div class="col-md-4 col-sm-4 col-xs-4">
											<img id="" class="img-responsive" src="<?php echo base_url().$row1->img_path; ?>" alt="Slider image" style="height: 80px; width:100%;border:1px solid #ccc" />
											<input type="button" class="btn btn-warning btn-outline btn-sm center-block" id="image_remove" name="remove" value="Remove" onclick="remove_image(<?php echo $row1->id; ?>)" style="margin-top: 5px;"/>
										</div>
										<?php } }?>
									</div>
											<div class="row" id="slider-row">
											</div>
											<div class="row">
												<div class="col-md-6 col-md-offset-2 text-right"> 
													<label for="add_more" class="custom-file-upload" style="margin-left: 20%;">
														<i class="glyphicon glyphicon-cloud-upload"></i> Add More
													</label>
													<input type="button" id="add_more" class="upload" value="Add More Files"/>
												</div>
											</div>
									</div>
								</div>
								<div class="col-md-6 col-sm-8">
									<div class="form-group">
										<input type="text" value="<?php echo $event->e_name; ?>" class="form-control" name="e_name" id="e_name" placeholder="Enter event name"/>
									</div>
									<div class="form-group">
										<input type="text-area" value="<?php echo $event->e_type;?>" class="form-control" name="e_type" id="e_type" placeholder="Enter event type"/>
									</div>
									<div class="form-group">										
										<select class="js-example-basic-single form-control" name="e_by" id="e_by" placeholder="Select CCA">
											<option value="<?php echo $event->e_by;?>" selected="" >
											<?php $q1=$this->db->get_where('club_table',array('c_id'=>$event->e_by)); 
												  echo $q1->row()->c_name;?>
											</option>
											<?php 
											//print_r($grps);
											foreach($grps as $glist){
											$q = $this->db->get_where('club_table',array('c_id' => $glist->gclubid));
											$a = $q->result();
											foreach($a as $row){
											?>
										   <option value="<?php echo $row->c_id;?>"><?php echo $row->c_name;?> </option>
											<?php } }?>
									</select>
									</div>
									<div class="form-group">
										<input type="text-area" value="<?php echo $event->e_loc;?>" class="form-control"  name="e_loc" id="e_loc" placeholder="Enter event location"/>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-md-6">
												<input type="text" class="form-control datepicker" value="<?php echo $event->e_start;?>" name="e_start"  placeholder="Start date"/>
											</div>
											<div class="visible-xs">
												<br/>
											</div>
											<div class="col-md-6">
												<div class="inner cover indexpicker">
													<input type="text" value="<?php echo $event->e_start_time; ?>" class="form-control timepicker1" name="e_start_time"  placeholder="Start time"/>
												</div>
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-md-6">
												<input type="text" class="form-control" value="<?php echo $event->e_end;?>" name="e_end" id="e_end" placeholder="End date"/>
											</div>
											<div class="visible-xs">
												<br/>
											</div>
											<div class="col-md-6">
											<div class="inner cover indexpicker">
												<input type="text" class="form-control timepicker1" value="<?php echo $event->e_end_time; ?>" name="e_end_time" id="e_end_time" placeholder="End time"/>
											</div>
											</div>
										</div>
									</div>
									<div class="form-group">
										<textarea class="form-control" value="" name="e_desc" cols="30" rows="4" placeholder="Enter event description"  aria-required="true"><?php if(isset($event->e_desc)){ echo $event->e_desc; } ?></textarea>										
									</div>
									<br/>
									<div class="form-group">
										<div class="row">
											<div class="col-md-4 col-sm-4">
												<a class="btn btn-success btn-join btn-save"  id="btn-join" href="javascript:void(0)" style="width:100%">Save Event</a>
											</div>
											<div class="visible-xs">
												<br/>
											</div>
											<div class="col-md-4 col-sm-4">
												<a class="btn btn-warning btn-join" id="btn-join" href="javascript:void(0)" style="width:100%">Publish</a>
											</div>
											<div class="visible-xs">
												<br/>
											</div>
											<div class="col-md-4 col-sm-4">
												<a class="btn btn-danger btn-join" id="btn-join" href="javascript:void(0)" style="width:100%">Delete Events</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="borderbottomsec"></div>
							<div class="row">							
								<div class="col-md-12">
									<div style="border: 1px solid gray;" class="table-responsive">
										<table id="member_table" class="table table-bordered table-striped table-hover">
											<thead>
												<tr>
													<th>Student Name</th>
													<th>Matriculation No.</th>
													<th>Faculty</th>
													<th>Hall</th>
													<th>Roles</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												<td>Sam</td>
												<td>121234</td>
												<td>Arts Faculty</td>
												<td>Hall No 2</td>
												<td>Admin</td>
												<td>
													<input type="button" value="Assign Admin" class="btn btn-success" />
													<input type="button" value="Remove" class="btn btn-danger" />
												</td>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>				
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?php echo base_url().'assets/updatedFrontend/js/timepicki.js'; ?>"></script>
 <script>
	 $('.timepicker1').timepicki();
 </script>
<script>
    function remove_image(id)   {
        //alert(id);
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                var image_id = id;
        console.log(image_id);
        $.ajax({
            type: "POST",
            url: "<?php echo base_url().'leader/remove_event_image'; ?>",
            data: {image_id: image_id },
            success: function(result)   {
                var parsed = JSON.parse(result);
                console.log(parsed);
                if(parsed.status_code == 1) {
                    location.reload(); 
                }
                else    {
                    swal({
                                html: parsed.status,
                                type: 'error',
                            });

                }
            }
        });
      }
                })
    }
</script>
<script>
    $(document).ready(function(){
       $('#loader').hide(); 
    });
</script>
<script>
    var html='';

for (var i = 0; i < 3; i++) {
   $('#slider-row').append($('<div class="col-md-4">').append($('<div id="filediv">').append($('<div id="slider-dropzone">').append($('<div class="choose_text">Choose Image</div>')).fadeIn('slow').append($("<input/>",{
                name:'images[]',
                type: 'file',
                id: 'file',
                
            })))));
}
$("#slider-row").append(html);
</script>
<script>
   var abc = 0;      // Declaring and defining global increment variable.
    $(document).ready(function() {
        //  To add new input file field dynamically, on click of "Add More Files" button below function will be executed.
        $('#add_more').click(function() {
            /*$('#slider-row').append($('<div/>',{
               class:'col-md-2'
            }).fadeIn('slow').append($("<input/>", {
            name: 'images[]',
            type: 'file',
            id: 'file'
            }), $("<br/><br/>")));*/
            $('#slider-row').append($('<div class="col-md-4">').append($('<div id="filediv">').append($('<div id="slider-dropzone">').append($('<div class="choose_text">Choose Image</div>')).fadeIn('slow').append($("<input/>",{
                name:'images[]',
                type: 'file',
                id: 'file',
                
            })))));
            });
// Following function will executes on change event of file input to select different file.
$('body').on('change', '#file', function() {
if (this.files && this.files[0]) {
abc += 1; // Incrementing global variable by 1.
var z = abc - 1;
var x = $(this).parent().find('#previewimg' + z).remove();

$(this).before("<div id='abcd" + abc + "' class='abcd'><img id='previewimg" + abc + "' src='' style='height:100%'/></div>");
var reader = new FileReader();
reader.onload = imageIsLoaded;
reader.readAsDataURL(this.files[0]);
$(this).hide();
$("#abcd" + abc).append($("<img/>", {
id: 'img',
src: "<?php echo base_url().'assets/img/x.png'; ?>",
alt: 'delete'
}).click(function() {
$(this).parent().parent().parent().parent().remove();
}));
}
});
// To Preview Image
function imageIsLoaded(e) {
$('#previewimg' + abc).attr('src', e.target.result);
};
$('#upload').click(function(e) {
var name = $(":file").val();
if (!name) {
alert("First Image Must Be Selected");
e.preventDefault();
}
});
});
</script>
 <script>
		  $(document).ready(function()
		  {
			   var minDate = new Date();
			  $("#e_start").datepicker({
				   dateFormat:"yy-mm-dd",
				   showAnim:'drop',
				   minDate :minDate,
				   onClose:function(selectedDate)
				   {
					   $('#e_end').datepicker("option","minDate",selectedDate);
				   }
				   
				   });
		  });
		  
		  $(function()
		  { 
		  var minDate1 = new Date();
			  $("#e_end").datepicker(
			  { 
				   dateFormat:"yy-mm-dd",
				   showAnim:'drop',
				   minDate :minDate1,
				   onClose:function(selectedDate1)
				   {
					   $('#e_start').datepicker("option","minDate1",selectedDate1);
				   }
			  });
		  });
		  </script>
		  <script>
$(document).ready(function(e)  {
        $(".btn-save").click(function(e)  {
		e.preventDefault();
		var form = $('#event_editform');
		var formData = new FormData(form[0]);
		console.log(formData);
		$('#loader').show();
		$.ajax({
                type: "POST",
                url: "<?php echo base_url().'leader/editsubmit'; ?>",
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function(res)  {
					$('#loader').hide();
					var parsed = JSON.parse(res);
                    console.log(parsed);
                    if(parsed.status_code == 1) {
                        //alert('data inserted successfully');
                        //window.location.href = "<?php echo site_url('leader/leader_home'); ?>";
                        swal({
                                html: parsed.status,
                                type: 'success'
                        }).then(function() {
                            window.location.href = "<?php echo site_url('leader/leader_home'); ?>";
                        });
                    }
                    else if(parsed.status_code == 0)    {
                        //alert(parsed.status);
                        swal({
                                html: parsed.status,
                                type: 'error',
                            });
                        }
                    else    {
                            swal({
                                html: parsed.status,
                                type: 'error',
                            });
                            //alert(parsed.status);
                        //$('#form-error').html(parsed.status);
					
					}
					
				}
		});
		});
	});
</script>
<script>
// Function to preview image after validation
$(function() {
$("#file").change(function() {
$("#message").empty(); // To remove the previous error message
var file = this.files[0];
var imagefile = file.type;
var match= ["image/jpeg","image/png","image/jpg"];
if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
{
$('#previewing').attr('src','noimage.png');
$("#message").html("<p id='error'>Please Select A valid Image File</p>"+"<h4>Note</h4>"+"<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
return false;
}
else
{
var reader = new FileReader();
reader.onload = imageIsLoaded;
reader.readAsDataURL(this.files[0]);
}
});
});
function imageIsLoaded(e) {
$("#file").css("color","green");
$('#image_preview').css("display", "block");
$('#previewing').attr('src', e.target.result);
$('#previewing').attr('width', '250px');
$('#previewing').attr('height', '230px');
};
</script>
<script>
// Function to preview image after validation
$(function() {
$("#file1").change(function() {
$("#message").empty(); // To remove the previous error message
var file = this.files[0];
var imagefile = file.type;
var match= ["image/jpeg","image/png","image/jpg"];
if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
{
$('#preview1').attr('src','noimage.png');
$("#message").html("<p id='error'>Please Select A valid Image File</p>"+"<h4>Note</h4>"+"<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
return false;
}
else
{
var reader = new FileReader();
reader.onload = image1IsLoaded;
reader.readAsDataURL(this.files[0]);
}
});
});
function image1IsLoaded(e) {
$("#file1").css("color","green");
$('#preview_image1').css("display", "block");
$('#preview1').attr('src', e.target.result);
$('#preview1').attr('width', '90px');
$('#preview1').attr('height', '90px');
};
</script>
<!--choose logo dropzone-->
<script>
    $(function() {
  
  $('#dropzone').on('dragover', function() {
    $(this).addClass('hover');
  });
  
  $('#dropzone').on('dragleave', function() {
    $(this).removeClass('hover');
  });
  
  $('#dropzone input').on('change', function(e) {
    var file = this.files[0];

    $('#dropzone').removeClass('hover');
    
    if (this.accept && $.inArray(file.type, this.accept.split(/, ?/)) == -1) {
      return alert('File type not allowed.');
    }
    
    $('#dropzone').addClass('dropped');
    $('#dropzone img').remove();
    
    if ((/^image\/(gif|png|jpeg)$/i).test(file.type)) {
      var reader = new FileReader(file);

      reader.readAsDataURL(file);
      
      reader.onload = function(e) {
        var data = e.target.result,
            $img = $('<img />').attr('src', data).fadeIn();
        
        $('#dropzone div').html($img);
      };
    } else {
      var ext = file.name.split('.').pop();
      
      $('#dropzone div').html(ext);
    }
  });
});
</script>