<style>
	.tabmenu {
		padding: 10px 40px 10px 10px !important;
		font-size: 16px;
	}
	.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
		color: #555555;
		background-color: #f2f2f2 !important;
		border: 1px solid #dddddd;
		border-bottom-color: rgb(221, 221, 221);
		border-bottom-color: transparent;
		cursor: default;
}
       #dropzone {
  position: relative;
  border: 5px dashed #cecece;
  border-radius: 20px;
  color: #cecece;
  font: bold 24px/200px arial;
  height: 200px;
  margin: 30px auto;
  text-align: center;
  width: 200px;
}

#dropzone.hover {
  border: 10px solid #FE5;
  color: #FE5;
}

#dropzone.dropped {
  background: #f5f5f5;
  border: 10px solid #444;
}

#dropzone div {
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
}

#dropzone img {
  border-radius: 10px;
  vertical-align: middle;
  max-width: 95%;
  max-height: 95%;
}

#dropzone [type="file"] {
  cursor: pointer;
  position: absolute;
  opacity: 0;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
}
   /* input[type="file"] {
    display: none;
}*/
    #file-input {
        display: none;
    }
.custom-file-upload {
    border: 1px solid #ccc;
    display: inline-block;
    padding: 6px 12px;
    cursor: pointer;
}
svg {
	width: 100px;
    height: 100px;
	margin: 10px;
    display:inline-block;
}
.cls-1,.cls-2 {
	fill:none;
	stroke-linecap:bevel;
	stroke-linejoin:round;
}
.cls-1 {
	stroke-width:2px;
	stroke: #dadada;
}
.cls-2 {
	fill:none;
	stroke:#fff;
	stroke-width:4px;
}
#form_loading{
    position: absolute;
    top: 50%;
    left: 50%;
    margin-top: -50px;
    margin-left: -50px;
    width: 100px;
    height: 100px;
	z-index: 9999;
}
.full-width{
	position: absolute;
	height: 100%;
	width:100%;
	background-color: #fbfbfb;
}
 /*slider dropzone*/
    #slider-dropzone {
  position: relative;
  border: 5px dashed #cecece;
  border-radius: 20px;
  color: #cecece;
  /*font: bold 15px/40px arial;*/
  height: 100px;
  margin: 30px auto;
  text-align: center;
  width: 100px;
}

#slider-dropzone.hover {
  border: 10px solid #FE5;
  color: #FE5;
}

#slider-dropzone.dropped {
  background: #f5f5f5;
  border: 10px solid #444;
}

#slider-dropzone div {
  position: absolute;
  top: 20px;
  right: 0;
  bottom: 0;
  left: 0;
  font-size: 15px;
}

#slider-dropzone img {
  border-radius: 10px;
  vertical-align: middle;
  max-width: 95%;
  max-height: 95%;
}

#slider-dropzone [type="file"] {
  cursor: pointer;
  position: absolute;
  opacity: 0;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
}
   /* input[type="file"] {
    display: none;
}*/
    #add_more {
        display: none;
    }
.custom-file-upload {
    border: 1px solid #ccc;
    display: inline-block;
    padding: 6px 12px;
    cursor: pointer;
}
</style>
<div class="full-width" id="loader">
<div id="form_loading"">
    <svg viewBox="-25 -25 100 100" preserveAspectRatio>
        <defs>
            <linearGradient id="gr-simple" x1="0" y1="0" x2="100%" y2="100%">
            <stop stop-color="rgba(255,255,255,.2)" offset="10%"/>
            <stop stop-color="rgba(255,255,255,.7)" offset="90%"/>
            </linearGradient>
        </defs>	
        <circle class="cls-1" cx="26" cy="27" r="26" stroke="url(#gr-simple)"/>
        <path class="cls-2" d="M25,0A24.92,24.92,0,0,1,42.68,7.32" transform="translate(1 2)">
            <animateTransform 
                     attributeName="transform" 
             type="rotate"
             dur="1s" 
             from="0 26 27"
             to="360 26 27" 
             repeatCount="indefinite"/>
            </path>
    </svg>
</div>
</div>
<div class="container">
	<div class="row">		
		<div class="col-md-12">
			<div>
				<ul class="nav nav-tabs">
					<li role="presentation"><a href="<?php echo base_url().'leader/leader_home';?>" class="tabmenu" data-target="manage-event">My Events</a></li>
					<li role="presentation" class="active"><a href="<?php echo base_url().'leader/my_groups';?>" class="tabmenu" data-target="manage-group">My Groups</a></li>
				</ul>
			</div>
			<div class="content-wrapsec">
				<div>
					<h3>Edit Group</h3>
				</div>
				<div class="borderbottomsec"></div>
				 <form method="post" action="" id="groupedit_form" name="dropzone">
					<input type="hidden" id="groupid" name="groupid" value="<?php echo $groupdata;?>"/>
					<div class="row">
						<div class="col-md-4 col-sm-4">
                            <?php $q=$this->db->get_where('club_table',array('c_id'=>$groupdata));
                                   $logo=$q->row()->c_logo; ?>
							<div class="form-group">
                                 <div id="dropzone">
                                    <div>Choose Logo</div>
                                     <img id="old_logo" src="<?php echo base_url().$logo;?>"/>
                                    <input type="file" name="group_logo" accept="image/png, image/jpeg, image/jpg" />
                                </div>
                                <hr />
                                <div class="row">
                                    <div class="col-md-6 col-md-offset-3 text-center">
                                        <b class="text-center">Slider Images</b>
                                    </div>
                                </div>
                                <div class="row">
                                <?php foreach($sliderimgs as $row1){?>
                                    <?php //print_r($row1); ?>
                                    <?php if($row1->status == 1)    { ?>
                                    <div class="col-md-4 col-sm-4 col-xs-4">
                                        <img id="" class="img-responsive" src="<?php echo base_url().$row1->images; ?>"                        alt="Slider image" style="height: 80px; width:100%;border:1px solid #ccc" />
                                        <input type="button" class="btn btn-warning btn-outline btn-sm center-block" id="image_remove" name="remove" value="Remove" onclick="remove_image(<?php echo $row1->id; ?>)" style="margin-top: 5px;"/>
                                    </div>
                                    <?php } }?>
                                </div>
                                <div class="row" id="slider-row">
                                </div>
                                <div class="row" style="margin-top: 1em;">
                                    <div class="col-md-6 col-md-offset-2 text-right">
                                        <label for="add_more" class="custom-file-upload" style="margin-left: 20%;">
                                            <i class="glyphicon glyphicon-cloud-upload"></i> Add More
                                        </label>
                                        <input type="button" id="add_more" class="upload" value="Add More Files"/>
                                    </div>
                                </div>
                                <!--<div id="filediv"><input name="images[]" type="file" id="file"/></div>-->,
							</div>						
						</div>
						<div class="visible-xs"><br/></div>
						<div class="col-md-8 col-sm-8">
							<div class="form-group">
								<label>School Name:</label>
								<?php $query2 = $this->db->get_where('university',array('u_id'=>$unidata)); ?>
								<select required class="form-control" name="c_uni" id="c_uni" value="<?php echo $query2->row()->u_name;?>">
									<option value="<?php echo $query2->row()->u_id;?>" selected="" ><?php echo $query2->row()->u_name;?></option>
									<?php $this->db->where('permit !=',0);
									$query = $this->db->get('university');
									foreach($query->result() as $row){ 
									?>
									<option value="<?php echo $row->u_id;?>"><?php echo $row->u_name;?></option>
									<?php } ?>
								</select>
								<p id="school_name_error" style="color:red;"></p>
							</div>
							<div class="form-group">
								<label>Group Name:</label>
								<?php $query1 = $this->db->get_where('club_table',array('c_id'=>$groupdata)); ?>
								<input type="text" class="form-control" id="c_name"  name="c_name" value="<?php echo $query1->row()->c_name;?>" required/>
								<p id="group_name_error" style="color:red;"></p>
							</div>	
							<div class="form-group">
								<label>Group Description:</label>
								<?php //$query3 = $this->db->get_where('club_table',array('c_id'=>$groupdata)); ?>
								<input type="text-area" class="form-control" name="c_desc" value="<?php echo $query1->row()->c_desc;?>" id="c_desc" 
								wrap="soft"/>
								<p id="group_desc_error" style="color:red;"></p>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-4 col-sm-4">
										<input type="submit" class="btn btn-success btn-save" value="Save and Update">							
									</div>
									<div class="visible-xs">
										<br/>
									</div>
									<div class="col-md-4 col-sm-4">
										<a class="btn btn-danger btn-delete" href="<?php echo base_url().'leader/delete_group/'.$groupdata;?>">Delete Group</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				
					<div class="borderbottomsec"></div>
				
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-7">
									<div>
										<h3>Manage Members</h3>
									</div>
								</div>
								<div class="col-md-3">
									
								</div>
							</div>
							<br/>
							<div class="row">
								<div class="col-md-12">
									<div style="border: 1px solid gray;" class="table-responsive">
										<table id="member_table" class="table table-bordered table-striped table-hover">
											<thead>
											<tr>
											<th>Student Name</th>
											<th>Matriculation No.</th>
											<th>Faculty</th>
											<th>Hall</th>
											<th>Roles</th>
											<th>Action</th>
											</tr>
											</thead>
											<tbody>
											<?php foreach($members as $stud){
												//print_r( $stud);
												$query1 = $this->db->get_where('user_table',array('id'=>$stud->gstudid));
												$user = $query1->row();
												?>
												 <tr>
											<td><?php echo $user->user_name;?></td>
											<td><?php echo $user->matno;?></td>
											<td><?php echo $user->faculty;
													  $query2 = $this->db->get_where('faculty_table',array('f_id'=>$user->faculty))->row();
													  if(isset($query2->f_name)){ echo $query2->f_name;} ?>
											</td>
											<td><?php $query3 = $this->db->get_where('hall_table',array('h_id'=>$user->hall_name))->row();
														if(isset($query3->h_name)){echo $query3->h_name;} ?></td>
											<td><?php if($stud->type==1){ echo "Member";} else {echo "Admin";}?></td>
											<td>
											<div class="row">
											<div class="col-md-6">
											<?php if($stud->type==1){ ?>
													<a  class="btn btn-success" name="remove" id="remove" style="color:#000000;" 
													href="<?php echo base_url().'leader/remove_member/'.$stud->gid.'/'.$groupdata.'/'.$unidata;?>">Remove</a>
											</div>
										 <div class="col-md-6">
											<a class="btn btn-warning" name="admin" id="admin" style="color:#000000;"
												 href="<?php echo base_url().'leader/add_groupadmin/'.$stud->gid.'/'.$groupdata.'/'.$unidata;?>">
												 Assign 
											</a>
											<?php } else {
												echo "Admin";
												}?>
										 </div>
											</div>
											</td>
											 </tr>
											<?php }?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script>
    $(document).ready(function(){
       $('#loader').hide(); 
    });
</script>
  <!--<script>
$('div.dataTables_filter input').addClass('form-control form-control-md');
    $(document).ready(function() {	 
        $('#member_table').DataTable();
		$('#member_table_filter input').removeClass('form-control-sm');
		$('#member_table_filter input').addClass('form-control-md');
		$('#member_table_length select').removeClass('form-control-sm');
		$('#emember_table_length select').addClass('form-control-md'); 
    });
</script>-->
 <script>
//EDIT GROUP SCRIPT
$(function() { // <----------------missed the doc ready function
    $('#groupedit_form').submit(function(e) {
        e.preventDefault(); // <------this will restrict the page refresh
		$('#loader').show();
        /*var form_data = {
            grpuni: $('#group_uni').val(),
            grpname: $('#group_name').val(),            
        };*/
		var form = $('#groupedit_form');
        var formData = new FormData(form[0]);
        console.log(formData);
        //alert(form_data.e_name);
        $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + 'leader/editgroup_submit',
                data: formData,
				processData: false,  // tell jQuery not to process the data
				contentType: false,  // tell jQuery not to set contentType
                success: function(res)  {
					//alert(res);
					$('#loader').hide();
                    var json = JSON.parse(res);
                    console.log(json);
                     if(json.st== 'fail'){
                        $('#school_name_error').html(json.c_uni);
                        $('#group_name_error').html(json.c_name);
						$('#group_desc_error').html(json.c_desc);
						}
                   
                   else if(json.status_code == 1){
                        //alert('Group Updated successfully');
						swal({
                                html: json.status,
                                type: 'success'
                        }).then(function() {
                            location.reload(true);
                        });
                    } 
                   else{
						console.log('error');
				   }
                }
        });

    });

});
</script>
<script>
   var abc = 0;      // Declaring and defining global increment variable.
    $(document).ready(function() {
        //  To add new input file field dynamically, on click of "Add More Files" button below function will be executed.
        $('#add_more').click(function() {
            /*$('#slider-row').append($('<div/>',{
               class:'col-md-2'
            }).fadeIn('slow').append($("<input/>", {
            name: 'images[]',
            type: 'file',
            id: 'file'
            }), $("<br/><br/>")));*/
            $('#slider-row').append($('<div class="col-md-4">').append($('<div id="filediv">').append($('<div id="slider-dropzone">').append($('<div class="choose_text">Choose Image</div>')).fadeIn('slow').append($("<input/>",{
                name:'images[]',
                type: 'file',
                id: 'file',
                
            })))));
            });
// Following function will executes on change event of file input to select different file.
$('body').on('change', '#file', function() {
if (this.files && this.files[0]) {
abc += 1; // Incrementing global variable by 1.
var z = abc - 1;
var x = $(this).parent().find('#previewimg' + z).remove();

$(this).before("<div id='abcd" + abc + "' class='abcd'><img id='previewimg" + abc + "' src='' style='height:100%'/></div>");
var reader = new FileReader();
reader.onload = imageIsLoaded;
reader.readAsDataURL(this.files[0]);
$(this).hide();
$("#abcd" + abc).append($("<img/>", {
id: 'img',
src: "<?php echo base_url().'assets/img/x.png'; ?>",
alt: 'delete'
}).click(function() {
$(this).parent().parent().parent().parent().remove();
}));
}
});
// To Preview Image
function imageIsLoaded(e) {
$('#previewimg' + abc).attr('src', e.target.result);
};
$('#upload').click(function(e) {
var name = $(":file").val();
if (!name) {
alert("First Image Must Be Selected");
e.preventDefault();
}
});
});
</script>
<script>
    $(document).ready(function() {
    $(document).on('click','#more_files', function() {
        var numOfInputs = 1;
        while($('#files'+numOfInputs).length) { numOfInputs++; }//once this loop breaks, numOfInputs is greater than the # of browse buttons

        $("<input type='file' multiple/>")
            .attr("id", "files"+numOfInputs)
            .attr("name", "files"+numOfInputs+"[]")
            .insertAfter("#files"+(numOfInputs-1));

        $("<br/>").insertBefore("#files"+numOfInputs);
    });
});
</script>
<script>
    $(function() {
  
  $('#dropzone').on('dragover', function() {
    $(this).addClass('hover');
  });
  
  $('#dropzone').on('dragleave', function() {
    $(this).removeClass('hover');
  });
  
  $('#dropzone input').on('change', function(e) {
    var file = this.files[0];

    $('#dropzone').removeClass('hover');
    
    if (this.accept && $.inArray(file.type, this.accept.split(/, ?/)) == -1) {
      return alert('File type not allowed.');
    }
    
    $('#dropzone').addClass('dropped');
    $('#dropzone img').remove();
    
    if ((/^image\/(gif|png|jpeg)$/i).test(file.type)) {
      var reader = new FileReader(file);

      reader.readAsDataURL(file);
      
      reader.onload = function(e) {
        var data = e.target.result,
            $img = $('<img />').attr('src', data).fadeIn();
        
        $('#dropzone div').html($img);
      };
    } else {
      var ext = file.name.split('.').pop();
      
      $('#dropzone div').html(ext);
    }
  });
});
</script>
<script>
    function remove_image(id)   {
        //alert(id);
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                var image_id = id;
        console.log(image_id);
        $.ajax({
            type: "POST",
            url: "<?php echo base_url().'leader/remove_image'; ?>",
            data: {image_id: image_id },
            success: function(result)   {
                var parsed = JSON.parse(result);
                console.log(parsed);
                if(parsed.status_code == 1) {
                    location.reload(); 
                }
                else    {
                    swal({
                                html: parsed.status,
                                type: 'error',
                            });

                }
            }
        });
      }
                })
    }
</script>