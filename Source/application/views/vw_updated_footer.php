    <!-- Footer
		============================================= -->
        <footer id="footer" class="dark" style="font-size: 20px; color: #fff">
            <!-- Copyrights
			============================================= -->
            <div id="copyrights">
                <div class="container clearfix">
                    <div class="row">
                        <div class="col-md-12">
                            <div style="text-align: center; font-size: 16px; color: #fff">
                                Copyrights &copy; 2017 All Rights Reserved by AFU.<br>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <!-- #copyrights end -->
        </footer>
        <!-- #footer end -->
		