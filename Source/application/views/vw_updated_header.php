<!doctype html>
<html lang="en" xmlns:fb="http://ogp.me/ns/fb#">
    <head>
		<meta charset="utf-8"/>
		<title>AFU</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<meta name="description" content="Site Description Here"/>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />

		<!-- Stylesheets
		============================================= -->
		<link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="<?php echo base_url().'assets/updatedFrontend/css/bootstrap.css'; ?>" type="text/css" />
		<link rel="stylesheet" href="<?php echo base_url().'assets/updatedFrontend/style.css'; ?>" type="text/css" />
		<link rel="stylesheet" href="<?php echo base_url().'assets/updatedFrontend/css/swiper.css'; ?>" type="text/css" />
		<link rel="stylesheet" href="<?php echo base_url().'assets/updatedFrontend/css/dark.css'; ?>" type="text/css" />
		<link rel="stylesheet" href="<?php echo base_url().'assets/updatedFrontend/css/font-icons.css'; ?>" type="text/css" />
		<link rel="stylesheet" href="<?php echo base_url().'assets/updatedFrontend/css/animate.css'; ?>" type="text/css" />
		<link rel="stylesheet" href="<?php echo base_url().'assets/updatedFrontend/css/magnific-popup.css'; ?>" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url().'assets/updatedFrontend/css/responsive.css'; ?>" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url().'assets/updatedFrontend/css/bootstrap-datetimepicker.min.css'; ?>" type="text/css" />
		<link rel="stylesheet" href="<?php echo base_url().'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'; ?>" type="text/css" />
        <!--Dropzone CSS-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.2.0/dropzone.css" type="text/css" />
        <!--SweetAlert 2 CSS-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.7/sweetalert2.min.css" />
		
		<style>
			  html {
				  height: 100%;
				  box-sizing: border-box;
				}
				*,
				*:before,
				*:after {
				  box-sizing: inherit;
				}
				.content-wrapsec {
					margin-bottom: 10%;
				}
				body {
				  position: relative;
				  margin: 0;
				  padding-bottom: 6rem;
				  min-height: 100%;
				}
				#footer {
					position: absolute;
					  right: 0;
					  bottom: 0;
					  left: 0;
					  background-color: #efefef;
					  text-align: center;
					}
			  .slider-caption, .nivo-caption, .flex-caption {
				  position: absolute;
				  top: 0;
				 <!-- left: 15px;
				  right: 15px; -->
				  z-index: 20;
				  min-width: 100%;
				  <!--min-width: 1300px;-->
				  color: #EEE;
				  font-size: 20px;
				  font-weight: 300;
				  text-shadow: 1px 1px 1px rgba(0,0,0,0.15);
				  -webkit-transition: top .3s ease;
				  -o-transition: top .3s ease;
				  transition: top .3s ease;
				  padding: 20px 60px 0px 20px;
		  }

		  .content-wrap {
			  position: relative;
			  padding: 0px 0 20px 0;
		  }

		  .owl-carousel .owl-nav [class*="owl-"] {
			  position: absolute;
			  top: 29%;
			  margin-top: -18px;
			  left: -36px;
			  zoom: 1;
			  width: 36px;
			  height: 36px;
			  line-height: 32px;
			  border: 1px solid rgba(0,0,0,0.2);
			  color: #666;
			  background-color: #FFF;
			  font-size: 18px;
			  border-radius: 50%;
			  opacity: 0;
			  -webkit-transition: all .3s ease;
			  -o-transition: all .3s ease;
			  transition: all .3s ease;
		  }

		  .dark #header.transparent-header.full-header:not(.sticky-header) #header-wrap:not(.not-dark) #primary-menu > ul, #header.transparent-header.full-header.dark:not(.sticky-header) #header-wrap:not(.not-dark) #primary-menu > ul {
			border-right-color: rgba(255,255,255,0.0);
		  }

		  #header.full-header #primary-menu > ul {
			  float: left;
			  padding-right: 15px;
			  margin-right: 15px;
			  border-right: 1px solid #EEE;
			  border-right-color: rgba(255,255,255,0.0);
		  }

		  #header.transparent-header.full-header #primary-menu > ul {
			border-right-color: rgba(0,0,0,0.0);
		  }

		  .dark #header-wrap:not(.not-dark) #primary-menu > ul > li:hover > a, .dark #header-wrap:not(.not-dark) #primary-menu > ul > li.current > a, .dark #header-wrap:not(.not-dark) #primary-menu > .container > ul > li:hover > a, .dark #header-wrap:not(.not-dark) #primary-menu > .container > ul > li.current > a {
			 color: #fff;
			 text-decoration: underline !important;
		  }
		  
		</style>

		<style>
			@media only screen and (min-device-width : 320px) and (max-device-width : 480px) {
				.swiper_wrapper:not(.force-full-screen), .swiper_wrapper:not(.force-full-screen):not(.canvas-slider-grid) .swiper-slide, .ei-slider {
				 height: 970px !important;
			  }

			.slider-caption {
				top: 55.5px !important;
			  }
		  }
		</style>

  </head>
  <body class="stretched">

    <!-- Document Wrapper
	============================================= -->
    <div id="wrapper" class="clearfix">

        <!-- Header
		============================================= -->
        <header id="header" class="transparent-header full-header" data-sticky-class="not-dark">

            <div id="header-wrap">

                <div class="container clearfix">

                    <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

                    <!-- Logo
					============================================= -->
                    <div id="logo">
                        <a href="#" class="standard-logo" data-dark-logo="images/logo-dark1.png">
                            <img src="<?php //echo base_url().'assets/updatedFrontend/images/afu.png'; ?>" alt="AFU" style="display: none"></a>
                    </div>
                    <!-- #logo end -->

                    <!-- Primary Navigation
					============================================= -->
                    <nav id="primary-menu">
                        <ul>
							
                            <?php if (isset($this->session->userdata['logged_in'])) {
									$username = ($this->session->userdata['logged_in']['username']);
									$email = ($this->session->userdata['logged_in']['email']);
									$q=$this->db->get_where('user_table' ,array('user_name'=> $username))->row();
									//$res=$q->type;
							?>
							<li><a href="<?php echo base_url().'leader/browse_events'; ?>">
                                <div>Browse event</div>
                            </a>
							</li>
							<li><a href="<?php echo base_url().'leader/create_event'; ?>">
                                <div>Create event</div>
                            </a>
							</li>
							<li><a href="<?php echo base_url().'leader/create_group'; ?>">
                                <div>Create group</div>
                            </a>
							</li>
							<li><a href="<?php echo base_url().'leader/join_group';?>">
                                <div>Join new group</div>
                            </a>
							</li>
                            <li>                               
                                <a href="">
                                <div>Welcome, 
								   <?php echo $username; ?>
								</div>									
								</a>
								<ul>
									<li>
										<?php if (isset($this->session->userdata['logged_in'])) {
												$usertype = ($this->session->userdata['logged_in']['sessiontype']);
												if($usertype==1){$url=base_url()."student/logout";}
												else{$url = base_url().'leader/logout';}
												}
										?>
										<a href="<?php echo base_url().'leader/logout'; ?>">
										<!--<a href="<?php //echo $url; ?>">-->
											<div>Logout</div>
										</a>																                                  
									</li>								
								</ul>
                            </li>
							
                            <?php } else {?>
                            <li><a href="<?php echo base_url().'leader'; ?>">
                                <div>Log In</div>
                            </a>
                            </li>
                            <li><a href="<?php echo base_url().'leader'; ?>">
                                <div>Sign Up</div>
                            </a>
                            </li>
                            <?php  } ?>
                            
												
                            <li><a href="<?php echo base_url().'home';?>">
                                <div>Contact Us</div>
                            </a>
                            </li>
                            
                        </ul>
                    </nav>
                    <!-- #primary-menu end -->
                </div>
            </div>

        </header>
        <!-- #header end -->


        <!-- Content
		============================================= -->
        <section id="content">
            <div class="content-wrap">
             
             </div>         
        </section>
       
        <!-- #content end -->

    </div>
    <!-- #wrapper end -->

    <!-- Go To Top
	============================================= -->
    <div id="gotoTop" class="icon-angle-up"></div>

    <!-- External JavaScripts
	============================================= -->
    <!--<script type="text/javascript" src="<?php //echo base_url().'assets/landing_page/js/jquery.js'; ?>"></script>-->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets/landing_page/js/plugins.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets/js/jquery.fileupload.js'; ?>"></script>
	
	<!--Moment JS-->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.min.js"></script>
	<script src="<?php echo base_url().'assets/updatedFrontend/js/bootstrap-datetimepicker.min.js'; ?>"></script>

    <!--Dropzone JS-->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.2.0/dropzone.js"></script>
    <!--SweetAlert JS-->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.7/sweetalert2.all.min.js"></script>
    <!-- Footer Scripts
	============================================= -->
    <script type="text/javascript" src="<?php echo base_url().'assets/landing_page/js/functions.js'; ?>"></script>

	</body>
</html>