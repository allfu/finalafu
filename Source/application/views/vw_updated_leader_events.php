<style>
	.boxed {
		padding: 0;
	}
	.pricing {	
		width: 350px;
		height: 635px;		
	}
	@media only screen and (min-device-width : 320px) and (max-device-width : 480px) {
		.pricing {
			width: 240px;
			height: 700px;
		}
	}
	/* iPads (portrait and landscape) ----------- */
	@media only screen and (min-device-width : 768px) and (max-device-width : 1024px) {
		.pricing {
			width: 280px;
			height: 650px;
		}
	}
	.modal-body {
		padding: 0 10px;
	}
	.modal-header {
		border-bottom: 1px solid #fff;
	}
</style>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div>
				<ul class="nav nav-tabs editabletabbar">
					<li role="presentation" class="<?php if($this->uri->segment(2) == 'leader_home'){echo 'active';} ?>" ><a href="<?php echo base_url().'leader/leader_home';?>" class="tabmenu" data-target="manage-event">My Events</a></li>
					<li role="presentation" class=""><a href="<?php echo base_url().'leader/my_groups';?>" class="tabmenu" data-target="manage-group">My Groups</a></li>
				</ul>
			</div>
			<div class="content-wrapsec">
				<div class="row">
					<div class="col-md-10 col-sm-10 col-xs-12">
						<h3>My Events</h3>
					</div>								
				</div>
				<div class="borderbottomsec"></div>
				<div class="row">
					<div class="col-md-12">
						<?php ?>
						<section class="switchable feature-large bg-secondary">
							<?php //foreach();
							//echo "groups";?>
								<div class="row main-sec">
									<?php foreach($events as $row){
										?>
										<div class="col-sm-4 col-sm-6">
											<div class="pricing pricing-1 boxed--border container-sec">												
												<div>
													<?php if(isset($row->e_img)){?>
															<img alt="Image" class="event-img" src="<?php echo base_url().$row->e_img;?>" />
														<?php } 
														else{?>
															<img alt="Image" src="<?php echo base_url().'assets/frontend/';?>img/xyz.jpg" />
													<?php } ?>													
												</div>
												<div class="row">
													<div class="col-md-4 col-xs-4 col-sm-4"></div>																										
													<div class="col-md-4 col-sm-4 col-xs-4">														
														<div class="logo-event-img-sec">
															<?php if(isset($row->e_logo)){?>
																<img alt="Image" style="height:90px;width:100%;border-radius:50px"  src="<?php echo base_url().$row->e_logo;?>"/>
															<?php } 
																else{?>
																<img alt="Image" style="height:90px;width:100%;border-radius:50px" src="<?php echo base_url().'assets/frontend/';?>img/upload_image.png"/>
															<?php } ?>															
														</div>
													</div>
													<div class="col-md-4 col-sm-4 col-xs-4"></div>																								
												</div>
												<div class="row">
													<div class="col-md-12">
														<div class="content-center attending-members"> 
															<h4>120 out of 155 Attending</h4>
														</div>
													</div>
												</div>
												<div class="row content-blog">
													<div class="col-md-12">														
														<div>													
															<label class="grp-title"><?php echo $row->e_name;?></label>
														</div>
														<div>											
															<label ><?php $q=$this->db->get_where('club_table',array('c_id'=> $row->e_by));
															 $gname = $q->row()->c_name;
															 echo $gname;?></label>
														</div>
													</div>
												</div>
												<div class="row date-location-content">
													<div class="col-md-6 col-xs-6">
														<label>Date & Time</label>
														<?php if(isset($row->e_start)){?>
															<p style="word-wrap: break-word"><?php
																echo date("F jS,  g:i a", strtotime($row->e_start));?>
															</p>
														<?php }?>															
													</div>
													<div class="col-md-6 col-xs-6">
														<label>Location</label>
														<p style="word-wrap: break-word">
															<?php if(isset($row->e_loc)){echo $row->e_loc;} 
															else{echo "Atrium A";}?>
														</p>
													</div>
												</div>
												<div class="row">
													<div class="col-md-12">
														<div style="text-align:center">
															<a class="btn btn-info btn-join" href="javascript:void(0)" data-toggle="modal" id="view_modal" data-target="#myModal" data-id="<?php echo $row->e_id; ?>">View More Details</a>
														</div>												
													</div>
												</div>	
												<br/>
												<div class="row">
													<div class="col-md-12">
														<p class="content-center days-rmng-sec">10 days remaining!</p>											
													</div>
												</div>	
											</div>
										</div>
									<?php }?>
								</div>							
						</section>
					</div>
				</div>
				<!-- First Modal -->
					<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1">
						 <div class="modal-dialog updatedmodal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header updated-modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="height:1.30em!important"><span aria-hidden="true">&times;</span></button>									
								</div>
							<div class="modal-body">   
								<div class="row">
									<div class="col-md-7" style="border-right:1px solid #ccc;padding-right:0">
										<div>
											<img alt="Image" class="event-img" src="<?php echo base_url().$row->e_img;?>" />
										</div>
										<br/>
										<div style="padding-right:15px">
											<div class="form-group">
												<h5>Your Club Have Worked Hard For Your Welfare Do Help Them Achieve These Objectives</h5>
											</div>
											<br/>
											<div class="row">
												<div class="col-md-6">
													<div>
														
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<img height="50px" width="auto" src="<?php echo base_url().'assets/updatedFrontend/images/like-button.png'?>" />
														<!--<input type="button"  value="Fb Like" class="btn btn-primary btn-fb"/>-->
													</div>
													<br/>
													<div class="form-group">
														<img height="40px" width="auto" style="border:2px solid #ccc;border-radius:5px" src="<?php echo base_url().'assets/updatedFrontend/images/fbshare.png'?>" />
														<!--<input type="button"  value="Fb Like" class="btn btn-primary btn-fb"/>-->
													</div> 
													<br/>
													<div class="form-group">
														<img height="40px" width="auto" src="<?php echo base_url().'assets/updatedFrontend/images/instafollow.png'?>" />
														<!--<input type="button"  value="Fb Like" class="btn btn-primary btn-fb"/>-->
													</div>
												</div>
											</div>
										</div>
										
										
									</div>
									<div class="col-md-5">
										<div style="text-align:center">
											<img alt="Image" style="border-radius:50px;height: 80px;width:80px"  src="<?php echo base_url().$row->e_logo;?>"/>
										</div>									
										<div class="content-center attending-members"> 
											<h4>120 out of 155 Attending</h4>
										</div>
										<div class="form-group content-blog">											
											<label ><?php $q=$this->db->get_where('club_table',array('c_id'=> $row->e_by));
											$gname = $q->row()->c_name;
											echo $gname;?></label>
										</div>
										<div>
											<p>Your Club Have Worked Hard For Your Welfare Do Help Them Achieve These Objectives.
											   Your Club Have Worked Hard For Your Welfare Do Help Them Achieve These Objectives.
											   Your Club Have Worked Hard For Your Welfare Do Help Them Achieve These Objectives</p>
										</div>
										<div class="row">
											<div class="col-md-6 col-xs-6">
												<label>Date & Time</label>
												<?php if(isset($row->e_start)){?>
													<p style="word-wrap: break-word;font-weight:bold"><?php
														//echo date("F jS,  g:i a", strtotime($row->e_start));
														echo $row->e_start; ?>
													</p>
												<?php }?>															
											</div>
											<div class="col-md-6 col-xs-6">
												<label>Location</label>
													<p style="word-wrap: break-word;font-weight:bold">
														<?php if(isset($row->e_loc)){echo $row->e_loc;} 
														else{echo "Atrium A";}?>
													</p>
											</div>
										</div>
										<div class="form-group rmng-days">
											<label>
												10 days remaining!
											</label>
										</div>
										<div class="form-group">
											<a href="<?php echo base_url().'leader/edit_event/'.$row->e_id; ?>" class="btn btn-info btn-block" id="event_id">Edit</a>
										</div>
									</div>
								</div>
							</div>						 
						</div>
					</div>
				</div>
				<!--Second Modal-->
				<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
						 <div class="modal-dialog updatedmodal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header updated-modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="height:1.30em!important"><span aria-hidden="true">&times;</span></button>									
								</div>
							<div class="modal-body">   
								<div class="row">
									<div class="col-md-7 norightpadding">
										<div>
											<img alt="Image" class="event-img" src="<?php echo base_url().$row->e_img;?>" />
										</div>
										<br/>
										<div class="withpadding">
											<div class="form-group">
												<h5>Your Club Have Worked Hard For Your Welfare Do Help Them Achieve These Objectives</h5>
											</div>
											<br/>
											<div class="row">
												<div class="col-md-3">
													<div class="form-group">
														<img alt="Image" width="100%" height="40px" src="<?php echo base_url().$row->e_logo;?>"/>
													</div>
													<br/>
													<div class="form-group">
														<img alt="Image" width="100%" height="40px" src="<?php echo base_url().$row->e_logo;?>"/>
													</div>
													<br/>
													<div class="form-group"> 
														<img alt="Image" width="100%" height="40px" src="<?php echo base_url().$row->e_logo;?>"/>
													</div>
												</div>
												<div class="col-md-7">
													<div>
														<div class="progress updatedprogress">
															<div class="progress-bar" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
														</div>
														<div>
															<label><span style="color:#00aeff">81</span> of 120 Facebook</label>
														</div>
													</div>
													<br/>
													<div>
														<div class="progress updatedprogress">
															<div class="progress-bar" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
														</div>
														<div>
															<label><span style="color:#00aeff">81</span> of 120 Facebook</label>
														</div>
													</div>
													<br/>
													<div>
														<div class="progress updatedprogress">
															<div class="progress-bar" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
														</div>
														<div>
															<label><span style="color:#00aeff">81</span> of 120 Facebook</label>
														</div>
													</div>
												</div>
												<div class="col-md-2">
													<div class="form-group">
														<input type="button"  value="+ 1" class="btn btn-primary btn-fb"/>
													</div>
													<br/>
													<div class="form-group">
														<input type="button"  value="+ 1" class="btn btn-success btn-fb"/>
													</div>
													<br/>
													<div class="form-group">
														<input type="button"  value="+ 1" class="btn btn-success btn-fb"/>
													</div>
												</div>
											</div>
										</div>																				
									</div>
									<div class="col-md-5">
										<div style="text-align:center">
											<img alt="Image" style="border-radius:50px;height: 80px;width:80px"  src="<?php echo base_url().$row->e_logo;?>"/>
										</div>									
										<div class="content-center attending-members"> 
											<h4>120 out of 155 Attending</h4>
										</div>
										<div class="form-group content-blog">											
											<label ><?php $q=$this->db->get_where('club_table',array('c_id'=> $row->e_by));
											$gname = $q->row()->c_name;
											echo $gname;?></label>
										</div>
										<div>
											<p>Your Club Have Worked Hard For Your Welfare Do Help Them Achieve These Objectives.
											   Your Club Have Worked Hard For Your Welfare Do Help Them Achieve These Objectives.
											   Your Club Have Worked Hard For Your Welfare Do Help Them Achieve These Objectives</p>
										</div>
										<div class="row">
											<div class="col-md-6 col-xs-6">
												<label>Date & Time</label>
												<?php if(isset($row->e_start)){?>
													<p style="word-wrap: break-word;font-weight:bold"><?php
														echo date("F jS,  g:i a", strtotime($row->e_start));?>
													</p>
												<?php }?>															
											</div>
											<div class="col-md-6 col-xs-6">
												<label>Location</label>
													<p style="word-wrap: break-word;font-weight:bold">
														<?php if(isset($row->e_loc)){echo $row->e_loc;} 
														else{echo "Atrium A";}?>
													</p>
											</div>
										</div>
										<div class="row form-group">
											<div class="col-md-4">
												Event
												<label>Welfare</label>
											</div>
											<div class="col-md-4">
												Suitability
												<label>80%</label>
											</div>
											<div class="col-md-4">
												Location
												<label>Spms</label>
											</div>
										</div>
										
									</div>
								</div>
							</div>						 
						</div>
					</div>
				</div>
			</div>	
		</div>
	</div>
</div>


