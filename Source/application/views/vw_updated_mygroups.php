<style>	
	
	/*.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
		color: #fff;
		background-color: rgba(0, 0, 0, .9); !important;
		border-left: 2px solid #fff;
		border-right: 2px solid #fff;		
		border-bottom-color: rgb(221, 221, 221);
		border-bottom-color: transparent;
		border-top-color: transparent;
		cursor: default;
		border-radius : 0;	
	}	
	.nav > li > a:hover, .nav > li > a:focus {
		text-decoration: none;
		background-color: #a6a6a6;
		color: #fff;
	}*/
	#group_name	{
		text-transform: uppercase;
		padding-top: 10px;
	}
	#universityname{
		color: #b5b4b4;
	}
	#upcoming-hr{
		border: 0;
		height: 0;
		border-top: 1px solid rgba(0, 0, 0, 0.2);
		border-bottom: 1px solid rgba(255, 255, 255, 0.3);
		padding-bottom: 10px;
	}
	h4{
		margin-bottom: 20px;
	}
	@media only screen 
	  and (min-device-width: 375px) 
	  and (max-device-width: 667px) 
	  and (-webkit-min-device-pixel-ratio: 2) { 
		#btn-join{
			margin-top: 2%;
		}
}
</style>


<?php //echo 'here is the uri segment'.$this->uri->segment(2); ?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div>
				<ul class="nav nav-tabs editabletabbar">
					<li role="presentation"><a href="<?php echo base_url().'leader/leader_home';?>" class="tabmenu" data-target="manage-event">My Events</a></li>
					<li role="presentation" class="<?php if($this->uri->segment(2) == 'my_groups'){echo 'active';} ?>"><a href="<?php echo base_url().'leader/my_groups';?>" class="tabmenu" data-target="manage-group">My Groups</a></li>				
				</ul>
			</div>
			<div class="content-wrapsec">
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<h3>My Groups</h3>
					</div>					
				</div>
				<div class="borderbottomsec"></div>
				<div class="row">
					<div class="col-md-12">
						<?php ?>
						<section class="switchable feature-large bg-secondary">
							<?php //foreach();
							//echo "groups";?>
								<div class="row main-sec">
									<?php if(!empty($grps))	{ ?>
									<?php foreach($grps	 as $row){
										$gclubid = $row->gclubid; 
										if(!empty($gclubid)) {
										$q=$this->db->get_where('club_table',array('c_id'=> $row->gclubid));
										
										if($q->row()->permit != 0){
											
										?>
										<div class="col-sm-4 col-sm-6">
											<div class="pricing boxed boxed--border container-sec" data-target="#group_pop_up" data-toggle="modal"
												data-id="<?php echo $q->row()->c_id;?>" data-name="<?php echo $q->row()->c_name; ?>" 
												data-uni="<?php echo $q->row()->c_uni; ?>" data-lead="<?php echo $q->row()->c_by; ?>"
												data-desc="<?php echo $q->row()->c_desc; ?>" data-logo="<?php echo $q->row()->c_logo; ?>">
												
												<div style="text-align:center">	
													<?php 
														//$q=$this->db->get_where('club_table',array('c_id'=> $row->gclubid));
														if($q->row()->c_logo != NULL){?>																												
															<img src="<?php echo base_url().$q->row()->c_logo;?>" style="height:200px;width:100%">															
														<?php } 
														else{?>												
															<img src="<?php echo base_url().'assets/frontend/img/clublogos/default_logo.png';?>"  style="height:200px;width:100%">															
													<?php } ?>												
												</div>
												<div class="row logo-blogrow">
													<div class="col-md-4 col-xs-4 col-sm-4 logo-blogcol"></div>																										
													<div class="col-md-4 col-sm-4 col-xs-4 logo-blogcolimg">														
														<div class="logoimg-sec">
															<img src="<?php echo base_url().'assets/frontend/img/upload_image.png';?>">
														</div>
													</div>
													<div class="col-md-4 col-sm-4 col-xs-4 logo-blogcol"></div>																								
												</div>
												<div class="row content-blog">
													<div class="col-md-12">
														<!-- <div>
															<?php 
																$q3=$this->db->get_where('university',array('u_id'=> $row->c_uni));
																$uni_name = $q3->row()->u_name;	
																
															?>
														</div> -->
														<div style="display:none">																							 							
															<labe><?php $gname = $q->row()->c_name; //print_r($row);?></label>
														</div>
														<div style="">																							 							
															<label class="grp-title"><?php echo $gname;?></label>
														</div>
														<div>
														<?php 
															$q2=$this->db->get_where('club_table',array('c_id'=> $row->gclubid));
															$uni_id = $q2->row()->c_uni;
															$q3=$this->db->get_where('university',array('u_id'=> $uni_id));
															$uni_name = $q3->row()->u_name; 
														?>
															<label><?php echo $uni_name; ?></label>
														</div>
														<div>																							 							
															<label class="desc-sec"><?php echo $q->row()->c_desc;?></label>
														</div>
														
													</div>
												</div>
												<br/>
												<div class="row">
													<div class="col-md-12">
														<div style="text-align:center">
															<a class="btn btn-info btn-join" id="btn-join" href="javascript:void(0)">View More Details</a>
														</div>												
													</div>
												</div>											
											</div>
										</div>
									<?php } } } }?>
								</div>							
						</section>
					</div>
				</div>
			</div>
			<!-- Modal -->
				<div class="modal fade" id="group_pop_up" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					 <div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header updated-modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="height:1.30em!important"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title" id="myModalLabel">
									 <!---clubname-->
									 <div id=""><p id="group_name"></p></div>
									 <!---universityname-->
									 <div class="blogLocation">
										 <form id="uniname_form" method="post">
											<input type="HIDDEN"  id="grp_uni" name="grp_uni" value=""/>
											<input type="HIDDEN"  id="club_id" name="club_id" value=""/>
										 </form>
									 <p id="universityname"></p>
									 </div>							 
								 </h4>
							 </div>
						<div class="modal-body">
							<div class="row">
								<div class="col-md-12">
									<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">	
										<ol class="carousel-indicators">
											<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
											<li data-target="#carousel-example-generic" data-slide-to="1"></li>
											<li data-target="#carousel-example-generic" data-slide-to="2"></li>
										</ol>
										  <!--carousel images-->
										<div class="carousel-inner" role="listbox" id="imgsdiv">
											<!--/* <div class="item active">
												<img src="<?php //echo base_url().'assets/frontend/img/slider1.jpg';?>" style="width:100%;height: 250px;margin-bottom:0">												 
											</div>
											<div class="item"  >
												<img src="<?php //echo base_url().'assets/frontend/img/slider2.jpg';?>" style="width:100%;height: 250px;;margin-bottom:0">												 
											</div> */-->											
										</div>

										  <!-- Controls -->
										  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
											<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"><i class="fa fa-chevron-circle-left" aria-hidden="true"></i></span>
											<span class="sr-only">Previous</span>
										  </a>
										  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
											<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
											<span class="sr-only">Next</span>
										  </a>
									</div>
								</div>
							 </div>
							 
							 <div class="row">
								<div class="col-md-5 col-xs-4 col-sm-4"></div>																										
									<div class="col-md-2 col-xs-4 col-sm-4">
										<div class="logoimg-sec" style="border: 2px solid #a6a6a6;background-color:white;">
											 		<!--club logo-->
											<img id="logo1" src="" class="silderlogomg">
										</div> 
									</div>
								<div class="col-md-5 col-xs-3 col-sm-4"></div>																								
							 </div>
							 
							 <div class="row">
								<div class="col-md-6 col-sm-6">
									<h4>About Our Club</h4>
									<!--club desc-->
									<p style="text-align:center" id="grp_desc"></p>
								</div>
								<div class="col-md-6 col-sm-6">
									<h4>Past Activities</h4>
									<div class="row">
										<div class="col-md-6 col-sm-6 col-xs-12">
											<div class="content-center"><img style="height:100px" class="img-thumbnail" src="<?php echo base_url().'assets/img/game1.jpg';?>"></div>	
											<div>
												<h5>FOC Camp</h5>
											</div>
											<div class="text-center">
												<p> Here is where
													freshman bond
													and make Friends
												</p>
											</div>
										</div>
										<div class="col-md-6 col-sm-6">
											<div class="content-center"><img style="height:100px" class="img-thumbnail" src="<?php echo base_url().'assets/img/game1.jpg';?>"></div>	
											<div>
												<h5>TLS Camp</h5>
											</div>
											<div class="text-center">
												<p> Here is where
													freshman bond
													and make Friends
												</p>
											</div>
										</div>
									</div>
								</div>
							 </div>
							 <div class="row">
								<div class="col-md-offset-1 col-md-10">
									<div id="upcoming-hr"></div>
								</div>
							 </div>
							  <div class="row">
								<div class="col-md-12">
									<h4 style="text-align:center">Upcoming Activities</h4>
								</div>
							 </div>
							  <div class="row">
								<div class="col-md-3 col-sm-3 col-xs-5">
									 <img style="height:100px" class="img-thumbnail" src="<?php echo base_url().'assets/img/dodgeball2.jpg';?>">
								</div>
								<div class="col-md-9 col-sm-9 col-xs-7">
									<p>Dodgeball 2017 Championship</p>
								</div>
							  </div>
							  <br/>
							<div class="row">
								<div class="col-md-12">
									<div style="text-align:center" id="btn_div">
										<!--<a class="btn btn-info btn-join" href="" id="">
											<!--<span class="btn__text"></span>--
											<p id="new_btn" class="btn__text"></p>-->
										</a>
									</div>
								</div>
							</div>
						  </div>						 
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
//SHOW MY GROUP SCRIPT
//triggered when modal is about to be shown
$('#group_pop_up').on('show.bs.modal', function(e) {
   
    //get data-id attribute of the clicked element
    var grpid   = $(e.relatedTarget).data('id');
    var grpname = $(e.relatedTarget).data('name');
    var grpuni  = $(e.relatedTarget).data('uni');
    var grplead = $(e.relatedTarget).data('lead');
    var grpdesc = $(e.relatedTarget).data('desc');
    var grplogo = $(e.relatedTarget).data('logo');
    var u_namei = null;
	var sliderimages = null;
    var sliderimages_count = null;
    var imgname = null;
    var id = null;
	var logopath1 = "<?php echo base_url();?>" + grplogo;	
	var btn_path = "<?php echo base_url().'leader/edit_group/';?>" + grpid +'/'+grpuni;
	var active='';
	var remove_me_path = "<?php echo base_url().'leader/remove_me_group/';?>" + grpid;
	
    //populate the textboxes
    $(e.currentTarget).find('input[name="grp_uni"]').val(grpuni);
	$(e.currentTarget).find('input[name="club_id"]').val(grpid);
	document.getElementById('group_name').innerHTML = grpname;
	$('#logo1').attr('src', logopath1);
	document.getElementById('grp_desc').innerHTML = grpdesc;
	document.getElementById('imgsdiv').innerHTML = '';/*empty slider before loading dynamically*/
   
      $.ajax({ 
           type: "GET",
           url: "<?php echo base_url().'leader/get_data/';?>"+grpid,
           data: $("#uniname_form").serialize(), // serializes the form's elements.
	       success: function(data1)
           {		 
			   console.log(data1);			   
			   var json = JSON.parse(data1);
			   u_namei = json.uni_name; 
			   document.getElementById('universityname').innerHTML = u_namei;		
			   sliderimages = json.imgs;
			   sliderimages_count = json.c;
			   var result = json['imgs'].map(a => a.images);
			  
			  //for dynamic carousel
			  if( sliderimages_count != 0)
			  { 
				document.getElementById('imgsdiv').innerHTML = '';
				for(i=0; i<result.length;i++)	{
					console.log(result[i]);
					var r=result[i];
					var path= "<?php echo base_url();?>"+r;
					//console.log(document.getElementsByTagName('imgsdiv').length);
				   // (l.getElementsByTagName('img').length > 0) 
				   if(i==0){ active="active";}
					$('#imgsdiv').append('<div class="item '+active+'" ><img src= '+path+' style="width:100%;height: 200px;;margin-bottom:0"></div>');
					active='';
			   }
			  }	
			  else{
				  document.getElementById('imgsdiv').innerHTML = '';
				  var img1= "<?php echo base_url();?>"+"assets/frontend/img/slider1.jpg";
				  var img2= "<?php echo base_url();?>"+"assets/frontend/img/slider2.jpg";
				$('#imgsdiv').append('<div class="item active" ><img src= '+img1+' style="width:100%;height: 200px;;margin-bottom:0"></div>');
				$('#imgsdiv').append('<div class="item" ><img src= '+img2+' style="width:100%;height: 200px;;margin-bottom:0"></div>');				
			  }
			  
			  document.getElementById('btn_div').innerHTML = '';
			  if(json.member_type==1)/*if member type=1:: member is not group admin, only member*/
			  {
				$('#btn_div').append('<a class="btn btn-danger" href="'+remove_me_path+'"><span class="btn__text">Remove me</span></div>');
			  }
			  else{/*if member type=2:: member is group admin*/
				$('#btn_div').append('<a class="btn btn-info" href="'+btn_path+'"><span class="btn__text">Edit group</span></div>');
			  }
           },
		  // async: false
         });
		 
	    //document.getElementById('universityname').innerHTML = u_namei;		
    
    //document.getElementById('grp_leader').value  = grplead;
    //$(e.currentTarget).find('img[name="blah1"]').val(editgrplogo); 
	
});
</script>	