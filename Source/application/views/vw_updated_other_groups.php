
<style>	
	.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
		color: #fff;
		background-color: rgba(0, 0, 0, .8); !important;
		border-left: 2px solid #fff;
		border-right: 2px solid #fff;		
		border-bottom-color: rgb(221, 221, 221);
		border-bottom-color: transparent;
		border-top-color: transparent;
		cursor: default;
		border-radius : 0;	
	}	
	#group_name{
		    text-transform: uppercase;
    padding-top: 10px;
	}
	#universityname	{
		color: #b5b4b4;
	}
</style>


<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div>
				<ul class="nav nav-tabs editabletabbar">
					<li role="presentation"><a href="<?php echo base_url().'leader/leader_home';?>" class="tabmenu" data-target="manage-event">My Events</a></li>
					<li role="presentation" class="active"><a href="<?php echo base_url().'leader/my_groups';?>" class="tabmenu" data-target="manage-group">My Groups</a></li>
				</ul>
			</div>
			<div class="content-wrapsec">
				<div class="row">
					<div class="col-md-10 col-sm-10 col-xs-9">
						<h3>Join Groups</h3>
					</div>
					<!--<div class="col-md-2 col-sm-2 col-xs-3">
						<a class="btn btn-primary" href="<?php //echo base_url().'leader/join_group';?>">
							<span class="btn__text hidden-xs">Join new group</span><span class="btn__text hidden-lg visible-xs"><i class="fa fa-plus-circle" aria-hidden="true"></i></span>
						</a>					
					</div>					-->
				</div>
				<div class="borderbottomsec"></div>
				<div class="row">
					<div class="col-md-12">
						<?php ?>
						<section class="switchable feature-large bg-secondary">
							<?php //foreach();
							//echo "groups";?>
								<div class="row main-sec">
									<?php foreach($newgrps as $row){
										
										$newclubs = null;	?>
										<div class="col-sm-4 col-sm-6"  data-toggle="modal" data-target="#group_modal"  
											data-id="<?php echo $row->c_id;?>" data-name="<?php echo $row->c_name; ?>" 
											data-uni="<?php echo $row->c_uni; ?>" data-lead="<?php echo $row->c_by; ?>"
											data-desc="<?php echo $row->c_desc; ?>" data-logo="<?php echo $row->c_logo; ?>" >
											<div class="pricing pricing-1 boxed boxed--lg boxed--border container-sec">
												<?php  print_r($newclubs);
													 //print_r($row);
													 //$q=$this->db->get_where('club_table',array('c_id'=> $row->c_id));
													 //$gname = $q->row()->c_name;?>
												<div style="text-align:center">										 
													<?php 
														//$q=$this->db->get_where('club_table',array('c_id'=> $row->gclubid));
														if($row->c_logo != NULL){?>													
															<img src="<?php echo base_url().$row->c_logo;?>" style="height:100px;width:150px">																						
													<?php } 
													
													else{?>												
														<img src="<?php echo base_url().'assets/frontend/img/clublogos/default_logo.png';?>" style="height:100px;width:150px">											
														<?php } ?>											  
														<!--<img src="<?php echo base_url().'assets/frontend/img/upload_image.png';?>" style="height:100px;width:150px">-->
												</div>
												<div class="row logo-blogrow" style="margin-top: 10px;">
													<div class="col-md-4 col-xs-4 col-sm-4 logo-blogcol"></div>																										
													<div class="col-md-4 col-sm-4 col-xs-4 logo-blogcolimg">														
														<div class="logoimg-sec">
															<img src="<?php echo base_url().'assets/frontend/img/upload_image.png';?>">
														</div>
													</div>
													<div class="col-md-4 col-sm-4 col-xs-4 logo-blogcol"></div>																								
												</div>
												<div class="row content-blog">
													<div class="col-md-12">
														<!-- <div>
															<?php 
																$q3=$this->db->get_where('university',array('u_id'=> $row->c_uni));
																$uni_name = $q3->row()->u_name;	
																
															?>
														</div> -->
														<div>													
															<label class="grp-title"><?php echo $row->c_name;?></label>
														</div>
														<div>											
															<label ><?php echo $uni_name;?></label>
														</div>
													</div>
												</div>
												<br/>
												<br/>
												<div class="row">
													<div class="col-md-12">
													    <style>
														.inactiveLink {
															   pointer-events: none;
															   cursor: default;
															}
														</style>
														<?php 							
															$this->db->where('gclubid',$row->c_id);
															$this->db->where('gstudid',$uid);
															$this->db->where('permit',1);
															$q1=$this->db->get_where('group_table');
															$newclubs = $q1->result();
															//print_r($newclubs);
															if($newclubs){?>
															<div style="text-align:center">
																<a class="btn btn-info btn-join inactiveLink" href="" >														
																	<span class="btn__text">Joined</span>
																</a>
															</div>
														 
															<?php } 
															else{?>
															<div style="text-align:center">
																<a class="btn btn-warning btn-join inactiveLink"  >	<!--href="<?php //echo base_url().'leader/join_newgroup/'.$row->c_id ;?>"-->													
																	<span class="btn__text">Be one of us</span>
																</a>
															</div>													
														<?php } ?>
													</div>
												</div>
												<br/>
												<div class="row">
													<div class="col-md-12">
														<div style="text-align:center">
															<a href="javascript:void(0)">View More Details</a>
														</div>												
													</div>
												</div>
											</div>
										</div>
									<?php }?>
								</div>							
						</section>
					</div>
				</div>
			</div>
			<!-- Modal -->
				<div class="modal fade" id="group_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					 <div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header updated-modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="height:1.30em!important"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title" id="myModalLabel">
									 <!---clubname-->
									 <div id=""><p id="group_name"></p></div>
									 
									 <!---universityname-->
									 <div class="blogLocation">
									 <form id="uniname_form" method="post">
										<input type="HIDDEN"  id="grp_uni" name="grp_uni" value=""/>
										<input type="HIDDEN"  id="club_id" name="club_id" value=""/>
									 </form>
									 <p id="universityname"></p>
									 </div>							 
								 </h4>
							  </div>
						<div class="modal-body">
							<div class="row">
								<div class="col-md-12">
									<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">										
										  <!--carousel images-->
										<div class="carousel-inner" role="listbox" id="imgsdiv">
												<!--<div class="item active">
												  <img src="<?php //echo base_url().'assets/frontend/img/slider1.jpg';?>" style="width:100%;height: 250px;margin-bottom:0">												 
												</div>
												<div class="item"  >
													   <img src="<?php //echo base_url().'assets/frontend/img/slider2.jpg';?>" style="width:100%;height: 250px;;margin-bottom:0">												 
												</div>-->
											
										</div>

										  <!-- Controls -->
										  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
											<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
											<span class="sr-only">Previous</span>
										  </a>
										  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
											<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
											<span class="sr-only">Next</span>
										  </a>
									</div>
								</div>
							 </div>
							 <div class="row">
								<div class="col-md-5 col-xs-4 col-sm-4"></div>																										
									<div class="col-md-2 col-xs-4 col-sm-4">
										<div class="logoimg-sec" style="border: 2px solid #a6a6a6;background-color:white;">
											 		<!--club logo-->
											<img id="logo1" src="" class="silderlogomg">
										</div> 
									</div>
								<div class="col-md-5 col-xs-3 col-sm-4"></div>																								
							 </div>
							 <br/>
							 <div class="row">
								<div class="col-md-6 col-sm-6">
									<h4>About Our Club</h4>
									<!--club desc-->
									<p id="grp_desc"></p>
								</div>
								<div class="col-md-6 col-sm-6">
									<h4>Past Activities</h4>
									<div class="row">
										<div class="col-md-6 col-sm-6 col-xs-12">
											<div class="content-center"><img style="height:100px" class="img-thumbnail" src="<?php echo base_url().'assets/img/game1.jpg';?>"></div>	
											<div>
												<h5>FOC Camp</h5>
											</div>
											<div>
												<p> Here is where
													freshman bond
													and make Friends
												</p>
											</div>
										</div>
										<div class="col-md-6 col-sm-6">
											<div class="content-center"><img style="height:100px" class="img-thumbnail" src="<?php echo base_url().'assets/img/game1.jpg';?>"></div>	
											<div>
												<h5>TLS Camp</h5>
											</div>
											<div>
												<p> Here is where
													freshman bond
													and make Friends
												</p>
											</div>
										</div>
									</div>
								</div>
							 </div>
							 <br/>
							 <br/>
							 <div class="row">
								<div class="col-md-offset-1 col-md-10">
									<div style="border-bottom: 5px solid #8080ff"></div>
								</div>
							 </div>
							  <div class="row">
								<div class="col-md-12">
									<h4 style="text-align:center">Upcoming Activities</h4>
								</div>
							 </div>
							 <br/>
							  <div class="row">
								<div class="col-md-3 col-sm-3 col-xs-5">
									 <img style="height:100px" class="img-thumbnail" src="<?php echo base_url().'assets/img/dodgeball2.jpg';?>">
								</div>
								<div class="col-md-9 col-sm-9 col-xs-7">
									<p>Dodgeball 2017 Championship</p>
								</div>
							  </div>
							  <br/>
							<div class="row">
								<div class="col-md-12">
									<div style="text-align:center">
										<a class="btn btn-info btn-join"  id="join_btn" href="" >														
											<span class="btn__text" id="span_button"><p id = "button_name" style="margin-bottom:0px;"></p></span>
										</a>
									</div>
								</div>
							</div>
						</div>						 
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
//SHOW GROUP SCRIPT
//triggered when modal is about to be shown
$('#group_modal').on('show.bs.modal', function(e) {
   
    //get data-id attribute of the clicked element
    var grpid   = $(e.relatedTarget).data('id');
    var grpname = $(e.relatedTarget).data('name');
    var grpuni  = $(e.relatedTarget).data('uni');
    var grplead = $(e.relatedTarget).data('lead');
    var grpdesc = $(e.relatedTarget).data('desc');
    var grplogo = $(e.relatedTarget).data('logo');
    var u_namei = null;
	var sliderimages = null;
    var sliderimages_count = null;
    var imgname = null;
    var id = null;
	var logopath1 = "<?php echo base_url();?>" + grplogo;
	var join_path = "<?php echo base_url().'leader/join_newgroup/';?>" + grpid;
	var active='';
		
    //populate the textboxes
    $(e.currentTarget).find('input[name="grp_uni"]').val(grpuni);
    $(e.currentTarget).find('input[name="club_id"]').val(grpid);
	document.getElementById('group_name').innerHTML = grpname;
	$('#logo1').attr('src', logopath1);
	document.getElementById('grp_desc').innerHTML = grpdesc;
	document.getElementById('imgsdiv').innerHTML = '';/*empty slider before loading dynamically*/
	  
	$.ajax({
	   type: "GET",
	   url: "<?php echo base_url().'leader/get_data/';?>"+grpid,
	   data: $("#uniname_form").serialize(), // serializes the form's elements.
	   success: function(data1)
	   {	 
		   console.log(data1);
		   var json = JSON.parse(data1);
		   u_namei = json.uni_name; 
		   document.getElementById('universityname').innerHTML = u_namei;
		   sliderimages = json.imgs;
		   sliderimages_count = json.c;
		   var result = json['imgs'].map(a => a.images);
		     
		  //for dynamic carousel
		  if(sliderimages_count != 0)
		  { 
			document.getElementById('imgsdiv').innerHTML = '';
			for(i=0; i<result.length;i++)	
			{
				console.log(result[i]);
				var r=result[i];
				var path= "<?php echo base_url();?>"+r;
				console.log(path);
				if(i==0){ active="active";}
				$('#imgsdiv').append('<div class="item"><img src='+path+' style="width:100%;height: 250px;;margin-bottom:0"></div>');
				active='';
			}
		  }
		  else
		  {
			  document.getElementById('imgsdiv').innerHTML = '';
				  var img1= "<?php echo base_url();?>"+"assets/frontend/img/slider1.jpg";
				  var img2= "<?php echo base_url();?>"+"assets/frontend/img/slider2.jpg";
				$('#imgsdiv').append('<div class="item active" ><img src= '+img1+' style="width:100%;height: 250px;;margin-bottom:0"></div>');
				$('#imgsdiv').append('<div class="item" ><img src= '+img2+' style="width:100%;height: 250px;;margin-bottom:0"></div>');			
		  }
		  if(json.is_joined != 'yes')
		  {
			$('#join_btn').attr('href', join_path);
			document.getElementById('button_name').innerHTML = 'Be one of us';/*for p tag*/
			//document.getElementById("button_name").textContent="Be one of us";/*for span*/
		  }
		  else{
			//document.getElementById("span_button").innerHTML = "Joined";  /*for span*/
			document.getElementById('button_name').innerHTML = 'Joined';  /*for p tag*/
			//document.getElementById("button_name").textContent="Joined"; /*for span*/
		  }
		  if( sliderimages_count != 0)
			  { 
				document.getElementById('imgsdiv').innerHTML = '';
				for(i=0; i<result.length;i++)	{
					console.log(result[i]);
					var r=result[i];
					var path= "<?php echo base_url();?>"+r;
					//console.log(document.getElementsByTagName('imgsdiv').length);
				   // (l.getElementsByTagName('img').length > 0) 
				   if(i==0){ active="active";}
					$('#imgsdiv').append('<div class="item '+active+'" ><img src= '+path+' style="width:100%;height: 250px;;margin-bottom:0"></div>');
					active='';
			   }
			  }	
			  else{
				  document.getElementById('imgsdiv').innerHTML = '';
				  var img1= "<?php echo base_url();?>"+"assets/frontend/img/slider1.jpg";
				  var img2= "<?php echo base_url();?>"+"assets/frontend/img/slider2.jpg";
				$('#imgsdiv').append('<div class="item active" ><img src= '+img1+' style="width:100%;height: 250px;;margin-bottom:0"></div>');
				$('#imgsdiv').append('<div class="item" ><img src= '+img2+' style="width:100%;height: 250px;;margin-bottom:0"></div>');				
			  }
		  // x = myObj.images;
		   //console.log(x);
		   /* for (i in myObj.images) {
				x += myObj.images[i] + "<br>";
			console.log(x);
			} */
	   },
	  // async: false
	 });
		 
	
	if(sliderimages_count !== 0)
	{
		//for (i = 0; i < sliderimages_count; i++) {
			//$('#imgsdiv').append('<div class="item><img src="<?php echo base_url().'assets/frontend/img/slider2.jpg';?>" style="width:100%;height: 250px;;margin-bottom:0">');
		//}
		//var result = json['imgs'].map(a => a.images);
		/* for(var i in sliderimages)
		{
			console.log(sliderimages);
			  id = sliderimages[i].id;
			  imgname = sliderimages[i].images;
			  alert(imgname);
			  
			  $('#imgsdiv').append('<div class="item><img src="<?php echo base_url();?>'+imgname+'"style="width:100%;height: 250px;;margin-bottom:0">');
		} */
	}
    
    //document.getElementById('grp_leader').value  = grplead;
    //$(e.currentTarget).find('img[name="blah1"]').val(editgrplogo); 
	//$('#blah1').attr('src', logopath);	
});
</script>	 